from .readlines import read_lines
from .readfts import read_fts
from .cont_regs import default_continuum_regions

spec_lines_db = read_lines()
fts_atlas = read_fts()