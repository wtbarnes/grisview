from os import path
import numpy as np
import sys


def read_fts():
    """ Reads FTS atlas spectrum used as reference for continuum fitting."""
    try:
        # default (run-time) path used by bundled executable from PyInstaller
        file_dir = path.join(sys._MEIPASS, 'dat')
    except AttributeError:
        file_dir = path.dirname(path.realpath(__file__))
    fn = path.join(file_dir, 'fts_atlas.csv')
    res = np.genfromtxt(fn)
    res[:, 1] /= 10000.0
    return res
