from collections import OrderedDict

from PyQt6 import QtGui, QtCore
from PyQt6.QtCore import pyqtSignal, pyqtSlot, QObject, QPoint, QLineF, QPointF, QRect
from PyQt6.QtGui import QColor, QAction
from PyQt6.QtWidgets import QColorDialog, QMenu, QLabel, QGraphicsGridLayout, QHBoxLayout, QWidget, \
    QWidgetAction

from pyqtgraph import PlotItem, ImageItem, ScatterPlotItem, HistogramLUTItem, \
    GridItem, GradientLegend, GraphicsLayout, GraphicsLayoutWidget, PlotWidget, AxisItem, \
    ScatterPlotItem, Point, LinearRegionItem, ROI, InfiniteLine, PlotDataItem, \
    LineSegmentROI, LabelItem, ArrowItem, GraphicsWidget, GraphicsWidgetAnchor, TextItem
import pyqtgraph as pg
from pyqtgraph import functions as fn

import numpy as np
from astropy import units as u
from dateutil.parser import parse as date_parse

plt_axes = ['left', 'right', 'top', 'bottom']
plt_axes_labels = {'x': 'X',
                   'y': 'Y'}
map_color_schemes = ['grey',
                     'inferno',
                     'plasma',
                     'magma',
                     'thermal',
                     'flame',
                     'yellowy',
                     'bipolar',
                     'spectrum',
                     'cyclic',
                     'viridis',
                     'greyclip']

Gradients = OrderedDict([
    ('thermal', {'ticks': [(0.3333, (185, 0, 0, 255)), (0.6666, (255, 220, 0, 255)), (1, (255, 255, 255, 255)),
                           (0, (0, 0, 0, 255))], 'mode': 'rgb'}),
    ('seismic', {'ticks': [(0.0, (0, 0, 77, 255)), (0.25, (0, 0, 255, 255)), (0.5, (255, 255, 255, 255)),
                           (0.75, (255, 0, 0, 255)), (1.0, (128, 0, 0, 255))], 'mode': 'rgb'}),
    ('twilight', {'ticks': [(0.0, (225, 218, 225, 255)), (0.25, (98, 116, 186, 255)), (0.5, (47, 20, 55, 255)),
                           (0.75, (179, 89, 83, 255)), (1.0, (225, 218, 225, 255))], 'mode': 'rgb'}),
    ('bipolar', {'ticks': [(0.0, (0, 255, 255, 255)), (1.0, (255, 255, 0, 255)), (0.5, (0, 0, 0, 255)),
                           (0.25, (0, 0, 255, 255)), (0.75, (255, 0, 0, 255))], 'mode': 'rgb'}),
    ('flame', {'ticks': [(0.2, (7, 0, 220, 255)), (0.5, (236, 0, 134, 255)), (0.8, (246, 246, 0, 255)),
                         (1.0, (255, 255, 255, 255)), (0.0, (0, 0, 0, 255))], 'mode': 'rgb'}),
    ('yellowy', {'ticks': [(0.0, (0, 0, 0, 255)), (0.2328863796753704, (32, 0, 129, 255)),
                           (0.8362738179251941, (255, 255, 0, 255)), (0.5257586450247, (115, 15, 255, 255)),
                           (1.0, (255, 255, 255, 255))], 'mode': 'rgb'} ),
    ('spectrum', {'ticks': [(1.0, (255, 0, 255, 255)), (0.0, (255, 0, 0, 255))], 'mode': 'hsv'}),
    ('cyclic', {'ticks': [(0.0, (255, 0, 4, 255)), (1.0, (255, 0, 0, 255))], 'mode': 'hsv'}),
    ('greyclip', {'ticks': [(0.0, (0, 0, 0, 255)), (0.99, (255, 255, 255, 255)),
                            (1.0, (255, 0, 0, 255))], 'mode': 'rgb'}),
    ('grey', {'ticks': [(0.0, (0, 0, 0, 255)), (1.0, (255, 255, 255, 255))], 'mode': 'rgb'}),
    ('viridis', {'ticks': [(0.0, (68, 1, 84, 255)), (0.25, (58, 82, 139, 255)), (0.5, (32, 144, 140, 255)),
                           (0.75, (94, 201, 97, 255)), (1.0, (253, 231, 36, 255))], 'mode': 'rgb'}),
    ('inferno', {'ticks': [(0.0, (0, 0, 3, 255)), (0.25, (87, 15, 109, 255)), (0.5, (187, 55, 84, 255)),
                           (0.75, (249, 142, 8, 255)), (1.0, (252, 254, 164, 255))], 'mode': 'rgb'}),
    ('plasma', {'ticks': [(0.0, (12, 7, 134, 255)), (0.25, (126, 3, 167, 255)), (0.5, (203, 71, 119, 255)),
                          (0.75, (248, 149, 64, 255)), (1.0, (239, 248, 33, 255))], 'mode': 'rgb'}),
    ('magma', {'ticks': [(0.0, (0, 0, 3, 255)), (0.25, (80, 18, 123, 255)), (0.5, (182, 54, 121, 255)),
                         (0.75, (251, 136, 97, 255)), (1.0, (251, 252, 191, 255))], 'mode': 'rgb'}),
])


class GrisPlotMap(GraphicsLayout):
    """Composite maps plotting class including points and regions of interest,
    measures, contours and profiles graphical items."""

    sig_poi_changed = pyqtSignal(object)
    sig_roi_changed = pyqtSignal(object)
    sig_roi_qs_changed = pyqtSignal(object)
    sig_measure_changed = pyqtSignal(object)
    sig_profile_changed = pyqtSignal(object)

    sig_hover_data = pyqtSignal(float, float, float)

    def __init__(self, par, **kargs):
        super().__init__(**kargs)
        self.par = par
        self.plot = GrisPlotMapBase()
        self.colorbar = GrisMapColorBar(parent=self.plot)
        self.contours = GrisMapContoursBar(self)
        self.profiles_plot = GrisMapProfilePlot(self)
        self.compass = GrisMapCompass(parent=self.plot)
        self.poi = []
        self.roi = []
        self.roi_qs = []
        self.uid_roi_qs = -1
        self.measures = []
        self.profiles = []
        self._init_view()

    def _init_view(self):
        """Init plot widget with colorbar."""
        self.setContentsMargins(0, 0, 0, 1)
        self.setSpacing(0)
        self.plot.sig_hover.connect(self.sig_hover_data)
        self.colorbar.set_label(self.par.name, self.par.unit, self.par.scale)
        self.colorbar.set_levels_zero(self.par.zero_centered)
        self.colorbar.set_color_scheme('grey')
        self.colorbar.setVisible(False)
        self.addItem(self.plot, row=0, col=0)
        self.addItem(self.colorbar, row=0, col=1)

    def set_title(self, *args):
        """Set plot top title caption."""
        self.plot.set_title(*args)

    def set_axes_labels(self, unit):
        """Set plot axes labels with units."""
        self.plot.set_axes_labels(unit)

    def hide_axes(self, *args):
        """Show/hide plot axes and title."""
        self.plot.hide_axes(*args)

    def hide_axes_labels(self, *args):
        """Show/hide plot axes labels."""
        self.plot.hide_axes_labels(*args)

    def show_grid(self, *args):
        """Show/hide plot grid."""
        self.plot.show_grid(*args)

    def show_colorbar(self, flag=True):
        """Show/hide plot colorbar widget."""
        self.colorbar.setVisible(flag)

    def set_header(self, header):
        """Set FITS header unit for plot."""
        self.plot.set_header(header)

    def set_compass(self, wcs, x, y):
        """Set compass arrow orientations."""
        self.compass.set_wcs(wcs, x, y)

    def show_compass(self, flag=True):
        """Show/hide compass."""
        self.compass.set_visible(flag)

    def link_view_to(self, p0):
        """Link plot view to selected plot instance (or unlink if None)."""
        if p0 is not None:
            self.plot.link_view_to(p0.plot)
        else:
            self.plot.link_view_to(None)

    def set_pixel_scales(self, *args):
        """Set plot pixel X and Y axis scale for different units."""
        self.plot.set_pixel_scales(*args)

    def set_color_scheme(self, scheme):
        """Set colorbar color scheme."""
        self.colorbar.set_color_scheme(scheme)

    def set_image_data(self, *args):
        """Set plot image data."""
        self.plot.set_image_data(*args)
        self.colorbar.update_axis()

    def update_contours_image(self):
        """Set/update image used for contours overlay."""
        self.contours.set_image_item(self.plot.imgItem)

    def set_test_image_data(self, *args):
        """Dummy method for creating random map (for testing)."""
        self.plot.set_image_data(*args)

    def add_poi(self, color='k'):
        """Add new POI instance to the plot."""
        idx = len(self.poi)
        self.poi.append(GrisMapPOI(parent=self.plot, color=color, idx=idx))
        self.poi[-1].sig_changed.connect(self.sig_poi_changed)

    def add_roi(self, color='k'):
        """Add new ROI instance to the plot."""
        idx = len(self.roi)
        self.roi.append(GrisMapROI(parent=self.plot, color=color, idx=idx))
        self.roi[-1].sig_changed.connect(self.sig_roi_changed)

    def add_roi_qs(self):
        """Add new ROI instance for quiet Sun selection."""
        self.uid_roi_qs += 1
        self.roi_qs.append(GrisMapROI(parent=self.plot, color='g', idx=self.uid_roi_qs))
        r = self.roi_qs[-1]
        r.sig_changed.connect(self.sig_roi_qs_changed)
        r.obj.menu = QMenu()
        act = QAction("Delete", r.obj.menu)
        # act.triggered.connect(r.delete)
        act.triggered.connect(r.reset)
        r.obj.menu.addAction(act)

    def delete_roi_qs(self, idx=-1):
        """Delete ROI used for quiet Sun by its index."""
        if idx < 0:
            for r in self.roi_qs:
                self.plot.removeItem(r.obj)
            self.roi_qs.clear()
            self.uid_roi_qs = 0
        else:
            for r in self.roi_qs:
                if r.idx == idx:
                    self.plot.removeItem(r.obj)
                    # r.delete()
                    # r.reset()
                    self.roi_qs.remove(r)
                    break

    def add_measure(self, color='k'):
        """Add new measure instance to the plot."""
        idx = len(self.measures)
        self.measures.append(GrisMapMeasure(parent=self.plot, color=color, idx=idx))
        self.measures[-1].sig_changed.connect(self.sig_measure_changed)

    def add_profile(self, color='k'):
        """Add new profile instance to the plot."""
        idx = len(self.profiles)
        self.profiles.append(GrisMapProfile(parent=self.plot, color=color, idx=idx))
        self.profiles[-1].sig_changed.connect(self.sig_profile_changed)
        self.profiles_plot.add_curve(color=color)

    def reset_poi(self):
        """Reset all plot POIs."""
        for p in self.poi:
            p.reset()

    def reset_roi(self):
        """Reset all plot ROIs."""
        for r in self.roi:
            r.reset()

    def reset_roi_qs(self):
        """Reset all plot ROIs of quiet Sun."""
        for r in self.roi_qs:
            r.reset()

    def reset_measures(self):
        """Reset all plot measures."""
        for m in self.measures:
            m.reset()

    def reset_profiles(self):
        """Reset all plot profiles."""
        for m in self.profiles:
            m.reset()
        self.link_profile_centers(False)

    def set_poi_visible(self, idx, flag=True):
        """Show/hide POI of a given index."""
        if flag:
            self.poi[idx].show()
        else:
            self.poi[idx].hide()

    def set_roi_visible(self, idx, flag=True):
        """Show/hide ROI of a given index."""
        if flag:
            self.roi[idx].show()
        else:
            self.roi[idx].hide()

    def set_measure_visible(self, idx, flag=True):
        """Show/hide measure of a given index."""
        if flag:
            self.measures[idx].show()
        else:
            self.measures[idx].hide()

    def set_profile_visible(self, idx, flag=True):
        """Show/hide profile of a given index."""
        if flag:
            self.profiles[idx].show()
        else:
            self.profiles[idx].hide()

    def hide_roi_handles(self, flag=True):
        """Show/hide ROI handles (used for resizing)."""
        for r in self.roi:
            r.hide_handles(flag)

    def set_poi_color(self, idx, c):
        """Set color for POI of a given index."""
        self.poi[idx].set_color(c)
        self.poi[idx].sig_changed.emit(self.poi[idx])

    def set_roi_color(self, idx, c):
        """Set color for ROI of a given index."""
        self.roi[idx].set_color(c)
        self.roi[idx].sig_changed.emit(self.roi[idx])

    def set_measure_color(self, idx, c):
        """Set color for measure line of a given index."""
        self.measures[idx].set_color(c)
        self.measures[idx].sig_changed.emit(self.measures[idx])

    def set_profile_color(self, idx, c):
        """Set color for profile of a given index."""
        self.profiles[idx].set_color(c)
        self.profiles[idx].sig_changed.emit(self.profiles[idx])

    def link_poi_to(self, plt):
        """Link POIs of this plot to POIs of a given plot."""
        if len(self.poi) == len(plt.poi):
            for i in range(len(plt.poi)):
                self.poi[i].link_to(plt.poi[i])

    def link_roi_to(self, plt):
        """Link ROIs of this plot to ROIs of a given plot."""
        if len(self.roi) == len(plt.roi):
            for i in range(len(plt.roi)):
                self.roi[i].link_to(plt.roi[i])

    def link_roi_qs_to(self, plt, idx=-1):
        """Link quiet Sun ROIs of this plot to ROIs of a given plot."""
        if len(self.roi_qs) == len(plt.roi_qs):
            self.roi_qs[idx].link_to(plt.roi_qs[idx])

    def link_measures_to(self, plt):
        """Link all measures from this plot to ones from a given plot."""
        if len(self.measures) == len(plt.measures):
            for i in range(len(plt.measures)):
                self.measures[i].link_to(plt.measures[i])

    def link_profiles_to(self, plt):
        """Link all profiles from this plot to ones from a given plot."""
        if len(self.profiles) == len(plt.profiles):
            for i in range(len(plt.profiles)):
                self.profiles[i].link_to(plt.profiles[i])

    def link_profile_centers(self, flag=True):
        """Link all profile centers to the first profile center."""
        if self.profiles:
            num = len(self.profiles)
            for i in range(num - 1):
                for j in range(i + 1, num):
                    if (flag and
                        self.profiles[i].visible and
                        self.profiles[j].visible) or \
                            not flag:
                        self.profiles[i].link_center_to(self.profiles[j], flag)
            if flag:
                if self.profiles[0].pos is not None:
                    for i in range(num):
                        if self.profiles[i].visible:
                            self.profiles[0].sig_center_changed.emit(self.profiles[0].pos)
                            break

    # def unlink_poi(self):
    #     """Unlink all POIs."""
    #     for wgt in self.poi:
    #         wgt.unlink()
            
    def sync_widgets(self, plt):
        """Sync color, position and visibility of child widgets."""
        if len(self.poi) == len(plt.poi):
            for i in range(len(plt.poi)):
                self.poi[i].sync(plt.poi[i])
        if len(self.roi) == len(plt.roi):
            for i in range(len(plt.roi)):
                self.roi[i].sync(plt.roi[i])
        if len(self.roi_qs) == len(plt.roi_qs):
            for i in range(len(plt.roi_qs)):
                self.roi_qs[i].sync(plt.roi_qs[i])
        if len(self.measures) == len(plt.measures):
            for i in range(len(plt.measures)):
                self.measures[i].sync(plt.measures[i])
        if len(self.profiles) == len(plt.profiles):
            for i in range(len(plt.profiles)):
                self.profiles[i].sync(plt.profiles[i])


class GrisPlotMapBase(PlotItem):
    """Widget for basic map plotting, includes image with axes and labels."""

    sig_hover = pyqtSignal(float, float, float)

    def __init__(self, **kargs):
        super().__init__(**kargs)
        self.imgItem = ImageItem()
        self.addItem(self.imgItem)
        self.data = None

        self.title = None
        self.label_x = ''
        self.label_y = ''
        self.label_z = ''
        self.label_style = {'color': 'k',
                            'font-size': '10pt'}
        self.width = -1
        self.height = -1
        self.scale_x = 1.0
        self.scale_y = 1.0
        self.scale_z = 1.0 #e-3

        self.unit_xy = 'px'
        self.step_angle = 0.0
        self.header_ref = None

        self._prepare()

    def _prepare(self):
        """Set plot properties, borders, axes and ticks."""
        self.setAspectLocked(True)
        self.disableAutoRange()
        self.hideButtons()
        self.setMenuEnabled(False)

        self.imgItem.hoverEvent = self.proc_hover_event

        # self.vb.invertY(True)
        self.vb.setBorder(pg.mkPen('k', width=1))
        self.vb.menu = QMenu()
        self.vb.mouseClickEvent = self.on_mouse_click
        self.vb.sigResized.connect(self.set_view_limits)
        # self.vb.sigRangeChanged.connect(self.on_range_change)

        self.showAxis('right')
        self.showAxis('top')

        tickFont = QtGui.QFont()
        tickFont.setPointSize(10)
        for pos in plt_axes:
            ax = self.getAxis(pos)
            ax.showLabel(False)
            ax.setPen('k', width=1)
            ax.setTextPen('k')
            ax.setStyle(tickLength=8, tickFont=tickFont, showValues=False)
            if pos == 'left' or pos == 'bottom':
                ax.mouseDragEvent = lambda ev: None
                ax.showLabel(True)
                ax.setStyle(showValues=True)

        self.set_axes_labels()

    def set_header(self, hdr):
        """Set header to invert Y axes for GRIS maps taken before/after 2016."""
        self.header_ref = hdr
        self.step_angle = float(hdr['STEPANGL'])
        try:
            self.year = date_parse(hdr['DATE-BEG']).year
        except:
            self.year = date_parse(hdr['DATE-OBS']).year
        if self.year < 2016:
            self.vb.invertY(self.step_angle != 0.0)
        else:
            self.vb.invertY(self.step_angle == 0.0)

    def raise_context_menu(self, ev):
        """Raise widget context menu."""
        pos = ev.screenPos()
        self.vb.menu.popup(QtCore.QPoint(int(pos.x()), int(pos.y())))

    def on_mouse_click(self, ev):
        """Process mouse click event."""
        if ev.button() == QtCore.Qt.MouseButton.RightButton:
            self.raise_context_menu(ev)
        ev.accept()

    def set_title(self, title):
        """Set top title text."""
        self.title = title
        self.setTitle(self.title, color='k')

    def set_pixel_scales(self, sc_x=1.0, sc_y=1.0):
        """Set plot pixel X and Y axis scale for different units."""
        self.scale_x = sc_x
        self.scale_y = sc_y

    def set_axes_labels(self, unit=None):
        """Set axes labels with units."""
        if unit is not None:
            self.unit_xy = unit
        if self.unit_xy == 'px':
            for pos in plt_axes:
                self.getAxis(pos).setScale(1.0)
        else:
            self.getAxis('left').setScale(self.scale_y)
            self.getAxis('right').setScale(self.scale_y)
            self.getAxis('bottom').setScale(self.scale_x)
            self.getAxis('top').setScale(self.scale_x)
        self.label_x = plt_axes_labels['x'] + f' [{self.unit_xy}]'
        self.label_y = plt_axes_labels['y'] + f' [{self.unit_xy}]'
        self.getAxis('left').setLabel(self.label_x, **self.label_style)
        self.getAxis('bottom').setLabel(self.label_y, **self.label_style)

    def hide_axes(self, flag=True):
        """Show/hide axes."""
        if flag:
            self.setTitle(None)
            # self.vb.setBorder('g', width=1)
        else:
            self.setTitle(self.title)
            # self.vb.setBorder('k', width=1)
        self.hide_axes_labels(flag)
        for pos in plt_axes:
            self.showAxis(pos, show=not flag)

    def hide_axes_labels(self, flag=True):
        """Show/hide axes labels."""
        self.getAxis('left').showLabel(not flag)
        self.getAxis('bottom').showLabel(not flag)

    def show_grid(self, flag=True):
        """Show/hide grid."""
        self.is_visible_grid = flag
        x_axis = self.getAxis('bottom')
        x_axis.setGrid(220 if flag else False)
        y_axis = self.getAxis('left')
        y_axis.setGrid(220 if flag else False)

    def link_view_to(self, plt=None):
        """Link view box range to a given plot view box."""
        if plt is not None:
            self.setXLink(plt.vb)
            self.setYLink(plt.vb)
        else:
            self.setXLink(None)
            self.setYLink(None)

    def proc_hover_event(self, ev):
        """Process hover event and emit signal with coordinates."""
        if ev.isExit():
            self.sig_hover.emit(-1, -1, -1)
            return
        x = ev.pos().x()
        y = ev.pos().y()
        if 0 <= x < self.width and \
                0 <= y < self.height:
            self.sig_hover.emit(x, y, self.data[int(y)][int(x)])
        else:
            self.sig_hover.emit(-1, -1, -1)

    def set_image_data(self, data):
        """Set plot image."""
        self.imgItem.setImage(data)
        self.width = len(data[0])
        self.height = len(data)
        self.data = data
        data_max = np.max(self.data)
        self.getAxis('left').setZValue(1.1 * data_max)
        self.getAxis('bottom').setZValue(1.1 * data_max)
        self.set_view_limits()

    def set_view_limits(self):
        """Set limits that constrain the possible view ranges."""
        if self.width > 0:
            vr = self.viewRange()
            ratio_vb = (vr[1][1] - vr[1][0])/(vr[0][1] - vr[0][0])
            ratio_img = self.height/self.width
            if ratio_vb < ratio_img:
                wb = self.height / ratio_vb
                dx = 0.5*(wb - self.width)
                self.setLimits(xMin=-dx, xMax=self.width + dx,
                               yMin=0, yMax=self.height,
                               minXRange=self.width/20.0,
                               minYRange=self.height/20.0)
            else:
                hb = self.width * ratio_vb
                dy = 0.5*(hb - self.height)
                self.setLimits(xMin=0, xMax=self.width,
                               yMin=-dy, yMax=self.height + dy,
                               minXRange=self.width / 20.0,
                               minYRange=self.height / 20.0)

    def fit_image_to_view(self):
        """Fit image to the current view box."""
        self.set_view_limits()
        src = self.vb.state['limits']        
        if src['xLimits'][0] is not None:
            self.setRange(xRange=src['xLimits'], yRange=src['yLimits'], padding=0.0)

    def set_test_image_data(self, w=300, h=400):
        """Dummy method for creating random map (for testing)."""
        self.width = w
        self.height = h
        self.setXRange(0, w - 1, padding=0.0)
        self.setYRange(0, h - 1, padding=0.0)
        data = np.random.normal(size=(h, w)) * 10000
        self.setLimits(yMin=0, yMax=h - 1)
        self.imgItem.setImage(data)
        self.data = data



class MapCompassArrow(ArrowItem):
    """Subclass based on ArrowItem with text label and its position
    at the tail end."""

    def __init__(self, txt='', tailLen=40, parent=None):
        if txt:
            self.textItem = TextItem(txt, color='k', anchor=(0.5, 0.5))
            self.textItem.setParentItem(parent)
        else:
            self.textItem = None
        super().__init__(tailLen=tailLen, tailWidth=1.5, headLen=10, tipAngle=45,
                         brush='k', angle=180, parent=parent)

    def set_visible(self, flag):
        """Set arrow and label visibility."""
        self.setVisible(flag)
        if self.textItem is not None:
            self.textItem.setVisible(flag)

    def setStyle(self, **opts):
        """Modify arrow position at tail end and add text at the head."""
        self.opts.update(opts)
        opt = dict([(k, self.opts[k]) for k in ['headLen', 'tipAngle', 'baseAngle', 'tailLen', 'tailWidth']])
        tr = QtGui.QTransform()
        path = fn.makeArrowPath(**opt)
        tr.rotate(self.opts['angle'])
        xoff = path.boundingRect().right()
        tr.translate(-xoff, 0)
        self.path = tr.map(path)
        self.setPath(self.path)

        if self.textItem is not None:
            pt_head = self.path.toFillPolygon().at(0) * 1.15
            pt_cen = self.textItem.parentItem().boundingRect().center()
            self.textItem.setPos(pt_cen + pt_head)
            # self.textItem.setAngle(self.opts['angle'] - 90)

        self.setPen(fn.mkPen(self.opts['pen']))
        self.setBrush(fn.mkBrush(self.opts['brush']))
        # if self.opts['pxMode']:
        #     self.setFlags(self.flags() | self.ItemIgnoresTransformations)
        # else:
        #     self.setFlags(self.flags() & ~self.ItemIgnoresTransformations)


class GrisMapCompass(GraphicsWidget, GraphicsWidgetAnchor):
    """Map compass widget showing directions to north, west and disc center. 
    Displayed inside plot viewbox in the bottom left corner."""

    def __init__(self, parent, size=70):
        GraphicsWidget.__init__(self)
        GraphicsWidgetAnchor.__init__(self)
        # self.setFlag(self.GraphicsItemFlag.ItemIgnoresTransformations)
        self.setGeometry(QtCore.QRectF(0, 0, 2*size, 2*size))
        self.arrows = [MapCompassArrow('W', tailLen=size, parent=self),
                       MapCompassArrow('N', tailLen=size, parent=self),
                       MapCompassArrow('DC', tailLen=size, parent=self)]
        self.parent = parent
        GraphicsWidget.setParentItem(self, parent.vb)

    def set_wcs(self, wcs, x0, y0):
        """Set compass arrow orientations base on WCS object and 
        direction to the disc center for given image coordinates x0 and y0
        in pixels (normally, image center)."""
        self.cos_p, self.sin_p = wcs.wcs.get_pc()[0]
        west_ang = 180 - np.rad2deg(np.arctan2(self.sin_p, self.cos_p))
        if self.parent.year >= 2016:
            if self.parent.step_angle != 0.0:
                west_ang = 180 + west_ang
        else:
            if self.parent.step_angle != 180.0:
                west_ang = 180 + west_ang
        lon, lat = wcs.pixel_to_world_values(x0, y0)
        dc_ang = 180 - np.rad2deg(np.arctan2(lat, lon)) + west_ang

        self.arrows[0].setStyle(angle=west_ang)
        self.arrows[1].setStyle(angle=west_ang-90)
        self.arrows[2].setStyle(angle=dc_ang)
        for a in self.arrows:
            a.setPos(self.boundingRect().center())
        x0 = self.width() * 0.5
        y0 = self.height() * 0.5
        br = [a.boundingRect() for a in self.arrows]
        dx = min([r.left() for r in br])
        dy = max([r.bottom() for r in br])
        offset = (-x0 - dx + 30, y0 - dy - 30)
        self.anchor(itemPos=(0, 1), parentPos=(0, 1), offset=offset)

    def set_visible(self, flag):
        """Set widget visibility."""
        for a in self.arrows:
            a.set_visible(flag)


class GrisMapPOI(QObject):
    """Point of interest widget to work with the map plot.
    On drag emits signal with the current position."""

    sig_changed = pyqtSignal(object)

    def __init__(self, pos=None, color='k', idx=-1, parent=None):
        """Init POI.

        Args:
            pos (tuple, optional): Initial coordinates on the plot. Defaults to None.
            color (str, optional): POI cross mark color. Defaults to 'k' (black).
            idx (int, optional): POI unique index. Defaults to -1.
            parent (GrisPlotMapBase, optional): parent plot. Defaults to None.
        """
        super().__init__(None)
        self.obj = ScatterPlotItem(symbol='x',
                                   pen=pg.mkPen('w', width=1),
                                   brush=color,
                                   size=16)
        self.obj.mouseDragEvent = self.drag_cross
        self.obj.mouseClickEvent = self.on_mouse_click
        self.obj.setVisible(False)
        self.obj.setZValue(10)
        self.pos = None
        self.data = None
        self.color = pg.mkColor(color)
        self.color0 = self.color
        self.visible = False
        self.idx = idx
        self.name = 'POI' + str(idx + 1)
        self.parent = parent
        if parent is not None:
            self.parent.addItem(self.obj)
        self.create_menu()

    def create_menu(self):
        """Create widget context menu."""
        self.obj.menu = QMenu()
        menu = self.obj.menu
        act = QAction("Change color...", menu)
        act.triggered.connect(self.change_color)
        menu.addAction(act)
        act = QAction("Hide", menu)
        act.triggered.connect(self.hide)
        menu.addAction(act)
        act = QAction("Delete", menu)
        act.triggered.connect(self.reset)
        menu.addAction(act)

    def raise_context_menu(self, ev):
        """Raise widget context menu."""
        pos = ev.screenPos()
        self.obj.menu.popup(QtCore.QPoint(int(pos.x()), int(pos.y())))

    def reset(self):
        """Reset widget color, position and visibility to defaults."""
        self.set_color(self.color0)
        self.set_visible(False)
        self.pos = None
        self.sig_changed.emit(self)

    def set_visible(self, flag=True):
        """Set visibility and init position (center of the view box)."""
        self.visible = flag
        self.obj.setVisible(flag)
        if flag and self.pos is None:
            vr = self.parent.viewRange()
            x = 0.5 * (vr[0][0] + vr[0][1])
            y = 0.5 * (vr[1][0] + vr[1][1])
            self.set_pos(Point(x, y))

    def show(self):
        """Show widget (and emit change signal)."""
        self.set_visible(True)
        self.sig_changed.emit(self)

    def hide(self):
        """Hide widget (and emit change signal)."""
        self.set_visible(False)
        self.sig_changed.emit(self)

    def set_pos(self, p):
        """Set position (tuple, in plot coordinates)"""
        self.pos = p
        if self.pos is not None:
            self.obj.setData(pos=[self.pos])

    def set_color(self, c):
        """Set widget color."""
        self.color = c
        self.obj.setBrush(c)

    def drag_cross(self, ev):
        """Process mouse drag event."""
        if ev.button() == QtCore.Qt.MouseButton.LeftButton:
            if ev.isStart():
                pos = ev.buttonDownPos()
                self.drag_offset = self.pos - pos
            elif ev.isFinish():
                pass
            else:
                p = ev.pos() + self.drag_offset
                self.set_pos(p)
                self.sig_changed.emit(self)
        ev.accept()

    def on_mouse_click(self, ev):
        """Process mouse click event."""
        if ev.double() and \
                ev.button() == QtCore.Qt.MouseButton.LeftButton:
            self.change_color()
        if ev.button() == QtCore.Qt.MouseButton.RightButton:
            self.raise_context_menu(ev)
        ev.accept()

    def change_color(self):
        """Set color using color dialog."""
        c = QColorDialog.getColor(self.color)
        if c.isValid():
            self.set_color(c)
            self.sig_changed.emit(self)

    def sync(self, p):
        """Synchronize with the given instance."""
        self.set_visible(p.visible)
        self.set_color(p.color)
        self.set_pos(p.pos)

    def link_to(self, p):
        """Link to the given instance."""
        p.sig_changed.connect(self.sync)
        self.sig_changed.connect(p.sync)

    def unlink(self):
        """Unlink and do not sync with other instances."""
        try:
            self.sig_changed.disconnect()
        except:
            pass

    def dump(self):
        """Dump attributes used to restore the state."""
        state = dict(color=self.color.name(),
                     visible=self.visible)
        if self.pos is not None:
            state['pos'] = tuple(self.pos)
        return state

    def load(self, d):
        """Restore the state."""
        self.set_visible(d['visible'])
        self.set_color(QColor(d['color']))
        if 'pos' in d:
            self.set_pos(Point(d['pos']))
        else:
            self.set_pos(None)
        self.sig_changed.emit(self)


class GrisMapROI(GrisMapPOI):
    """Region-of-interest widget to work with the map plot.
    Subclass of PyQtGraph ROI widget with additional attributes and custom
    interaction model. User can move, resize ROI and change its color.
    Parent widget provides routines to make complex slices of the underlying
    data arrays."""

    def __init__(self, color='k', idx=-1, parent=None):
        """Init ROI.

        Args:            
            color (str, optional): ROI color. Defaults to 'k' (black).
            idx (int, optional): ROI unique index. Defaults to -1.
            parent (GrisPlotMapBase, optional): parent plot. Defaults to None.
        """
        super().__init__(None)
        self.obj = ROI(pos=[0, 0],
                       pen=pg.mkPen(color, width=2),
                       handlePen=pg.mkPen(color, width=2),
                       handleHoverPen=pg.mkPen(color, width=2),
                       hoverPen=pg.mkPen(color, width=3),
                       removable=True,
                       invertible=True)
        if parent is not None:
            self.obj.parent = parent.imgItem
        self.color = pg.mkColor(color)
        self.color0 = self.color
        self.add_handles()
        self.obj.setAcceptedMouseButtons(QtCore.Qt.MouseButton.LeftButton | QtCore.Qt.MouseButton.RightButton)
        #self.obj.sigClicked.connect(self.on_mouse_click)
        self.obj.mouseClickEvent = self.on_mouse_click
        self.obj.sigRegionChanged.connect(self.on_change)
        self.obj.sigRegionChangeFinished.connect(self.on_change_finished)
        self.obj.setVisible(False)
        self.obj.setZValue(9)
        self.pos = None
        self.visible = False
        self.visible_handles = True
        self.change_finished = False
        self.idx = idx
        self.name = 'ROI' + str(idx+1)
        self.parent = parent
        if parent is not None:
            self.parent.addItem(self.obj)
        self.create_menu()
        self.obj.raiseContextMenu = self.raise_context_menu

    def get_bounds(self, width=None, height=None):
        """Return ROI bounds accounting for plot edges."""
        rect = self.obj.parentBounds()
        x1_r = int(max(0, rect.left()))
        y1_r = int(max(0, rect.top()))
        x2_r = int(rect.right())
        y2_r = int(rect.bottom())
        if width is not None:
            x2_r = min(x2_r, width-1)
        if height is not None:
            y2_r = min(y2_r, height-1)
        res = None
        if x2_r > x1_r and y2_r > y1_r:
            res = QRect(QPoint(x1_r, y1_r), QPoint(x2_r, y2_r))
        return res

    def set_visible(self, flag=True):
        """Set visibility and initposition (center of the view box)."""
        self.visible = flag
        self.obj.setVisible(flag)
        if flag and self.pos is None:
            vr = self.parent.viewRange()
            x = 0.5 * (vr[0][0] + vr[0][1])
            y = 0.5 * (vr[1][0] + vr[1][1])
            w = (vr[0][1] - vr[0][0]) / 5
            h = w
            self.obj.blockSignals(True)
            self.obj.setSize(w, h)
            self.obj.setPos(x - w / 2, y - h / 2)
            self.obj.setAngle(0)
            self.obj.blockSignals(False)
            self.pos = Point(x - w / 2, y - h / 2)
            self.change_finished = True

    def set_color(self, c):
        """Set ROI color."""
        self.color = c
        self.obj.hoverPen = pg.mkPen(c, width=3)
        self.obj.setPen(pg.mkPen(c, width=2))
        for h in self.obj.getHandles():
            h.pen = self.obj.pen
            h.currentPen = self.obj.pen
            h.hoverPen = self.obj.hoverPen
            h.update()

    def add_handles(self):
        """Add handles used for resizing ROI."""
        self.obj.addScaleHandle([0.0, 0.0], [1.0, 1.0])
        self.obj.addScaleHandle([1.0, 1.0], [0.0, 0.0])
        self.obj.addScaleHandle([0.0, 1.0], [1.0, 0.0])
        self.obj.addScaleHandle([1.0, 0.0], [0.0, 1.0])

    def hide_handles(self, flag=True):
        """Show/hide ROI handles."""
        self.visible_handles = not flag
        for h in self.obj.getHandles():
            h.setVisible(not flag)

    @pyqtSlot(object)
    def sync(self, p):
        """Synchronize with the given instance."""
        self.set_visible(p.visible)
        self.set_color(p.color)
        self.pos = p.pos
        s = p.obj.saveState()
        self.obj.blockSignals(True)
        self.obj.setState(s)
        self.obj.blockSignals(False)

    @pyqtSlot()
    def on_change(self):
        """Slot used to propagate changes between linked ROI."""
        self.pos = self.obj.parentBounds().center()
        self.sig_changed.emit(self)
        # simple fix to keep handles hidden upon ROI changes
        if not self.visible_handles:
            self.hide_handles()

    @pyqtSlot(object)
    def on_change_finished(self, p):
        """Send signal once change is finished (mouse button released)."""
        self.change_finished = True
        self.sig_changed.emit(self)

    def delete(self):
        """Delete ROI from the parent plot."""
        self.parent.removeItem(self.obj)
        self.pos = None        

    def dump(self):
        """Dump attributes used to restore the ROI state."""
        state = self.obj.saveState()
        if self.pos is None:
            del state['pos']
            del state['size']
            del state['angle']
        state['color'] = self.color.name()
        state['visible'] = self.visible
        return state

    def load(self, d):
        """Restore the ROI instance state."""
        self.set_visible(d['visible'])
        self.set_color(QColor(d['color']))
        if 'pos' in d:
            state = {'pos': d['pos'], 'size': d['size'], 'angle': d['angle']}
            self.obj.setState(state)


class GrisMapMeasure(GrisMapROI):
    """Measure widget is used to interactively measure distances between
    map points to estimate size of the feature depending on selected unit."""


    def __init__(self, color='k', idx=-1, parent=None):
        """Init measure instance.

        Args:            
            color (str, optional): measure line color. Defaults to 'k' (black).
            idx (int, optional): measure unique index. Defaults to -1.
            parent (GrisPlotMapBase, optional): parent plot. Defaults to None.
        """
        super().__init__(color=color, idx=idx, parent=parent)
        self.obj = LineSegmentROI([[0, 0], [1, 1]],
                                  pen=pg.mkPen(color, width=2),
                                  handlePen=pg.mkPen(color, width=2),
                                  handleHoverPen=pg.mkPen(color, width=2),
                                  hoverPen=pg.mkPen(color, width=3))
        self.obj.parent = parent.imgItem
        self.obj.setAcceptedMouseButtons(
            QtCore.Qt.MouseButton.LeftButton | QtCore.Qt.MouseButton.RightButton)
        self.obj.mouseClickEvent = self.on_mouse_click
        self.obj.sigRegionChanged.connect(self.on_change)
        self.create_menu()
        self.obj.raiseContextMenu = self.raise_context_menu
        self.obj.setVisible(False)
        self.obj.setZValue(11)
        if parent is not None:
            self.parent.addItem(self.obj)

    def set_visible(self, flag=True):
        """Set visibility and init position (center of the view box)."""
        self.visible = flag
        self.obj.setVisible(flag)
        if flag and self.pos is None:
            vr = self.parent.viewRange()
            x = 0.5 * (vr[0][0] + vr[0][1])
            y = 0.5 * (vr[1][0] + vr[1][1])
            w = (vr[0][1] - vr[0][0]) / 10
            self.obj.blockSignals(True)
            self.obj.setPos(0, 0)
            self.set_pos([Point(x - w, y), Point(x + w, y)])
            self.obj.blockSignals(False)
            self.pos = Point(x, y)

    def set_pos(self, pts):
        """Set position (two tuples for end points, in plot coordinates)."""
        for i, h in enumerate(self.obj.getHandles()):
            if pts[i] is not None:
                h.setPos(pts[i])
        st = self.obj.getState()
        self.obj.setState(st)

    def get_pos(self):
        """Get end points positions (tuples, in plot coordinates)."""
        if self.pos is not None:
            h = self.obj.listPoints()
            return h[0] + self.obj.pos(), h[1] + self.obj.pos()
        else:
            return None, None

    @pyqtSlot(object)
    def on_change(self):
        """Send signal upon measure change."""
        self.pos = self.obj.parentBounds().center()
        self.sig_changed.emit(self)

    def dump(self):
        """Dump attributes used to restore the measure state."""
        state = self.obj.saveState()
        if self.pos is None:
            del state['points']
            del state['pos']
            del state['size']
            del state['angle']
        state['color'] = self.color.name()
        state['visible'] = self.visible
        return state

    def load(self, d):
        """Restore the instance state"""
        self.set_visible(d['visible'])
        self.set_color(QColor(d['color']))
        if 'points' in d:
            state = {}
            for key in ['pos', 'size', 'angle', 'points']:
                state[key] = d[key]
            self.obj.setState(state)


class GrisMapProfile(GrisMapROI):
    """Profile widget is used to interactively create map slices along the
    selected region."""
    
    sig_center_changed = pyqtSignal(QPointF)

    def __init__(self, color='k', idx=-1, parent=None):
        """Init profile instance.

        Args:            
            color (str, optional): profile cut line color. Defaults to 'k' (black).
            idx (int, optional): profile unique index. Defaults to -1.
            parent (GrisPlotMapBase, optional): parent plot. Defaults to None.
        """
        super().__init__(color=color, idx=idx, parent=parent)
        self.fix_orient = False
        self.avg_width = 1

    def reset(self):
        """Reset widget color, position, orientation and visibility to defaults."""
        self.set_orient(fix=False)
        self.avg_width = 1
        super().reset()

    def set_visible(self, flag=True):
        """Set visibility and init position (center of the view box)."""
        self.visible = flag
        self.obj.setVisible(flag)
        if flag and self.pos is None:
            vr = self.parent.viewRange()
            x = 0.5 * (vr[0][0] + vr[0][1])
            y = 0.5 * (vr[1][0] + vr[1][1])
            s = (vr[0][1] - vr[0][0]) // 2
            self.obj.blockSignals(True)
            self.obj.setSize([s, self.avg_width])
            self.obj.setPos(x - s // 2, y - self.avg_width // 2)
            self.obj.setAngle(0)
            self.obj.blockSignals(False)
            self.pos = Point(x, y)

    def set_orient(self, angle=None, fix=False):
        """Set orientation of the profile.

        Args:
            angle (float, optional): 0 is horizontal, 90 - vertical. Defaults to None.
            fix (bool, optional): lock/unlock orientation change. Defaults to False.
        """        
        if angle is not None:
            self.obj.blockSignals(True)
            self.obj.setAngle(angle, center=[0.5, 0.5])
            self.obj.blockSignals(False)
        self.fix_orient = fix
        self.on_change(self)

    def set_width(self, width):
        """Set width of the profile cut (in pixels)."""
        size = self.obj.size()
        self.avg_width = width
        if self.pos is not None:
            self.obj.setSize([size[0], width], center=[0.5, 0.5])

    @pyqtSlot(object)
    def on_change(self, obj):
        """Send signal upon profile change."""
        self.scale_rot_handle.setVisible(not self.fix_orient)
        self.scale_handle.setVisible(self.fix_orient)
        pos_curr = self.obj.parentBounds().center()
        if self.pos != pos_curr:
            self.pos = pos_curr
            self.sig_center_changed.emit(self.pos)
        self.sig_changed.emit(self)

    def add_handles(self):
        """Add handles used for resizing, rotation the profile."""
        self.scale_rot_handle = self.obj.addScaleRotateHandle([0.0, 0.5], [0.5, 0.5])
        self.scale_handle = self.obj.addScaleHandle([0.0, 0.5], [0.5, 0.5])
        self.center_handle = self.obj.addTranslateHandle([0.5, 0.5])
        self.scale_handle.setVisible(False)
        self.set_color(self.color)

    def sync_center(self, p):
        """Sync profile slice center with a given instance."""
        if self.pos is not None:
            pos_curr = self.obj.parentBounds().center()
            if p != pos_curr:
                diff = p - self.pos
                self.obj.translate(diff)

    def link_center_to(self, p, flag=True):
        """Link profile center to the given instance center."""
        if flag:
            p.sig_center_changed.connect(self.sync_center)
            self.sig_center_changed.connect(p.sync_center)
        else:
            try:
                p.sig_center_changed.disconnect(self.sync_center)
                self.sig_center_changed.disconnect(p.sync_center)
            except:
                pass


class GrisMapColorBar(HistogramLUTItem):
    """Modified pyqtgraph.HistogramLUTItem class
    Introduces two modes:
    - non-interactive mode:
        Has read-only colorbar and an axis on the right with auto map values range.
    - interactive mode:
        Image histogram and colorbar with ticks (small triangles on the right) that
        can be dragged. Histogram has a selectable region: its range is shown as axis
        range in non-interactive mode.
    Short description of modified class attributes:
    - layout (QGraphicsGridLayout): contains graphic elements in rows and columns
      with given margins and spacing
    - vb (ViewBox): where histogram and selectable view region is shown
    - gradient (GradientEditorItem): shows current color scheme (with movable
      ticks in interactive mode)
    - axis (AxisItem): axis for current view range
    """

    def __init__(self, parent=None, **kargs):
        """Init colorbar instance.

        Args:            
            parent (GrisPlotMapBase, optional): parent plot. Defaults to None.
        """
        super().__init__(**kargs)

        self.layout.setContentsMargins(10, 20, 1, 20)
        self.layout.setSpacing(0)
        self.layout.removeItem(self.axis)
        # self.axis.setVisible(False)

        self.set_ticks(5)
        self.create_gradient_menu()
        self.gradient.tickSize = 10
        self.gradient.tickPen = pg.mkPen(0.5)
        for t in self.gradient.ticks.items():
            t[0].pen = self.gradient.tickPen
            t[0].currentPen = self.gradient.tickPen
            t[0].update()
        # self.gradient.mouseClickEvent = self.mouse_click_gradient
        self.gradient.mouseDoubleClickEvent = self.mouse_click_gradient
        self.gradient.sigGradientChanged.connect(self.on_gradient_change)
        # self.gradient.loadPreset('inferno')
        self.gradient.allowAdd = True
        self.gradient.loadPreset = self.load_gradient_preset
        self.gradient_name_curr = ''
        # self.gradient_state = None

        self.vb.setFixedWidth(70)
        self.vb.setMenuEnabled(False)

        self.region.sigRegionChanged.connect(self.update_axis)
        self.region.mouseDragEvent = lambda ev: None # self.on_mouse_drag_event
        self.vb.mouseDragEvent = lambda ev: None
        self.region.lines[0].sigPositionChangeFinished.connect(self.on_region_line_drag_finish)
        self.region.lines[1].sigPositionChangeFinished.connect(self.on_region_line_drag_finish)
        self.region.lines[0].sigDragged.connect(self.on_region_line_drag)
        self.region.lines[1].sigDragged.connect(self.on_region_line_drag)
        self.levels_zero_centered = False
        self.is_region_line_dragged = False
        #self.vb.setMaximumWidth(35)

        self.label_style = {'color': 'k',
                            'font-size': '10pt'}
        self.axis = AxisItem('right', maxTickLength=8)
        if parent is not None:
            self.parent = parent
            self.setImageItem(parent.imgItem)
        #    self.axis.setScale(parent.scale_z)
        # self.label = LabelItem(f"Counts [×10<sup>{np.log10(parent.scale_z):.0f}</sup>]", angle=270)

        self.interactive_mode = None
        self.set_interactive_mode(False)

    def create_gradient_menu(self):
        """Create right-click menu for selecting color scheme gradient."""
        wgt = self.gradient
        wgt.menu = QMenu()

        wgt.menu.addSeparator()
        self.actAlignZero = QAction("Align Scale to Zero", wgt.menu)
        act = self.actAlignZero
        act.setCheckable(True)
        act.triggered.connect(self.set_levels_zero)
        wgt.menu.addAction(act)
        ## build context menu of gradients
        l = wgt.length
        wgt.length = 100
        for g in Gradients:
            px = QtGui.QPixmap(100, 15)
            p = QtGui.QPainter(px)
            wgt.restoreState(Gradients[g])
            grad = wgt.getGradient()
            brush = QtGui.QBrush(grad)
            p.fillRect(QtCore.QRect(0, 0, 100, 15), brush)
            p.end()
            label = QLabel()
            label.setPixmap(px)
            label.setContentsMargins(1, 1, 1, 1)
            labelName = QLabel(g)
            hbox = QHBoxLayout()
            hbox.addWidget(labelName)
            hbox.addWidget(label)
            widget = QWidget()
            widget.setLayout(hbox)
            act = QWidgetAction(wgt)
            act.setDefaultWidget(widget)
            act.triggered.connect(wgt.contextMenuClicked)
            act.name = g
            wgt.menu.addAction(act)
        wgt.length = l
        wgt.menu.addSeparator()
        act = QAction("Reset", wgt.menu)
        act.triggered.connect(self.reset_ticks)
        wgt.menu.addAction(act)
        # self.menu.addAction(self.rgbAction)
        # self.menu.addAction(self.hsvAction)

    def load_gradient_preset(self, name):
        """Overloaded method for setting custom gradient (by name)."""
        self.gradient.restoreState(Gradients[name])
        self.gradient_name_curr = name

    def set_label(self, par='', unit='', scale=0):
        """Set label text for color bar with parameter, unit and
        scale factor."""
        txt = ''
        if par:
            txt = par + ' '
        if scale != 0 and unit:
            txt += f"[×10<sup>{-scale:.0f}</sup> {unit}]"
        elif scale !=0:
            txt += f"[×10<sup>{-scale:.0f}</sup>]"
        elif unit:
            txt += f"[{unit}]"
        self.axis.setLabel(txt, **self.label_style)
        self.axis.setScale(10**scale)

    def set_ticks(self, num=5):
        """Set evenly distributed, movable ticks for color gradient."""
        prev_ticks = self.gradient.listTicks()
        for i in range(num):
            x = i / float(num - 1)
            self.gradient.addTick(x, self.gradient.getColor(x))
        for tick in prev_ticks:
            self.gradient.removeTick(tick[0])
        self.gradient.showTicks(False)
        self.save_ticks()

    def reset_ticks(self):
        """Reset color gradients ticks to default number and positions."""
        self.load_gradient_preset(self.gradient_name_curr)
        self.ticks_pos = []
        num = 5
        for i in range(num):
            x = i / float(num - 1)
            self.ticks_pos.append(x)
        self.restore_ticks()

    def set_interactive_mode(self, flag=True):
        """Turn on/off interactive mode and change the look accordingly."""
        if flag != self.interactive_mode:
            self.interactive_mode = flag

            self.vb.setVisible(flag)
            self.axis.showLabel(not flag)
            # self.axis.setVisible(not flag)
            # self.label.setVisible(not flag)
            self.regions[0].setVisible(flag)
            # for t in self.gradient.ticks.items():
            #     t[0].setVisible(flag)

            if flag:
                # show colorbar and viewbox with histogram and region selection
                # self.gradient.tickSize = 10
                self.gradient.setMaxDim(self.gradient.rectSize +
                                        self.gradient.tickSize + 2)
                # self.gradient.showTicks(True)
                self.layout.removeItem(self.gradient)
                self.layout.removeItem(self.axis)
                self.layout.addItem(self.vb, 0, 0)
                self.layout.addItem(self.gradient, 0, 1)
                self.layout.addItem(self.axis, 0, 2)
                # self.gradient.showTicks(True)
                # if self.gradient_state is not None:
                #     self.gradient.restoreState(self.gradient_state)
            else:
                # show only colorbar and axis with current range
                # self.gradient.tickSize = -2
                self.gradient.setMaxDim(self.gradient.rectSize)
                self.restore_ticks()
                self.update_axis()
                self.layout.removeItem(self.vb)
                self.layout.removeItem(self.gradient)
                self.layout.addItem(self.axis, 0, 1)
                self.layout.addItem(self.gradient, 0, 0)
                # self.layout.addItem(self.label, 0, 2)

            for t in self.gradient.listTicks():
                t[0].setVisible(self.interactive_mode)
                # self.gradient.showTicks(False)

    def update_axis(self):
        """Update axis range accounting for zero-centered scale."""
        if self.levels_zero_centered:
            if not self.is_region_line_dragged:
                lmin, lmax = self.region.getRegion()
                lmax = max(abs(lmin), abs(lmax))
                self.region.setRegion([-lmax, lmax])
        lmin, lmax = self.region.getRegion()
        self.axis.setRange(lmin, lmax)

    def set_levels_zero(self, flag=True):
        """Set levels range to auto or zero-centered."""
        self.levels_zero_centered = flag
        if not isinstance(self.sender(), QAction):
            self.actAlignZero.blockSignals(True)
            self.actAlignZero.setChecked(flag)
            self.actAlignZero.blockSignals(False)
        if not self.levels_zero_centered:
            self.setImageItem(self.parent.imgItem)
            self.vb.enableAutoRange()
        self.update_axis()

    def on_region_line_drag(self):
        """Process mouse drag event of lines marking ends of visible colorbar axis
        range in interactive mode."""
        if self.levels_zero_centered:
            self.is_region_line_dragged = True
            # self.region.blockSignals(True)
            l1 = self.region.lines[0]
            l2 = self.region.lines[1]
            if abs(l1.value()) != abs(l2.value()):
                if self.sender() == l1:
                    # l2.blockSignals(True)
                    l2.setValue(-l1.value())
                    # l2.blockSignals(False)
                if self.sender() == l2:
                    # l1.blockSignals(True)
                    l1.setValue(-l2.value())
                    # l1.blockSignals(False)
            # self.region.blockSignals(False)
        # if self.levels_zero_centered:
        #     self.levels_zero_centered = False
        #     self.actAlignZero.blockSignals(True)
        #     self.actAlignZero.setChecked(False)
        #     self.actAlignZero.blockSignals(False)
        #     self.update_axis()

    def on_region_line_drag_finish(self):
        """Run when dragging lines marking the histogram range is
        finished (interactive mode)."""
        self.is_region_line_dragged = False

    def save_ticks(self):
        """Save gradient ticks state."""
        ticks = self.gradient.listTicks()
        self.ticks_pos = []
        for t in ticks:
            self.ticks_pos.append(t[1])

    def restore_ticks(self):
        """Restore gradient ticks state."""
        self.gradient.blockSignals(True)
        prev_ticks = self.gradient.listTicks()
        for p in self.ticks_pos:
            self.gradient.addTick(p, self.gradient.getColor(p))
        for tick in prev_ticks:
            self.gradient.removeTick(tick[0])
        for t in self.gradient.listTicks():
            t[0].setVisible(self.interactive_mode)
        self.gradient.blockSignals(False)

    def on_gradient_change(self):
        """Process gradient change signal."""
        ticks = self.gradient.listTicks()
        tp = [t[1] for t in ticks]
        if len(set(tp) ^ set(self.ticks_pos)) > 2:
            self.restore_ticks()
        else:
            self.save_ticks()
        for t in self.gradient.listTicks():
            t[0].setVisible(self.interactive_mode)

    def set_color_scheme(self, scheme):
        """Set gradient color scheme."""
        tick_size = self.gradient.tickSize
        self.gradient.tickSize = 10
        self.gradient.loadPreset(scheme)
        self.gradient.allowAdd = True
        self.gradient.tickSize = tick_size
        # self.save_gradient_state()
        for t in self.gradient.ticks.items():
            t[0].setVisible(self.interactive_mode)

    def paint(self, p, *args):
        """ Simple method to supress drawing certain elements in
        non-interactive mode."""
        if not self.interactive_mode:
            pass
        else:
            super().paint(p, *args)

    def mouse_click_gradient(self, ev):
        """Process gradient mouse click event."""
        ev.accept()
        if ev.button() == QtCore.Qt.MouseButton.LeftButton:
            self.set_interactive_mode(not self.interactive_mode)
        # else:
        #     self.gradient.showMenu(ev)



class GrisMapContoursBar(HistogramLUTItem):
    """Interactive widget for generating map image contours.
    Includes histogram and gradient with axis and ticks that
    can be added/removed."""

    def __init__(self, parent, **kargs):
        """ Completely rewritten HistogramLUTItem constructor,
        no super().__init__"""
        pg.GraphicsWidget.__init__(self)
        self.lut = None
        self.imageItem = lambda: None  # fake a dead weakref
        self.levelMode = 'mono'
        self.rgbHistogram = False

        self.layout = QGraphicsGridLayout()
        self.setLayout(self.layout)
        self.layout.setContentsMargins(10, 0, 10, 0)
        self.layout.setSpacing(0)

        self.vb = pg.ViewBox(parent=self)
        self.vb.setBorder('k', width=1)
        self.vb.setMinimumHeight(45)
        self.vb.setMaximumHeight(45)
        self.vb.setMouseEnabled(x=True, y=False)
        self.vb.sigRangeChanged.connect(self.viewRangeChanged)

        self.curves = []
        self.ticks_pos = []

        self.gradient = pg.GradientEditorItem()
        # added only to shift gradient to the left
        self.gradient.tickSize = -2
        self.gradient.setOrientation('bottom')
        self.gradient.tickSize = 12
        self.gradient.tickPen = pg.mkPen(0.5)
        self.gradient.loadPreset('spectrum')
        # self.gradient.showMenu = lambda ev: None
        # self.gradient.sigTicksChanged.connect(self.update_curves)
        # self.gradient.sigGradientChanged.connect(self.update_curves)
        self.gradient.resizeEvent = self._resize_gradient
        # self.gradient.setFlag(self.gradient.ItemStacksBehindParent)
        # self.gradient.contextMenuClicked = self.change_preset
        # self.sigLookupTableChanged.connect(self.restore_ticks)
        # self.gradient.sigTicksChanged.connect(self.update_curves)
        # self.vb.setFlag(self.gradient.ItemStacksBehindParent)

        self.regions = [LinearRegionItem([0, 1], swapMode='block')]
        self.region = self.regions[0]
        self.region.setZValue(1000)
        self.region.lines[0].addMarker('<|>', 0.5)
        self.region.lines[1].addMarker('<|>', 0.5)
        self.region.sigRegionChanged.connect(self.update_axis)
        # self.region.sigRegionChanged.connect(self.regionChanging)
        self.region.sigRegionChangeFinished.connect(lambda: self.auto_update_curves(None))

        self.axis = AxisItem('top', maxTickLength=-10, parent=self)
        # self.axis.setScale(0.001)

        self.plots = [pg.PlotCurveItem(pen=(200, 200, 200, 100))]  # mono
        self.plot = self.plots[0]  # for backward compatibility.
        self.vb.setMenuEnabled(False)

        self.vb.addItem(self.plot)
        self.vb.addItem(self.region)
        self.layout.addItem(self.vb, 0, 0)
        self.layout.addItem(self.axis, 1, 0)
        self.layout.addItem(self.gradient, 2, 0)

        self.range = None
        self.paint = lambda p, *args: None

        self.fillHistogram(True)
        self.autoHistogramRange()

        self.line_width = 1
        self.is_map_visible = True
        self.is_enabled = True
        self.is_auto_updated = False
        self.parent = parent
        if parent.plot.imgItem is not None:
            self.set_image_item(parent.plot.imgItem)
            self.axis.setScale(10**parent.par.scale)
        self.parent.plot.imgItem.update()
        # self.is_enabled = False
        # self.gradient.sigTicksChanged.connect(self.update_curves)

    def set_image_item(self, img):
        """Set image source used for histogram/axis range."""
        if self.is_enabled:
            h = img.getHistogram()
            if h[0] is None:
                return
            self.plot.setData(*h)
            mn = h[0][0]
            mx = h[0][-1]
            self.region.setRegion([mn, mx])
            self.vb.enableAutoRange(x=None)
            self.vb.setXRange(mn, mx, padding=0.0)

            if self.is_auto_updated:
                self.update_map_image()
            # self.axis.setRange(mn, mx)
            # self.update_level_region()

    def _resize_gradient(self, ev):        
        """Modified resizeEvent with custom gradient size."""
        wlen = max(40, self.gradient.widgetLength())
        self.gradient.setLength(wlen)

    def hide_map(self, flag=True):
        """Show/hide map image."""
        self.is_map_visible = not flag
        self.parent.plot.imgItem.setVisible(not flag)

    def save_ticks(self):
        """Save gradient ticks positions."""
        ticks = self.gradient.listTicks()
        self.ticks_pos = []
        for t in ticks:
            self.ticks_pos.append(t[1])

    def restore_ticks(self):
        """Restore gradient ticks positions."""
        self.gradient.blockSignals(True)
        prev_ticks = self.gradient.listTicks()
        for p in self.ticks_pos:
            self.gradient.addTick(p, self.gradient.getColor(p))
        for tick in prev_ticks:
            self.gradient.removeTick(tick[0])
        self.gradient.blockSignals(False)
        # self.gradient.sigTicksChanged.connect(self.update_curves)

    def generate_curves(self, num=3):
        """Generate contours curves overlayed on the map image."""
        # self.gradient.blockSignals(True)
        try:
            self.gradient.sigTicksChanged.disconnect(self.update_curves)
            # self.gradient.sigGradientChanged.disconnect(self.restore_ticks)
        except:
            pass
        prev_ticks = self.gradient.listTicks()
        for i in range(num):
            x = i / float(num - 1)
            self.gradient.addTick(x, self.gradient.getColor(x))
        for tick in prev_ticks:
            self.gradient.removeTick(tick[0])
        self.delete_curves()
        for tick in self.gradient.listTicks():
            self.add_curve()
            self.curves[-1].setLevel(self.get_tick_level(tick))
            self.curves[-1].setPen(tick[0].color,
                                   width=self.line_width)
        self.save_ticks()
        # self.gradient.blockSignals(False)
        # if not self.is_enabled:
        self.gradient.sigTicksChanged.connect(self.update_curves)
        # self.is_enabled = True
        # self.gradient.sigGradientChanged.connect(self.restore_ticks)

    def add_curve(self):
        """Add new contour line curve."""
        self.curves.append(pg.IsocurveItem())
        # self.curves[-1].setParentItem(self.parent.imgItem)
        self.curves[-1].setData(self.parent.plot.data)
        self.curves[-1].setVisible(self.is_enabled)
        self.parent.plot.addItem(self.curves[-1])

    def hide_curves(self, flag=True):
        """Show/hide contours."""
        self.is_enabled = not flag
        for c in self.curves:
            c.setVisible(not flag)
        # if self.is_enabled:
        #     self.set_image_item(self.parent.imgItem)
            # self.update_level_region()
            # self.update_curves()

    def delete_curves(self):
        """Delete all contour curves."""
        if self.curves:
            for c in self.curves:
                self.parent.plot.removeItem(c)
            self.curves = []

    def get_tick_level(self, t):
        """Converts ticks scale value [0, 1] to the current gradient scale."""
        min_lvl, max_lvl = self.region.getRegion()
        return min_lvl + t[1] * (max_lvl - min_lvl)

    def auto_update_curves(self, flag=None):
        """Turn on/off contours auto update on map image change."""
        if flag is not None:
            self.is_auto_updated = flag
        if self.is_auto_updated:
            self.update_map_image()

    def update_curves(self):
        """Update map contour curves when ticks positions, colors or line width
        change."""
        ticks = self.gradient.listTicks()
        if len(ticks) > 1:
            ticks_pos_new = [t[1] for t in ticks]
            if len(set(ticks_pos_new) ^ set(self.ticks_pos)) > 2:
                self.restore_ticks()
                ticks = self.gradient.listTicks()
        self.save_ticks()

        if self.is_enabled:
            tick_levels = [self.get_tick_level(t) for t in ticks]
            curve_levels = []
            if self.curves:
                curve_levels = [c.level for c in self.curves]
            diff1 = list(set(tick_levels) - set(curve_levels))
            diff2 = list(set(curve_levels) - set(tick_levels))
            if sorted(tick_levels) != sorted(curve_levels):
                if len(tick_levels) > len(curve_levels):
                    # add curve (only one at a time possible)
                    self.add_curve()
                    self.curves[-1].setLevel(diff1[0])
                elif len(tick_levels) < len(curve_levels):
                    # remove curve (only one at a time possible)
                    idx = [i for i, v in enumerate(curve_levels) if v == diff2[0]][0]
                    self.parent.plot.removeItem(self.curves[idx])
                    self.curves.remove(self.curves[idx])
                else:
                    # change isocurve  (only one at a time possible)
                    idx1 = [i for i, v in enumerate(tick_levels) if v == diff1[0]][0]
                    idx2 = [i for i, v in enumerate(curve_levels) if v == diff2[0]][0]
                    self.curves[idx2].setLevel(tick_levels[idx1])

            for c in self.curves:
                for i, tl in enumerate(tick_levels):
                    if c.level == tl:
                        c.setPen(ticks[i][0].color,
                                 width=self.line_width)
                        break

    def update_map_image(self):
        """Update source map image and rebuild contours."""
        if self.is_enabled:
            ticks = self.gradient.listTicks()
            if len(self.curves) == len(ticks):
                # keep ticks pos, get new curves levels
                for i, tick in enumerate(ticks):
                    self.curves[i].setData(self.parent.plot.data)
                    self.curves[i].setLevel(self.get_tick_level(tick))

    def update_axis(self):
        """Update axis range."""
        rmin, rmax = self.region.getRegion()
        self.axis.setRange(rmin, rmax)


class GrisMapProfilePlot(PlotItem):
    """Profile plotting widget connected to GrisMapPlot and GrisMapProfile.
    Displayed in separate GraphicsLayoutWidget."""

    def __init__(self, parent, **kargs):
        super().__init__(**kargs)
        self.title = None
        self.unit = u.pix
        self.label_x = ''
        self.label_y = ''
        self.label_style = {'color': 'k',
                            'font-size': '10pt'}
        self.curveItems = []
        self.parent = parent
        self._prepare()

    def _prepare(self):
        """Set plot properties, border, axes and ticks."""
        self.setContentsMargins(0, 0, 0, 0)
        # self.vb.setSpacing(0)
        # self.setBackgroundColor('w')
        self.vb.setBorder(pg.mkPen('k', width=1))
        self.enableAutoRange('xy')
        self.autoRange(padding=0.0)
        self.setMouseEnabled(x=False, y=False)
        self.hideButtons()
        self.setMenuEnabled(False)

        self.showAxis('right')
        self.showAxis('top')
        tickFont = QtGui.QFont()
        tickFont.setPointSize(10)
        for pos in plt_axes:
            ax = self.getAxis(pos)
            ax.showLabel(False)
            ax.setPen('k', width=1)
            ax.setTextPen('k')
            ax.setStyle(tickLength=-10, tickFont=tickFont, showValues=False)
            if pos == 'left' or pos == 'bottom':
                ax.setStyle(showValues=True)
        self.set_x_axis()
        self.set_y_axis()
        self.showGrid(x=True, y=True, alpha=0.1)

    def set_x_axis(self, label=None):
        """Set x-axis label text, auto if None (with unit of parent plot)
        or custom."""
        if label is not None:
            self.label_x = label
        else:
            self.label_x = f'Position [{self.parent.plot.unit_xy}]'
        ax = self.getAxis('bottom')
        ax.setLabel(self.label_x, **self.label_style)

    def set_y_axis(self, label=None, scale=None):
        """Set y-axis label text and scale, auto if None (with scale taken from 
        parent plot) or custom ones."""
        if label is not None:
            self.label_y = label
        else:
            self.label_y = 'Value'
        ax = self.getAxis('left')
        ax.setLabel(self.label_y, **self.label_style)
        if scale is not None:
            ax.setScale(scale)
        else:
            ax.setScale(10**self.parent.par.scale)

    def add_curve(self, data=None, color=None):
        """Add new profile curve with a given color."""
        self.curveItems.append(PlotDataItem(data=data, antialias=True))
        self.addItem(self.curveItems[-1])
        # self.specItems[-1].setPen(width=2)  # , cosmetic=True)
        if color is not None:
            self.curveItems[-1].setPen(pg.mkPen(color, width=1))

    def set_curve_data(self, i, data):
        """Set data for selected profile curve."""
        npts = len(data)
        xsc = (np.arange(npts) - npts / 2) * self.parent.plot.scale_x
        self.curveItems[i].setData(x=xsc, y=data)
        self.autoRange(padding=0.0)

    def set_curve_visible(self, i, flag):
        """Set profile curve visibility."""
        self.curveItems[i].setVisible(flag)
        # if flag:
        self.autoRange(padding=0.0)

    def set_curve_color(self, i, color):
        """Set color for selected profile curve line."""
        self.curveItems[i].setPen(color)
