from collections import namedtuple
from numpy import sqrt
import astropy.units as u

# Define main plot parameter properties, where:
# name - parameter name as shown in the combo-boxes
# descr - parameter name as shown in the plot label
# unit - unit of measure
# scale - axis scale power (10^)
# zero-centered - if y axis is zero-centered
# normalized - if parameter is normalized (to continuum)
# func - function to determine parameter value using input and other derived 
#        parameter values

PlotParam = namedtuple("PlotParam",
                       ['name',
                        'descr',
                        'unit',
                        'scale',
                        'zero_centered',
                        'normalized',
                        'func'],
                       defaults=['', '', '', 0, False, False, None])

PlotPars = [
    PlotParam('I', 'Stokes I', u.adu, -3, False, False, lambda d: d[0]),
    PlotParam('Q', 'Stokes Q', u.adu, -3, True, False, lambda d: d[1]),
    PlotParam('U', 'Stokes U', u.adu, -3, True, False, lambda d: d[2]),
    PlotParam('V', 'Stokes V', u.adu, -3, True, False, lambda d: d[3]),
    PlotParam('Q/I', 'Stokes Q/I', u.pct, 0, True, False, lambda d: 100. * d[1] / d[0]),
    PlotParam('U/I', 'Stokes U/I', u.pct, 0, True, False, lambda d: 100. * d[2] / d[0]),
    PlotParam('V/I', 'Stokes V/I', u.pct, 0, True, False, lambda d: 100. * d[3] / d[0]),
    PlotParam('DoLP', 'Degree of linear polarization', u.pct, 0, False, False, lambda d: 100. * sqrt(d[1] ** 2 + d[2] ** 2) / d[0]),
    PlotParam('DoP', 'Degree of polarization', '', 2, False, False, lambda d: sqrt(d[1] ** 2 + d[2] ** 2 + d[3] ** 2) / d[0]),
    PlotParam('I/Ic', 'Stokes I/Ic', '', 0, False, True, lambda d, c: d[0] / c),
    PlotParam('Q/Ic', 'Stokes Q/Ic', u.pct, 0, True, True, lambda d, c: 100. * d[1] / c),
    PlotParam('U/Ic', 'Stokes U/Ic', u.pct, 0, True, True, lambda d, c: 100. * d[2] / c),
    PlotParam('V/Ic', 'Stokes V/Ic', u.pct, 0, True, True, lambda d, c: 100. * d[3] / c),
    ]

ParIntensity = 'I'
ParIntensityNorm = 'I/Ic'
