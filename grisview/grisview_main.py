import sys
import argparse
import os
from os import path
from pathlib import Path

from PyQt6 import QtWidgets, QtCore
from PyQt6.QtCore import QLineF, QPointF, Qt, QRegularExpression, QFileInfo
from PyQt6.QtGui import QKeySequence, QShortcut
from PyQt6.QtWidgets import QFileDialog, QColorDialog, QSlider, QCheckBox, QMenu, QWidgetAction, QLabel, \
    QRadioButton, QVBoxLayout, QWidget
import pyqtgraph as pg
import pyqtgraph.exporters

import numpy as np
from astropy.coordinates import SkyCoord
from sunpy.coordinates.frames import Helioprojective
from sunpy.coordinates.sun import earth_distance
import astropy.units as u
import toml
import regex as re

from grisview.gui.grisview_form import Ui_GrisView
from grisview.gui.shortcuts_form import Ui_GrisViewShortcuts
from grisview.manual.browser import GrisViewHelp
from grisview.lib.grisobs import GrisObs, GrisObsDir, GrisInvMaps, GrisInvProfile
from grisview.lib.headers import GrisViewHeaders
from grisview.lib.plotpars import *
from grisview.lib.plotmap import GrisPlotMap, GrisMapPOI, GrisMapROI, GrisMapProfile, GrisMapMeasure
from grisview.lib.plotspec import GrisPlotSpec
from grisview.lib.misc import show_info, show_error, show_question_yes
from grisview.lib.dialogs import ImageSaveDialog, GrisObsOpenDialog
from grisview.dat import fts_atlas
from grisview.__version__ import __version__


class GrisViewGUI(QtWidgets.QMainWindow):    

    def __init__(self, obs_file=''):
        """Initialize main window and global variables."""
        super(GrisViewGUI, self).__init__()
        self.ui = Ui_GrisView()
        self.ui.setupUi(self)
        self.setGeometry(400, 100, 1000, 600)
        self.show()

        self.obs_dir = ''
        self.gris_obs = None
        self.gris_obs_pre = None
        self.session_config = None
        self.gris_inv_maps = []
        self.gris_inv_profiles = []
        self.is_obs_open = False

        self.pltMaps = []
        self.pltSpecs = []
        self.pltMapCurr = None
        self.pltSpecCurr = None

        self.map_id_curr = 0
        self.waveln_curr = 0

        self.map_pars_selected = []
        self.map_pars_prev = []
        self.num_map_pars_selected = 0
        self.map_par_curr = -1

        self.spec_pars_selected = []
        self.spec_pars_prev = []
        self.spec_pars_save = []
        self.num_spec_pars_selected = 0
        self.spec_par_curr = -1
        self.spec_view_graph_conf = []

        self.maps_opts = dict(
            panel_quickinfo_visible=True,
            panel_poi_visible=True,
            panel_measures_visible=True,
            panel_profiles_visible=False,
            panel_contours_visible=False,
            poi_visible=True,
            roi_visible=True,
            measures_visible=True,
            view_mode='multi',
            unit_xy='px',
            unit_measures='px',
            axes_visible=True,
            compass_visible=True,
            grids_visible=False,
            avg_box_size=1)

        self.spec_opts = dict(
            panel_quickinfo_visible=True,
            panel_markers_visible=True,
            view_mode='multi',
            lines_visible=False,
            grids_visible=False,
            markers_visible=False,
            waveln_offset=0.0,
        )

        self.dlgShortcuts = None
        self.dlgHeaders = None
        self.dlgOpenObs = None
        self.dlgHelp = None

        pg.setConfigOptions(antialias=True, foreground='k')

        self.init_ui()

        self.create_default_map_plots()
        self.create_default_spec_plots()
        self.init_map_plots_widget()
        self.init_spec_plots_widget()

        self.reset_panels()

    def init_ui(self):
        """Initialize UI."""
        self.init_ui_menu()
        self.init_ui_maps_view_ctrls()
        self.init_ui_maps_view_options()
        self.init_ui_maps_poi()
        self.init_ui_maps_roi()
        self.init_ui_maps_measures()
        self.init_ui_maps_profiles()
        self.init_ui_maps_contours()
        self.init_ui_spec_view_ctrls()
        self.init_ui_spec_view_options()
        self.init_ui_spec_lines_markers()
        self.init_ui_spec_cont_fit()
        self.init_ui_spec_style()
        self.ui.statusbar.hide()

    def init_ui_menu(self):
        """Connect main menu actions to slots."""
        # File menu
        self.ui.actOpenObs.triggered.connect(self.open_obs)
        self.ui.actViewHeaders.triggered.connect(self.show_headers_dialog)
        self.ui.actLoadSession.triggered.connect(self.load_session)
        self.ui.actSaveSession.triggered.connect(self.save_session)
        self.ui.actMapsLoadInversions.triggered.connect(self.load_inversion_maps)
        self.ui.actSpecLoadFitProfiles.triggered.connect(self.load_inversion_profiles)
        self.ui.actAppExit.triggered.connect(self.exit_app)
        self.flag_menu_exit = False

        # Maps menu
        self.ui.actMapsQuickInfo.toggled.connect(self.show_maps_quickinfo_panel)
        self.ui.actMapsPOI.toggled.connect(self.show_maps_poi_panel)
        self.ui.actMapsMeasures.toggled.connect(self.show_maps_measures_panel)
        self.ui.actMapsContours.toggled.connect(self.toggle_map_contours_panel)
        self.ui.actMapsProfiles.toggled.connect(self.toggle_map_profiles_panel)

        self.ui.actMapsRoiQS.toggled.connect(self.toggle_maps_roi_qs_panel)

        self.ui.actMapsFitToView.triggered.connect(self.fit_maps_to_view)
        self.ui.actMapsAxesLabels.triggered.connect(self.toggle_maps_axes_labels)
        self.ui.actMapsCompass.toggled.connect(self.toggle_maps_compass)
        self.ui.actMapsGrids.toggled.connect(self.toggle_maps_grids)

        self.ui.actMapsExportAsImage.triggered.connect(self.export_maps_as_image)
        self.ui.actMapsOptions.toggled.connect(self.ui.panelMapsOptions.setVisible)

        # Spectra menu
        self.ui.actSpecQuickInfo.toggled.connect(self.show_spec_quickinfo_panel)
        self.ui.actSpecMarkers.toggled.connect(self.show_spec_markers_panel)
        self.ui.actSpecAddMark.triggered.connect(self.add_spec_marker)
        self.ui.actSpecDelMark.triggered.connect(self.delete_spec_marker)
        self.ui.actSpecPrevMark.triggered.connect(self.goto_spec_prev_marker)
        self.ui.actSpecNextMark.triggered.connect(self.goto_spec_next_marker)
        self.ui.actSpecRelWaveln.toggled.connect(self.set_spec_wl_ref)
        self.ui.actSpecGrids.toggled.connect(self.toggle_spec_grids)
        self.ui.actSpecExportAsImage.triggered.connect(self.export_specs_as_image)
        self.ui.actSpecOptions.toggled.connect(self.ui.panelSpecOptions.setVisible)

        self.ui.actSpecAvg.toggled.connect(self.toggle_spec_avg)
        self.ui.actSpecQS.toggled.connect(self.toggle_spec_qs)
        self.ui.actSpecContFit.toggled.connect(self.toggle_spec_cont_fit_panel)
        self.ui.actSpecAtlas.toggled.connect(self.toggle_spec_fts)

        self.ui.actSpecGoToCursor.triggered.connect(self.spec_move_view_to_cursor)
        self.ui.actSpecMoveCursorToView.triggered.connect(self.spec_move_cursor_to_view)
        self.ui.actSpecLinkViewToCursor.triggered.connect(self.spec_link_view_to_cursor)
        self.ui.actSpecMoveCursorLeft.triggered.connect(self.spec_move_cursor_left)
        self.ui.actSpecMoveCursorRight.triggered.connect(self.spec_move_cursor_right)
        self.ui.actSpecMoveCursorLeftFast.triggered.connect(self.spec_move_cursor_left_fast)
        self.ui.actSpecMoveCursorRightFast.triggered.connect(self.spec_move_cursor_right_fast)
        self.ui.actSpecZoomInHoriz.triggered.connect(self.spec_zoom_in_horiz)
        self.ui.actSpecZoomOutHoriz.triggered.connect(self.spec_zoom_out_horiz)
        self.ui.actSpecZoomAllHoriz.triggered.connect(self.spec_zoom_all_horiz)

        self.ui.actSpecPrevLine.triggered.connect(self.goto_spec_prev_line)
        self.ui.actSpecNextLine.triggered.connect(self.goto_spec_next_line)

        # Help menu
        self.ui.actUserManual.triggered.connect(self.show_help)
        self.ui.actHelpShortcuts.triggered.connect(self.toggle_shortcuts)
        self.ui.actHelpAbout.triggered.connect(self.about_app)

    def init_ui_maps_view_ctrls(self):
        """Initialize top control panel for maps."""

        # Init plot parameters toggle buttons and combo boxes
        self.chkMapPars = []
        self.cboMapPars = []
        for i in range(4):
            chk = getattr(self.ui, 'btnMapPar' + str(i + 1))
            chk.setProperty('Index', i)
            chk.toggled.connect(self.toggle_map_plot)
            cbo = getattr(self.ui, 'cboMapPar' + str(i + 1))
            cbo.setProperty('Index', i)
            cbo.currentIndexChanged.connect(self.update_plot_maps_widget)
            self.cboMapPars.append(cbo)
            self.chkMapPars.append(chk)

        # Init plot parameter prev/next buttons and shortcuts
        self.ui.wgtMapParCurr.setVisible(False)
        self.ui.btnMapParPrev.clicked.connect(self.goto_map_par_prev)
        self.ui.btnMapParNext.clicked.connect(self.goto_map_par_next)
        sht1 = QShortcut(QKeySequence('PgDown'), self)
        sht1.activated.connect(self.goto_map_par_next)
        sht2 = QShortcut(QKeySequence('PgUp'), self)
        sht2.activated.connect(self.goto_map_par_prev)

        # Create view mode drop-down submenu
        self.ui.mapOptsMenu = QMenu(self)
        menu = self.ui.mapOptsMenu
        layout = QVBoxLayout()
        layout.setSpacing(2)
        layout.setContentsMargins(8, 4, 4, 4)
        widget = QWidget()
        widget.setLayout(layout)
        wgt_act = QWidgetAction(menu)
        wgt_act.setDefaultWidget(widget)
        menu.addAction(wgt_act)
        self.ui.btnMapViewOptions.setMenu(menu)

        lbl = QLabel("Select mode:")
        self.ui.rbMapViewMode1 = QRadioButton("One-by-one")
        self.ui.rbMapViewMode2 = QRadioButton("Side-by-side")
        self.ui.rbMapViewMode2.setChecked(True)
        self.ui.rbMapViewMode1.toggled.connect(self.set_maps_view_mode_single)
        self.ui.rbMapViewMode2.toggled.connect(self.set_maps_view_mode_multi)
        layout.addWidget(lbl)
        layout.addWidget(self.ui.rbMapViewMode1)
        layout.addWidget(self.ui.rbMapViewMode2)

        # Init time series navigation controls
        self.ui.comboMapID.currentIndexChanged.connect(self.set_map_id_curr)
        self.ui.sliderMapID.valueChanged.connect(self.set_map_id_curr)
        self.ui.btnMapPrev.clicked.connect(self.set_map_id_prev)
        self.ui.btnMapNext.clicked.connect(self.set_map_id_next)
        self.ui.btnMapRemove.clicked.connect(self.remove_map)

    def init_ui_maps_view_options(self):
        """Init bottom option panel for maps."""
        self.ui.btnViewMapUnits1.clicked.connect(self.change_maps_unit_xy)
        self.ui.btnViewMapUnits2.clicked.connect(self.change_maps_unit_xy)

    def init_ui_maps_poi(self):
        """Initialize points of interest (POI) panel UI elements."""
        # Count number of POI control widgets
        self.num_maps_poi = len(self.findChildren(QCheckBox, QRegularExpression(r'chkPOI\d')))

        self.poi_colors = []
        for k in range(self.num_maps_poi):
            chk = getattr(self.ui, 'chkPOI' + str(k + 1))
            chk.setProperty('idx', k)
            chk.stateChanged.connect(self.toggle_maps_poi)
            btn = getattr(self.ui, 'clrPOI' + str(k + 1))
            btn.setProperty('idx', k)
            btn.clicked.connect(self.change_maps_plot_wgt_color)
            self.poi_colors.append(btn.palette().button().color())

        self.ui.chkShowPOI.stateChanged.connect(self.toggle_all_maps_poi)
        self.ui.btnResetPOI.clicked.connect(self.reset_all_maps_poi)
        self.ui.comboPOIAvgBox.currentIndexChanged.connect(self.change_maps_poi_avg_box)

    def init_ui_maps_roi(self):
        """Initialize regions of interest (ROI) (+ Quiet Sun ROI) UI elements."""
        self.num_maps_roi = len(self.findChildren(QCheckBox, 
            QRegularExpression(r'chkROI\d')))

        self.roi_colors = []
        for k in range(self.num_maps_roi):
            chk = getattr(self.ui, 'chkROI' + str(k + 1))
            chk.setProperty('idx', k)
            chk.stateChanged.connect(self.toggle_maps_roi)
            btn = getattr(self.ui, 'clrROI' + str(k + 1))
            btn.clicked.connect(self.change_maps_plot_wgt_color)
            self.roi_colors.append(btn.palette().base().color())

        self.ui.chkShowROI.stateChanged.connect(self.toggle_all_maps_roi)
        self.ui.chkHideROIHandles.stateChanged.connect(self.hide_maps_roi_handles)
        self.ui.btnResetROI.clicked.connect(self.reset_all_maps_roi)

        self.ui.panelMapsRoiQS.setVisible(False)
        self.ui.btnAddRoiQS.clicked.connect(self.add_maps_roi_qs)
        self.ui.btnClosePanelRoiQS.clicked.connect(self.ui.actMapsRoiQS.trigger)

    def init_ui_maps_measures(self):
        """Initialize maps measures panel UI elements."""
        self.num_maps_measures = len(self.findChildren(QCheckBox, 
            QRegularExpression(r'chkMeasure\d')))

        self.measure_colors = []
        for k in range(self.num_maps_measures):
            chk = getattr(self.ui, 'chkMeasure' + str(k + 1))
            chk.setProperty('idx', k)
            btn = getattr(self.ui, 'clrMeasure' + str(k + 1))
            chk.stateChanged.connect(self.toggle_maps_measure)
            btn.clicked.connect(self.change_maps_plot_wgt_color)
            self.measure_colors.append(btn.palette().base().color())

        for i in range(6):
            btn = getattr(self.ui, 'btnMeasuresUnits' + str(i + 1))
            btn.clicked.connect(self.change_maps_measures_unit)

        self.ui.chkShowMeasures.stateChanged.connect(self.toggle_all_maps_measures)
        self.ui.btnResetMeasures.clicked.connect(self.reset_all_maps_measures)

    def init_ui_maps_profiles(self):
        """Initialize maps profile panel UI elements."""
        self.num_maps_profiles = len(self.findChildren(QCheckBox, 
            QRegularExpression(r'chkProfile\d')))

        self.profile_colors = []
        for k in range(self.num_maps_profiles):
            btn = getattr(self.ui, 'clrProfile' + str(k + 1))
            btn.clicked.connect(self.change_maps_plot_wgt_color)
            chk = getattr(self.ui, 'chkProfile' + str(k + 1))
            chk.setProperty('Index', k)
            chk.stateChanged.connect(self.toggle_map_profile)
            self.profile_colors.append(btn.palette().base().color())

        self.ui.btnResetProfiles.clicked.connect(self.reset_map_profiles)
        self.ui.comboProfileAvgWidth.currentIndexChanged.connect(
            self.change_map_profile_avg_width)
        self.ui.orientProfile1.clicked.connect(self.change_map_profile_orient)
        self.ui.orientProfile2.clicked.connect(self.change_map_profile_orient)
        self.ui.chkLinkProfiles.stateChanged.connect(self.link_map_profiles)

        wgt = self.ui.wgtMapProfiles
        wgt.setBackground('w')
        wgt.ci.layout.setContentsMargins(4, 5, 5, 4)
        wgt.ci.layout.setSpacing(0)

    def init_ui_maps_contours(self):
        """Initialize maps contours panel UI elements."""
        self.ui.checkShowContours.stateChanged.connect(self.toggle_map_contours)
        self.ui.checkContoursHideMap.stateChanged.connect(self.toggle_map_contours_image)
        self.ui.btnGenerateContours.clicked.connect(self.generate_map_contours)
        self.ui.btnUpdateContours.clicked.connect(self.update_map_contours_curves)
        self.ui.chkAutoUpdateContours.stateChanged.connect(self.auto_update_map_contours)
        self.ui.sliderContoursNumLevels.valueChanged.connect(self.change_map_contours_levels)
        self.ui.comboContoursWidth.currentIndexChanged.connect(self.change_map_contours_width)
        self.ui.graphContoursControl.setBackground('w')

    def init_ui_spec_view_ctrls(self):
        """Initialize top panel controls for spectra."""
        self.chkSpecPars = []
        self.cboSpecPars = []
        for i in range(4):
            chk = getattr(self.ui, 'btnSpecPar' + str(i + 1))
            chk.setProperty('Index', i)
            chk.toggled.connect(self.toggle_spec_plot)
            cbo = getattr(self.ui, 'cboSpecPar' + str(i + 1))
            cbo.setProperty('Index', i)
            cbo.currentIndexChanged.connect(self.update_plot_specs_widget)
            self.cboSpecPars.append(cbo)
            self.chkSpecPars.append(chk)
        # self.ui.btnViewSpecUnits1.clicked.connect(self.change_view_spec_unit)
        self.ui.btnViewSpecUnits2.clicked.connect(self.change_view_spec_unit)
        self.ui.btnViewSpecUnits3.clicked.connect(self.change_view_spec_unit)
        self.ui.sliderWaveln.valueChanged.connect(self.set_waveln_curr)

        # Create view mode drop-down submenu
        self.ui.specOptsMenu = QMenu(self)
        menu = self.ui.specOptsMenu
        layout = QVBoxLayout()
        layout.setSpacing(2)
        layout.setContentsMargins(8, 4, 4, 4)
        widget = QWidget()
        widget.setLayout(layout)
        wgt_act = QWidgetAction(menu)
        wgt_act.setDefaultWidget(widget)
        menu.addAction(wgt_act)

        lbl = QLabel("Select mode:")
        self.ui.rbSpecViewMode1 = QRadioButton("One-by-one")
        self.ui.rbSpecViewMode2 = QRadioButton("Side-by-side")
        self.ui.rbSpecViewMode2.setChecked(True)
        self.ui.rbSpecViewMode1.toggled.connect(self.set_specs_view_mode_single)
        self.ui.rbSpecViewMode2.toggled.connect(self.set_specs_view_mode_multi)
        layout.addWidget(lbl)
        layout.addWidget(self.ui.rbSpecViewMode1)
        layout.addWidget(self.ui.rbSpecViewMode2)

    def init_ui_spec_view_options(self):
        """Init bottom option panel for spectra."""
        self.ui.wgtSpecParCurr.setVisible(False)
        self.ui.btnSpecParPrev.clicked.connect(self.goto_spec_par_prev)
        self.ui.btnSpecParNext.clicked.connect(self.goto_spec_par_next)
        sht1 = QShortcut(QKeySequence('Shift+PgDown'), self)
        sht1.activated.connect(self.goto_spec_par_next)
        sht2 = QShortcut(QKeySequence('Shift+PgUp'), self)
        sht2.activated.connect(self.goto_spec_par_prev)

        self.ui.btnSpecViewOptions.setMenu(self.ui.specOptsMenu)

    def init_ui_spec_lines_markers(self):
        """Initialize spectral lines and markers panel UI control elements."""
        self.ui.chkShowSpecLines.toggled.connect(self.toggle_spec_lines)
        # self.ui.colorSpecLines.clicked.connect(self.change_spec_lines_color)
        # self.ui.sliderLinesLessMore.valueChanged.connect(self.update_spec_lines)
        self.ui.btnSpecPrevLine.clicked.connect(self.goto_spec_prev_line)
        self.ui.btnSpecNextLine.clicked.connect(self.goto_spec_next_line)
        self.ui.listSpecLines.currentRowChanged.connect(self.goto_spec_line)
        # self.lines_color = self.ui.colorSpecLines.palette().base().color()

        self.ui.chkShowSpecMarkers.toggled.connect(self.toggle_spec_markers)
        self.ui.btnSpecAddMark.clicked.connect(self.add_spec_marker)
        self.ui.btnSpecDelMark.clicked.connect(self.delete_spec_marker)
        self.ui.btnSpecPrevMark.clicked.connect(self.goto_spec_prev_marker)
        self.ui.btnSpecNextMark.clicked.connect(self.goto_spec_next_marker)
        self.ui.btnSpecClearMarks.clicked.connect(self.clear_spec_markers)
        self.ui.listSpecsMarkers.currentRowChanged.connect(self.goto_spec_marker)

    def init_ui_spec_cont_fit(self):
        """Initialize spectrum continuum fitting panel elements."""
        # self.ui.panelSpecContFit.setVisible(False)
        self.ui.btnAddContReg.clicked.connect(self.add_spec_cont_reg)
        self.ui.btnResetContReg.clicked.connect(self.reset_spec_cont_regs)
        self.ui.btnDelContReg.clicked.connect(self.del_spec_cont_reg)
        self.ui.sboContFitDegree.valueChanged.connect(self.update_spec_cont_fit_degree)
        self.ui.dsbContFitMult.valueChanged.connect(self.update_spec_cont_fit_mult)
        self.ui.sliContFitMult.valueChanged.connect(self.update_spec_cont_fit_mult)
        self.ui.chkContFitShowFTS.toggled.connect(self.update_spec_cont_fit_fts_atlas)
        self.ui.dsbSpecWavelnOffset.valueChanged.connect(self.set_spec_wl_offset)
        self.ui.sliSpecWavelnOffset.valueChanged.connect(self.set_spec_wl_offset)
        self.ui.btnCloseContFit.clicked.connect(self.ui.actSpecContFit.trigger)
        self.ui.lstContFitPars.clear()

    def init_ui_spec_style(self):
        """Initialize spectral graphs style UI elements."""
        self.ui.chkShowGraph.stateChanged.connect(self.toggle_spec_graph)
        self.ui.cboGraphCurr.currentTextChanged.connect(self.select_spec_graph)
        self.ui.btnGraphColor.clicked.connect(self.change_spec_graph_color)
        self.ui.cboGraphWidth.currentTextChanged.connect(self.change_spec_graph_width)
        self.ui.cboGraphStyle.currentIndexChanged.connect(self.change_spec_graph_style)

    def reset_panels(self):
        """Reset all panels state to default workspace."""
        flag = self.is_obs_open

        self.ui.actMapsPOI.setChecked(True)
        self.ui.actMapsMeasures.setChecked(True)
        self.ui.actMapsContours.setChecked(False)
        self.ui.actMapsProfiles.setChecked(False)
        self.ui.actMapsQuickInfo.setChecked(True)
        self.ui.actMapsRoiQS.setChecked(False)
        self.ui.actMapsOptions.setChecked(False)

        self.ui.actSpecQuickInfo.setChecked(True)
        self.ui.actSpecMarkers.setChecked(True)
        self.ui.actSpecRelWaveln.setChecked(False)
        self.ui.actSpecContFit.setChecked(False)
        self.ui.actSpecGrids.setChecked(False)
        self.ui.actSpecOptions.setChecked(False)

        self.ui.actSaveSession.setEnabled(flag)
        self.ui.actViewHeaders.setEnabled(flag)
        self.ui.menuMaps.setEnabled(flag)
        self.ui.menuSpectra.setEnabled(flag)
        self.ui.menuImport.setEnabled(flag)
        self.ui.menuExport.setEnabled(flag)

        self.ui.panelMapsQuickInfo.setEnabled(flag)
        self.ui.panelMapsViewControls.setEnabled(flag)
        self.ui.panelMapsPOI.setEnabled(flag)
        self.ui.panelMapsMeasures.setEnabled(flag)

        self.ui.panelSpecViewControls.setEnabled(flag)
        self.ui.panelSpecQuickInfo.setEnabled(flag)
        self.ui.panelSpecMarkers.setEnabled(flag)

    def create_default_map_plots(self):
        """Create map plot instances for predefined parameters."""
        pg.setConfigOption('imageAxisOrder', 'row-major')
        self.pltMaps = []
        for par in PlotPars:
            self.add_map_plot(par)

    def add_map_plot(self, par):
        """Create map plot instance for a given parameter name."""
        self.pltMaps.append(GrisPlotMap(par))
        plt = self.pltMaps[-1]
        plt.sig_hover_data.connect(self.on_map_plots_hover)
        for j in range(self.num_maps_poi):
            plt.add_poi(color=self.poi_colors[j])
        for j in range(self.num_maps_roi):
            plt.add_roi(color=self.roi_colors[j])
        for j in range(self.num_maps_measures):
            plt.add_measure(color=self.measure_colors[j])
        for j in range(self.num_maps_profiles):
            plt.add_profile(color=self.profile_colors[j])
        self.create_map_plot_menu(plt)
        self.link_plot_widgets()

    def create_default_spec_plots(self):
        """Create spectrum plots with predefined average, poi, roi and
        quiet Sun graphs."""
        self.pltSpecs = []
        for par in PlotPars:
            self.pltSpecs.append(GrisPlotSpec(par))

            self.pltSpecs[-1].add_graph('Average', color=(90, 90, 90))
            self.pltSpecs[-1].add_graph('Quiet Sun', color=(170, 0, 0))
            if par.name == ParIntensityNorm or \
               par.name == ParIntensity:
                self.pltSpecs[-1].add_graph('FTS Atlas', color=(30, 30, 30))
                self.pltSpecs[-1].set_graph_visible('FTS Atlas', False)

            for j in range(self.num_maps_poi):
                self.pltSpecs[-1].add_graph('POI' + str(j + 1), self.poi_colors[j])

            for j in range(self.num_maps_roi):
                self.pltSpecs[-1].add_graph('ROI' + str(j + 1), self.roi_colors[j])

    def init_map_plots_widget(self):
        """Initialize map plots widget."""
        wgt = self.ui.wgtMaps
        wgt.setBackground('w')
        # wgt.ci.layout.setContentsMargins(10, 10, 10, 10)
        wgt.ci.layout.setSpacing(0)
        wgt.resizeEvent = self.on_map_wgt_resize
        # for i, plt in enumerate(self.pltMaps):
        #     self.create_map_plot_menu(plt)
        #     self.link_plot_widgets(plt)

    def create_map_plot_menu(self, plt):
        """Create context menu for map plots widget."""
        cx_menu = plt.plot.vb.menu
        cx_menu.addAction(self.ui.actMapsFitToView)
        cx_menu.addSeparator()
        cx_menu.addAction(self.ui.actMapsQuickInfo)
        cx_menu.addAction(self.ui.actMapsPOI)
        cx_menu.addAction(self.ui.actMapsMeasures)
        cx_menu.addAction(self.ui.actMapsContours)
        cx_menu.addAction(self.ui.actMapsProfiles)
        cx_menu.addSeparator()
        cx_menu.addAction(self.ui.actMapsRoiQS)
        cx_menu.addSeparator()
        # cx_menu.addAction(self.ui.actMapsColorbars)
        cx_menu.addAction(self.ui.actMapsAxesLabels)
        cx_menu.addAction(self.ui.actMapsCompass)
        cx_menu.addAction(self.ui.actMapsGrids)
        cx_menu.addSeparator()
        cx_menu.addAction(self.ui.actMapsOptions)
        # cx_menu.addAction(self.ui.actMapsExportAsImage)

    def link_plot_widgets(self):
        """Link all graphical items (POI, ROI, measures and profiles) to be
        in sync for all map plots."""
        plt0 = self.pltMaps[-1]
        if len(self.pltMaps) > 1:
            for plt in self.pltMaps[:-1]:
                plt.link_poi_to(plt0)
                plt.link_roi_to(plt0)
                plt.link_measures_to(plt0)
                plt.link_profiles_to(plt0)

        plt0.sig_poi_changed.connect(self.update_maps_wgt_info)
        plt0.sig_poi_changed.connect(self.update_poi_graph)
        plt0.sig_roi_changed.connect(self.update_maps_wgt_info)
        plt0.sig_roi_changed.connect(self.update_roi_graph)
        plt0.sig_roi_qs_changed.connect(self.update_roi_qs_spec)
        plt0.sig_measure_changed.connect(self.update_maps_wgt_info)
        plt0.sig_profile_changed.connect(self.update_maps_wgt_info)
        plt0.sig_profile_changed.connect(self.update_map_profile_curve)

    def init_spec_plots_widget(self):
        """Initialize spectrum plots widget and its control elements."""
        wgt = self.ui.wgtSpec
        wgt.setBackground('w')
        # wgt.setBackground((245,245,245))
        # wgt.ci.layout.setContentsMargins(0, 0, 0, 0)
        wgt.ci.layout.setContentsMargins(5, 10, 5, 5)
        wgt.ci.layout.setSpacing(0)
        # wgt.mouseReleaseEvent = self.on_specs_mouse_release
        # wgt.mousePressEvent = self.on_specs_mouse_press

        for i, plt in enumerate(self.pltSpecs):
            plt.set_x_label('Wavelength')
            plt.set_y_label(plt.par.name, plt.par.unit, plt.par.scale)
            # plt.lines_color = self.lines_color
            plt.sig_hover.connect(self.on_spec_plots_hover)
            plt.sig_cursor_pos_changed.connect(self.set_waveln_curr)

            cx_menu = plt.vb.menu
            cx_menu.addSeparator()
            cx_menu.addAction(self.ui.actSpecGoToCursor)
            cx_menu.addAction(self.ui.actSpecMoveCursorToView)
            cx_menu.addAction(self.ui.actSpecLinkViewToCursor)
            cx_menu.addSeparator()
            cx_menu.addAction(self.ui.actSpecAvg)
            cx_menu.addAction(self.ui.actSpecQS)
            cx_menu.addAction(self.ui.actSpecAtlas)
            cx_menu.addSeparator()
            cx_menu.addAction(self.ui.actSpecContFit)
            cx_menu.addSeparator()
            cx_menu.addAction(self.ui.actSpecRelWaveln)
            cx_menu.addSeparator()
            cx_menu.addAction(self.ui.actSpecQuickInfo)
            cx_menu.addAction(self.ui.actSpecMarkers)
            cx_menu.addAction(self.ui.actSpecGrids)
            cx_menu.addSeparator()
            cx_menu.addAction(self.ui.actSpecOptions)
            # cx_menu.addAction(self.ui.actSpecExportAsImage)

            self.ui.chkShowSpecCursor.stateChanged.connect(plt.show_cursor)
            self.ui.chkShowSpecHoverLines.stateChanged.connect(plt.show_hover_lines)
            if i > 0:
                plt.link_view_to(self.pltSpecs[0])
            # plt.zero_centered = PlotPars[plt.par].zero_centered

            if plt.par.name == ParIntensityNorm:
                plt.set_graph_style('FTS Atlas',
                                    color=(80, 80, 80),
                                    width=1.25,
                                    linestyle=Qt.PenStyle.DashLine)

        plt0 = self.pltSpecs[0]
        plt0.sig_contin_changed.connect(self.update_spec_cont_fit_intervals)
        plt0.sig_contin_changed.connect(self.update_spec_cont_fit_coeff)
        plt0.sig_contin_changed.connect(self.update_spec_cont_fit_fts_atlas)
        plt0.sig_contin_changed.connect(self.update_spec_pars_selected)
        plt0.sig_contin_changed.connect(self.update_map_pars_selected)

        num = len(self.pltSpecs)
        for i in range(num - 1):
            for j in range(i + 1, num):
                self.pltSpecs[i].link_cursor_to(self.pltSpecs[j])

    def load_session(self, file_name0=''):
        """Load saved session from a given file or using open dialog."""
        if not file_name0:
            if self.gris_obs:
                start_dir = self.gris_obs.dir_path
            else:
                start_dir = ''
            file_name0 = QFileDialog.getOpenFileName(self,
                                                     'Please choose session file',
                                                     start_dir,
                                                     'Session files (*.ses)')[0]
        if file_name0:
            self.session_config = toml.load(file_name0)
            # Check session for compatibility and integrity
            if 'Preferences' not in self.session_config:
                show_error("Could not open session")
                return

            conf = self.session_config['Preferences']
            # Check if observation exists on the disk
            obs_dir_obj = GrisObsDir(conf['ObsDir'])
            if not obs_dir_obj.has_obs(conf['ObsName']):
                show_error("Could not find observation.\n"
                           "Please check the path: " + obs_dir_obj.dir_path)
                return

            self.gris_obs_pre = GrisObs(conf['ObsDir'], conf['ObsName'], conf['Map1'], conf['Map2'])
            flag_load_obs = self.gris_obs is None or \
                            self.gris_obs_pre.name != self.gris_obs.name or \
                            self.gris_obs_pre.dir_path != self.gris_obs.dir_path
            if not flag_load_obs:
                self.gris_obs_pre = self.gris_obs
                self.restore_session()
            else:
                self.gris_obs_pre.sig_loaded.connect(self.restore_session)
                self.gris_obs_pre.read_maps()

    def restore_session(self):
        """Restore saved work session."""
        self.gris_obs = self.gris_obs_pre
        self.show_obs()
        conf = self.session_config['Preferences']
        self.ui.sliderWaveln.setValue(conf['Waveln'])

        self.restore_map_poi()
        self.restore_map_roi()
        self.restore_map_measures()
        self.restore_map_profiles()
        self.restore_session_panels()
        self.restore_spec_markers()
        self.restore_specs_cont_fit()
        self.restore_maps_view()
        self.restore_specs_view()

        show_info("Session has been successfully restored")

    def restore_session_panels(self):
        """Restore panels state from saved session."""
        conf = self.session_config['Maps']
        self.ui.actMapsQuickInfo.setChecked(conf['PanelQuickInfo'])
        self.ui.panelMapsQuickInfo.setVisible(conf['PanelQuickInfo'])
        self.ui.actMapsPOI.setChecked(conf['PanelPOI'])
        self.ui.panelMapsPOI.setVisible(conf['PanelPOI'])
        self.ui.actMapsMeasures.setChecked(conf['PanelMeasures'])
        self.ui.panelMapsMeasures.setVisible(conf['PanelMeasures'])
        self.ui.actMapsProfiles.setChecked(conf['PanelProfiles'])
        self.ui.panelMapsProfiles.setVisible(conf['PanelProfiles'])
        # self.ui.actMapsContours.setChecked(False)
        # self.ui.panelMapsContours.setVisible(False)
        self.ui.actMapsRoiQS.setChecked(False)
        self.ui.panelMapsRoiQS.setVisible(False)
        self.ui.actMapsOptions.setChecked(False)
        self.ui.panelMapsOptions.setVisible(False)

        conf = self.session_config['Specs']
        self.ui.actSpecQuickInfo.setChecked(conf['PanelQuickInfo'])
        self.ui.panelSpecQuickInfo.setVisible(conf['PanelQuickInfo'])
        self.ui.actSpecMarkers.setChecked(conf['PanelMarkers'])
        self.ui.panelSpecMarkers.setVisible(conf['PanelMarkers'])
        self.ui.actSpecContFit.setChecked(False)
        self.ui.panelSpecContFit.setVisible(False)
        self.ui.actSpecOptions.setChecked(False)
        self.ui.panelSpecOptions.setVisible(False)

    def restore_maps_view(self):
        """Restore maps look from saved session."""
        conf = self.session_config['Maps']

        for i, chk in enumerate(self.chkMapPars):
            par = conf['Plots']['par' + str(i + 1)]
            enabled = conf['Plots']['par' + str(i + 1) + '_enabled']
            self.cboMapPars[i].setCurrentText(par)
            chk.setChecked(enabled)

        self.ui.actMapsAxesLabels.setChecked(conf['AxesLabels'])
        self.toggle_maps_axes_labels(conf['AxesLabels'])
        self.ui.actMapsGrids.setChecked(conf['Grids'])
        self.toggle_maps_grids(conf['Grids'])

    def restore_map_poi(self):
        """Restore map POIs from saved session."""
        conf = self.session_config['Maps']
        plt0 = self.pltMaps[0]
        self.maps_opts['avg_box_size'] = conf['POIAvgBoxSize']
        txt = f"{conf['POIAvgBoxSize']}x{conf['POIAvgBoxSize']}"
        self.ui.comboPOIAvgBox.setCurrentText(txt)
        for p in plt0.poi:
            key = f'POI{p.idx + 1}'
            if key in conf:
                p.load(conf[key])

    def restore_map_roi(self):
        """Restore map ROIs from saved session."""
        conf = self.session_config['Maps']
        plt0 = self.pltMaps[0]
        for r in plt0.roi:
            key = f'ROI{r.idx + 1}'
            if key in conf:
                r.load(conf[key])
        plt0.blockSignals(True)
        idx = 1
        while True:
            key = f'ROI_QS{idx}'
            if key in conf:
                self.add_maps_roi_qs()
                plt0.roi_qs[-1].load(conf[key])
                plt0.roi_qs[-1].hide()
                idx += 1
            else:
                break
        plt0.blockSignals(False)
        if plt0.roi_qs:
            plt0.sig_roi_qs_changed.emit(plt0.roi_qs[0])

    def restore_map_measures(self):
        """Restore map measures from saved session."""
        conf = self.session_config['Maps']
        plt0 = self.pltMaps[0]
        for i in range(6):
            btn = getattr(self.ui, f'btnMeasuresUnits{i + 1}')
            if btn.text() == conf['MeasuresUnit']:
                btn.click()
        for m in plt0.measures:
            key = f'Measure{m.idx + 1}'
            if key in conf:
                m.load(conf[key])

    def restore_map_profiles(self):
        """Restore map profiles from saved session."""
        conf = self.session_config['Maps']
        plt0 = self.pltMaps[0]
        for i in range(3):
            sect = f'Profile{i + 1}'
            plt0.profiles[i].load(conf[sect])
            if 'size' in conf[sect]:
                self.ui.comboProfileAvgWidth.setCurrentText(
                    f"{conf[sect]['size'][1]:.0f}")

    def restore_specs_view(self):
        """Restore spectrum widget look from saved session."""
        conf = self.session_config['Specs']

        for i, chk in enumerate(self.chkSpecPars):
            par = conf['Plots']['par' + str(i + 1)]
            enabled = conf['Plots']['par' + str(i + 1) + '_enabled']
            self.cboSpecPars[i].setCurrentText(par)
            chk.setChecked(enabled)

        self.ui.chkShowSpecLines.setChecked(conf['Lines'])
        self.ui.chkShowSpecMarkers.setChecked(conf['Markers'])
        self.ui.dsbSpecWavelnOffset.setValue(conf['WavelnOffset'])
        self.set_spec_wl_offset(conf['WavelnOffset'])
        self.ui.actSpecGrids.setChecked(conf['Grids'])
        self.toggle_spec_grids(conf['Grids'])

        self.ui.actSpecAvg.setChecked(conf['GraphAvg'])
        self.ui.actSpecQS.setChecked(conf['GraphQS'])
        self.ui.actSpecAtlas.setChecked(conf['GraphFTS'])

    def restore_spec_markers(self):
        """Restore spectral wavelength markers from saved session."""
        conf = self.session_config['Specs']
        idx = 1
        while True:
            key = f'Marker{idx}'
            if key in conf:
                for plt in self.pltSpecs:
                    plt.add_wl_marker(conf[key]['waveln'])
                self.ui.listSpecsMarkers.addItem('')
                self.relist_spec_markers()
                idx += 1
            else:
                break

    def restore_specs_cont_fit(self):
        """Restore spectral continuum fit from saved session."""
        conf = self.session_config['Specs']
        plt0 = self.pltSpecs[0]
        plt0.contin_regs = []
        idx = 1
        while True:
            key = f'ContFitReg{idx}'
            if key in conf:
                plt0.add_cont_reg()
                plt0.contin_regs[-1].setRegion(conf[key]['waveln'])
                idx += 1
            else:
                break
        self.ui.sboContFitDegree.setValue(conf['ContFitDegree'])
        self.ui.dsbContFitMult.setValue(conf['ContFitMult'])
        plt0.contin_fit_pars['degree'] = conf['ContFitDegree']
        plt0.contin_fit_pars['mult'] = conf['ContFitMult']
        plt0.show_cont_fit_regs(False)
        plt0.fit_continuum(degree=plt0.contin_fit_pars['degree'],
                           mult=plt0.contin_fit_pars['mult'])
        for plt in self.pltSpecs[1:]:
            plt.contin_data = plt0.contin_data
        self.update_plot_specs_widget()

    def save_session(self):
        """Save current session configuration to file."""
        file_name0 = QFileDialog.getSaveFileName(self,
                                                 'Please choose session file',
                                                 self.gris_obs.dir_path,
                                                 'Session files (*.ses)')[0]
        if file_name0:
            if not QFileInfo(file_name0).suffix():
                file_name0 += '.ses'
            plt0 = self.pltMaps[0]
            conf = {}

            # save current wavelength
            conf['Preferences'] = dict(
                ObsDir=self.gris_obs.dir_path,
                ObsName=self.gris_obs.name,
                Map1=self.gris_obs.map_start,
                Map2=self.gris_obs.map_end,
                Waveln=self.ui.sliderWaveln.value())

            conf['Maps'] = dict(
                PanelQuickInfo=self.maps_opts['panel_quickinfo_visible'],
                PanelPOI=self.maps_opts['panel_poi_visible'],
                PanelMeasures=self.maps_opts['panel_measures_visible'],
                PanelProfiles=self.maps_opts['panel_profiles_visible'],
                AxesLabels=self.maps_opts['axes_visible'],
                Grids=self.maps_opts['grids_visible'],
                POIAvgBoxSize=self.maps_opts['avg_box_size'],
                MeasuresUnit=self.maps_opts['unit_measures']
            )

            conf['Maps']['Plots'] = self.save_maps_layout()

            for p in plt0.poi:
                k = f'POI{p.idx + 1}'
                conf['Maps'][k] = p.dump()

            for r in plt0.roi:
                k = f'ROI{r.idx + 1}'
                conf['Maps'][k] = r.dump()

            for idx, r in enumerate(plt0.roi_qs):
                k = f'ROI_QS{idx + 1}'
                conf['Maps'][k] = r.dump()

            for m in plt0.measures:
                k = f'Measure{m.idx + 1}'
                conf['Maps'][k] = m.dump()

            conf['Maps'].update(self.save_session_profiles())

            conf['Specs'] = dict(
                PanelQuickInfo=self.spec_opts['panel_quickinfo_visible'],
                PanelMarkers=self.spec_opts['panel_markers_visible'],
                Lines=self.spec_opts['lines_visible'],
                Markers=self.spec_opts['markers_visible'],
                Grids=self.spec_opts['grids_visible'],
                WavelnOffset=self.spec_opts['waveln_offset'],
                GraphAvg=self.ui.actSpecAvg.isChecked(),
                GraphQS=self.ui.actSpecQS.isChecked(),
                GraphFTS=self.ui.actSpecAtlas.isChecked(),
            )
            conf['Specs']['Plots'] = self.save_specs_layout()
            conf['Specs'].update(self.save_spec_markers())
            conf['Specs'].update(self.save_spec_cont_fit())

            with open(file_name0, 'w') as f:
                toml.dump(conf, f, encoder=toml.TomlNumpyEncoder())

            show_info("Session has been successfully saved")

    def save_maps_layout(self):
        """Get maps layout config for saving in session file."""
        conf = {}
        for i, chk in enumerate(self.chkMapPars):
            conf[f'par{i + 1}_enabled'] = chk.isChecked()
            conf[f'par{i + 1}'] = self.cboMapPars[i].currentText()
        return conf

    def save_session_profiles(self):
        """Get maps profiles config for saving in a session file."""
        plt0 = self.pltMaps[0]
        conf = {}
        for i in range(3):
            conf[f'Profile{i + 1}'] = plt0.profiles[i].dump()
            chk = getattr(self.ui, f'chkProfile{i + 1}')
            conf[f'Profile{i + 1}']['visible'] = chk.isChecked()
        return conf

    def save_specs_layout(self):
        """Get spectra layout config for saving in session file."""
        conf = {}
        for i, chk in enumerate(self.chkSpecPars):
            conf[f'par{i + 1}_enabled'] = chk.isChecked()
            conf[f'par{i + 1}'] = self.cboSpecPars[i].currentText()
        return conf

    def save_spec_markers(self):
        """Get spectral wavelength markers config for saving in session file."""
        conf = {}
        plt0 = self.pltSpecs[0]
        for i, mrk in enumerate(plt0.wl_markers):
            conf[f'Marker{i + 1}'] = dict(waveln=mrk.value())
        return conf

    def save_spec_cont_fit(self):
        """Get spectral continuum fit parameters for saving in a session file."""
        conf = {}
        plt0 = self.pltSpecs[0]
        conf['ContFitDegree'] = plt0.contin_fit_pars['degree']
        conf['ContFitMult'] = plt0.contin_fit_pars['mult']
        for i, reg in enumerate(plt0.contin_regs):
            conf[f'ContFitReg{i + 1}'] = dict(waveln=reg.getRegion())
        return conf

    def open_obs(self, obs_dir='', obs_name='', obs_map1=-1, obs_map2=-1):
        """
        Open new observation interactively with the dialog window or using
        command-line interface provided arguments.

        Args:
            obs_dir (str, optional): folder containing dataset
            obs_name (str, optional): specific observation name to open
            obs_map1 (int, optional): start map index (for time series only)
            obs_map2 (int, optional): end map index (for time series only)
        """
        if not obs_name:
            if obs_dir:
                start_dir = obs_dir
            else:
                start_dir = '/home/yakob/projects/sdc/gris.examp/'
            if self.dlgOpenObs is None:
                self.dlgOpenObs = GrisObsOpenDialog(start_dir)
            dlg = self.dlgOpenObs
            dlg.show()
            obs_dir = dlg.obs_dir
            obs_name = dlg.obs_name
            obs_map1 = dlg.obs_map1
            obs_map2 = dlg.obs_map2
        if obs_dir:
            self.obs_dir = obs_dir
            self.gris_obs = GrisObs(obs_dir, obs_name, obs_map1, obs_map2)
            self.gris_obs.sig_loaded.connect(self.show_obs)
            # self.prepare_load_obs()
            self.gris_obs.read_maps()

    def show_obs(self):
        """Show loaded observation."""
        self.is_obs_open = True
        self.reset_headers_form()
        self.reset_panels()
        self.reset_plots()

        self.init_obs_view()
        self.reset_plot_pars_selected()
        self.fit_maps_to_view()

    def reset_plots(self):
        """Reset all plots and graphical elements."""
        self.reset_all_maps_poi()
        self.reset_all_maps_roi()
        self.reset_all_maps_roi_qs()
        self.reset_all_maps_measures()
        self.reset_map_profiles()
        self.reset_map_contours()
        for plt in self.pltSpecs:
            plt.reset()
        if self.gris_inv_maps:
            num_def = len(PlotPars)
            num = len(self.pltMaps)
            for pid in range(num, num_def, -1):
                self.pltMaps.pop(-1)
        self.gris_inv_maps = []
        self.gris_inv_profiles = []
        self.clear_spec_markers()

    def init_obs_view(self):
        """Initialize UI for a newly loaded observation."""
        obs = self.gris_obs

        self.setWindowTitle('GRISView - ' + obs.name)

        self.ui.panelTimeSeries.setVisible(obs.is_time_series)
        if obs.is_time_series:
            self.blockSignals(True)
            self.ui.comboMapID.clear()
            self.ui.comboMapID.addItems([f'{i:02d}' for i in obs.orig_map_ids])
            self.ui.sliderMapID.setMaximum(obs.num_maps - 1)
            self.ui.sliderMapID.setValue(0)
            self.blockSignals(False)

        self.map_id_curr = 0
        self.set_map_compass_orient()
        self.set_specs_wl_scale()
        self.update_graph_average()
        self.update_graph_fts_atlas()
        self.ui.chkShowSpecLines.setChecked(False)
        for plt in self.pltSpecs:
            plt.init_lines()
        self.list_spec_lines()

        self.ui.actSpecAvg.setChecked(True)
        self.ui.actSpecQS.setChecked(False)
        self.ui.actSpecQS.setEnabled(False)
        self.ui.actSpecAtlas.setChecked(False)

        self.ui.btnViewMapUnits2.click()
        self.ui.btnMeasuresUnits2.click()
        self.ui.btnMeasuresUnits5.setDisabled(obs.is_ifu_mode)
        self.ui.btnMeasuresUnits6.setDisabled(obs.is_ifu_mode)

        self.ui.btnViewSpecUnits3.click()

        self.ui.sliderWaveln.setMaximum(obs.num_waveln - 1)
        self.ui.sliderWaveln.setValue(0)

        self.ui.dsbContFitMult.setValue(1.0)        
        self.ui.dsbSpecWavelnOffset.setValue(0.0)

    def reset_plot_pars_selected(self):
        """Reset selected parameters list for maps and spectra."""
        is_full_stokes = self.gris_obs.num_stokes > 1

        for i, chk in enumerate(self.chkMapPars):
            cbo = self.cboMapPars[i]
            cbo.blockSignals(True)
            cbo.clear()
            cbo.addItems([p.name for p in PlotPars])            
            if not is_full_stokes:
                for idx, par in enumerate(PlotPars):
                    cbo.model().item(idx).setEnabled(
                        par.name in [ParIntensity, ParIntensityNorm])
            cbo.setCurrentIndex(-1)
            cbo.blockSignals(False)

            chk.blockSignals(True)
            chk.setChecked(False)
            chk.blockSignals(False)

        for i, chk in enumerate(self.chkSpecPars):
            cbo = self.cboSpecPars[i]
            cbo.blockSignals(True)
            cbo.clear()
            cbo.addItems([p.name for p in PlotPars])
            if not is_full_stokes:
                for idx, par in enumerate(PlotPars):
                    cbo.model().item(idx).setEnabled(
                        par.name in [ParIntensity, ParIntensityNorm])
            cbo.setCurrentIndex(-1)
            cbo.blockSignals(False)

            chk.blockSignals(True)
            chk.setChecked(False)
            chk.blockSignals(False)

        self.map_pars_prev = []
        self.spec_pars_prev = []
        self.cboMapPars[0].setCurrentIndex(0)
        self.cboSpecPars[0].setCurrentIndex(0)
        self.chkMapPars[0].setChecked(True)
        self.chkSpecPars[0].setChecked(True)

    def set_maps_view_mode_single(self, selected):
        """Switch one-by-one view mode for maps if selected."""
        if selected:
            self.maps_opts['view_mode'] = 'single'
            if self.map_pars_selected:
                self.map_par_curr = 0
                i = self.map_pars_selected[0]
                self.pltMapCurr = self.pltMaps[i]
                self.set_maps_view_mode()                

    def set_maps_view_mode_multi(self, selected):
        """Switch side-by-side view mode for maps if selected."""
        if selected:
            self.maps_opts['view_mode'] = 'multi'
            self.pltMapCurr = None
            self.set_maps_view_mode()

    def set_maps_view_mode(self):
        """Set spectral widgets view to one-by-one or side-by-side mode."""
        flag = self.maps_opts['view_mode'] == 'single'
        self.ui.wgtMapParCurr.setVisible(flag)
        self.set_maps_view()
        if not flag:
            for plt in self.pltMaps:
                plt.plot.fit_image_to_view()

    def set_maps_view(self):
        """Set maps widget view."""
        wgt = self.ui.wgtMaps
        wgt.clear()

        # show/hide axes
        flag = self.maps_opts['axes_visible']
        for plt in self.pltMaps:
            plt.plot.hide_axes(not flag)
            plt.plot.hide_axes_labels(True)
            plt.show_colorbar(flag)

        # show/hide compass
        for plt in self.pltMaps:
            plt.show_compass(self.maps_opts['compass_visible'])
        if self.maps_opts['view_mode'] != 'single' and \
           self.num_map_pars_selected > 1:
            for pid in self.map_pars_selected[1:]:
                self.pltMaps[pid].show_compass(False)

        # set parent widget margins
        if flag:
            wgt.ci.layout.setContentsMargins(10, 10, 10, 5)
        else:
            wgt.ci.layout.setContentsMargins(0, 0, 0, 0)

        # show axes labels
        if flag:
            p = self.pltMaps[0].plot
            if self.maps_opts['view_mode'] == 'single':
                wgt.addLabel(p.label_x, **p.label_style, row=1, col=1)
                wgt.addLabel(p.label_y, **p.label_style, angle=-90, row=0, col=0)
            else:
                if self.num_map_pars_selected < 4:
                    wgt.addLabel(p.label_y, **p.label_style, angle=-90, row=0, col=0)
                    for i in range(self.num_map_pars_selected):
                        wgt.addLabel(p.label_x, **p.label_style, row=1, col=i + 1)
                else:
                    wgt.addLabel(p.label_y, **p.label_style, angle=-90, row=0, col=0)
                    wgt.addLabel(p.label_y, **p.label_style, angle=-90, row=1, col=0)
                    wgt.addLabel(p.label_x, **p.label_style, row=2, col=1)
                    wgt.addLabel(p.label_x, **p.label_style, row=2, col=2)

        # add and arrange map plots
        if self.maps_opts['view_mode'] == 'single':
            pid = self.map_pars_selected[self.map_par_curr]
            wgt.addItem(self.pltMaps[pid], row=0, col=1)
        else:
            if self.num_map_pars_selected < 4:
                for i, pid in enumerate(self.map_pars_selected):
                    wgt.addItem(self.pltMaps[pid], row=0, col=i + 1)
            else:
                p = self.map_pars_selected
                wgt.addItem(self.pltMaps[p[0]], row=0, col=1)
                wgt.addItem(self.pltMaps[p[1]], row=0, col=2)
                wgt.addItem(self.pltMaps[p[2]], row=1, col=1)
                wgt.addItem(self.pltMaps[p[3]], row=1, col=2)

        self.highlight_map_par_curr()
        self.correct_map_wgt_col_width()
        self.toggle_map_contours_panel()
        self.toggle_map_profiles_panel()

    def set_maps_image_data(self):
        """Set maps image data for current wavelength and time point (for time
        series)."""
        map_curr = self.gris_obs.maps[self.map_id_curr]
        for pid in self.map_pars_selected:
            plt = self.pltMaps[pid]
            if plt.par.func is not None:
                dat_sel = map_curr.data[:, self.waveln_curr]
                if plt.par.normalized:
                    cont = self.pltSpecs[0].get_continuum_at(self.waveln_curr, wl_unit=u.pix)
                    img_data = plt.par.func(dat_sel, cont)
                else:
                    img_data = plt.par.func(dat_sel)
                plt.set_image_data(img_data)

    def set_specs_view_mode_single(self, selected):
        """Switch one-by-one view mode for spectrum plots if selected."""
        if selected:
            self.spec_opts['view_mode'] = 'single'
            self.spec_par_curr = 0
            i = self.spec_pars_selected[0]
            self.pltSpecCurr = self.pltSpecs[i]
            self.set_specs_view_mode()

    def set_specs_view_mode_multi(self, selected):
        """Switch side-by-side view mode for spectrum plots if selected."""
        if selected:
            self.spec_opts['view_mode'] = 'multi'
            self.pltSpecCurr = None
            self.set_specs_view_mode()

    def set_specs_view_mode(self):
        """Set view mode for spectrum plots."""
        flag = self.spec_opts['view_mode'] == 'single'
        self.ui.wgtSpecParCurr.setVisible(flag)
        self.set_specs_view()

    def show_headers_dialog(self):
        """Show file headers dialog window."""
        if self.dlgHeaders is None and \
                self.gris_obs is not None:
            self.dlgHeaders = GrisViewHeaders(self.gris_obs)
        self.dlgHeaders.show()

    def reset_headers_form(self):
        """Reset file headers dialog window."""
        self.dlgHeaders = None

    def set_map_compass_orient(self):
        """Add compass to map plots."""        
        map_curr = self.gris_obs.maps[self.map_id_curr]
        is_visible = map_curr.wcs_coord is not None
        if is_visible:
            for plt in self.pltMaps:
                plt.set_header(map_curr.headers[0])            
                plt.set_compass(map_curr.wcs_coord[0],
                                map_curr.num_x * 0.5,
                                map_curr.num_y * 0.5)        
        self.ui.actMapsCompass.setChecked(is_visible)
        self.ui.actMapsCompass.setEnabled(is_visible)

    def set_map_id_curr(self, value):
        """Load selected time point using its index value (for time series observations only)."""
        self.map_id_curr = value
        if isinstance(self.sender(), QSlider):
            self.ui.comboMapID.blockSignals(True)
            self.ui.comboMapID.setCurrentIndex(self.map_id_curr)
            self.ui.comboMapID.blockSignals(False)
        else:
            self.ui.sliderMapID.blockSignals(True)
            self.ui.sliderMapID.setValue(self.map_id_curr)
            self.ui.sliderMapID.blockSignals(False)
        self.ui.lblMapTimeBeg.setText(
            self.gris_obs.get_timestamp(self.map_id_curr, 0, 0))

        self.set_maps_image_data()
        self.set_map_compass_orient()
        self.update_map_contours_image()
        self.update_map_profiles_plot()
        self.update_graph_average()
        self.update_graph_qs()
        self.update_poi_roi_graphs()
        self.update_inversion_maps()

    def set_map_id_prev(self):
        """Go to previous time point (for time series observations only)."""
        value = self.map_id_curr - 1
        if value < 0:
            if self.ui.chkMapLoop.isChecked():
                value = self.gris_obs.num_maps - 1
            else:
                value = 0
        if value != self.map_id_curr:
            self.ui.sliderMapID.setValue(value)

    def set_map_id_next(self):
        """Go to next time point (for time series observations only)."""
        # value = min(self.gris_obs.num_maps - 1, self.map_id_curr + 1)
        value = self.map_id_curr + 1
        if value > self.gris_obs.num_maps - 1:
            if self.ui.chkMapLoop.isChecked():
                value = 0
            else:
                value = self.gris_obs.num_maps - 1
        if value != self.map_id_curr:
            self.ui.sliderMapID.setValue(value)

    def remove_map(self):
        """Remove current time point data from consideration and go to the next
        one (for time series observations only)."""
        if show_question_yes(
                'Are you sure you want to remove current map from analysis?'):
            if self.gris_obs.delete_map(self.map_id_curr):
                if self.gris_inv_profiles:
                    self.gris_inv_profiles.pop(self.map_id_curr)
                self.ui.comboMapID.removeItem(self.map_id_curr)
                self.ui.sliderMapID.setMaximum(self.gris_obs.num_maps - 1)
                self.map_id_curr = min(self.map_id_curr, self.gris_obs.num_maps - 1)
                self.ui.sliderMapID.setValue(self.map_id_curr)
                self.set_map_id_curr(self.map_id_curr)
            else:
                show_error('Could not delete selected map')

    def set_waveln_curr(self, wl_idx):
        """Update current wavelength cursor position."""
        plt = self.pltSpecs[0]
        if isinstance(self.sender(), QSlider):
            plt.set_cursor_pos(wl_idx)
        else:
            self.ui.sliderWaveln.blockSignals(True)
            self.ui.sliderWaveln.setValue(wl_idx)
            self.ui.sliderWaveln.blockSignals(False)

        self.ui.lblWavelnCurr.setText(plt.get_cursor_pos_str())

        if self.waveln_curr != wl_idx:
            self.waveln_curr = wl_idx
            self.set_maps_image_data()
            self.update_map_contours_image()
            self.update_map_profiles_plot()

    def link_maps_views(self, flag=True):
        """Link/unlink all map plot views (sync range/pan/zoom)."""
        # make it so that view ranges are in sync with first displayed plot
        if flag:
            pid0 = self.map_pars_prev[0]
            plt0 = self.pltMaps[pid0]
        else:
            pid0 = -1
            plt0 = None
        for i, plt in enumerate(self.pltMaps):
            if i != pid0:
                plt.link_view_to(plt0)

    def fit_maps_to_view(self):
        """Zoom maps to fit plot view boxes."""
        if self.is_obs_open:
            self.link_maps_views(False)
            for plt in self.pltMaps:
                plt.plot.fit_image_to_view()
            # for pid in self.map_pars_selected:
            #     self.pltMaps[pid].plot.fit_image_to_view()
            self.link_maps_views(True)

    def toggle_spec_plot(self, flag):
        """Enable/disable parameter and update spectrum plots."""
        pid = self.sender().property('Index')
        if self.num_spec_pars_selected == 1 and \
           self.cboSpecPars[pid].currentText() and \
           not self.chkSpecPars[pid].isChecked():
            self.chkSpecPars[pid].setChecked(True)            
        else:
            self.cboSpecPars[pid].setEnabled(flag)
            self.update_plot_specs_widget()

    def toggle_map_plot(self, flag):
        """Enabled/disable parameter and update map plots."""
        pid = self.sender().property('Index')
        if self.num_map_pars_selected == 1 and \
           self.cboMapPars[pid].currentText() and \
           not self.chkMapPars[pid].isChecked():
            self.chkMapPars[pid].setChecked(True)
        else:
            self.cboMapPars[pid].setEnabled(flag)
            self.update_plot_maps_widget()

    def update_plot_specs_widget(self):
        """Update spectrum plots widget according to enabled parameters."""
        self.update_spec_pars_selected()
        self.set_specs_view()
        self.update_poi_roi_graphs()
        for pid in self.spec_pars_selected:
            self.pltSpecs[pid].update_graph('Quiet Sun')
            self.pltSpecs[pid].update_graph('Average')

    def set_specs_view(self):
        """Set spectral widget plots layout according to selected view mode."""
        wgt = self.ui.wgtSpec
        wgt.clear()
        if self.spec_opts['view_mode'] == 'single':
            pid = self.spec_pars_selected[self.spec_par_curr]
            wgt.addItem(self.pltSpecs[pid])
        else:
            for i, pid in enumerate(self.spec_pars_selected):
                wgt.addItem(self.pltSpecs[pid], row=i, col=0)
        self.highlight_spec_par_curr()

    def goto_map_par_next(self):
        """Go to next selected parameter in one-by-one view mode for maps."""
        if self.maps_opts['view_mode'] == 'single':
            if self.map_par_curr < self.num_map_pars_selected - 1:
                self.map_par_curr += 1
            else:
                self.map_par_curr = 0
            i = self.map_pars_selected[self.map_par_curr]
            self.pltMapCurr = self.pltMaps[i]
            self.set_maps_view()

    def goto_map_par_prev(self):
        """Go to previous selected parameter in one-by-one view mode for maps."""
        if self.maps_opts['view_mode'] == 'single':
            if self.map_par_curr > 0:
                self.map_par_curr -= 1
            else:
                self.map_par_curr = self.num_map_pars_selected - 1
            i = self.map_pars_selected[self.map_par_curr]
            self.pltMapCurr = self.pltMaps[i]
            self.set_maps_view()

    def goto_spec_par_next(self):
        """Go to next selected parameter in one-by-one view mode for spectra."""
        if self.spec_opts['view_mode'] == 'single':
            if self.spec_par_curr < self.num_spec_pars_selected - 1:
                self.spec_par_curr += 1
            else:
                self.spec_par_curr = 0
            i = self.spec_pars_selected[self.spec_par_curr]
            self.pltSpecCurr = self.pltSpecs[i]
            self.set_specs_view()

    def goto_spec_par_prev(self):
        """Go to previous selected parameter in one-by-one view mode for ."""
        if self.spec_opts['view_mode'] == 'single':
            if self.spec_par_curr > 0:
                self.spec_par_curr -= 1
            else:
                self.spec_par_curr = self.num_spec_pars_selected - 1
            i = self.spec_pars_selected[self.spec_par_curr]
            self.pltSpecCurr = self.pltSpecs[i]
            self.set_specs_view()

    def highlight_map_par_curr(self):
        """Highlight check buttons for active map parameters."""
        pid = -1
        flag_single = self.maps_opts['view_mode'] == 'single'
        if flag_single:
            for i, cbo in enumerate(self.cboMapPars):
                if self.pltMapCurr.par.name == cbo.currentText():
                    pid = i
        for i, chk in enumerate(self.chkMapPars):
            fnt = chk.font()
            fnt.setBold(i == pid)
            fnt.setUnderline(i == pid)
            chk.setFont(fnt)
            if i == pid or not flag_single:
                chk.setStyleSheet("QPushButton:checked { "
                                 "background-color: rgb(62, 68, 81); "
                                  "color: rgb(247, 247, 247) }")
            else:
                chk.setStyleSheet("QPushButton:checked { "
                                  "background-color: rgb(124, 136, 162); "
                                  "color: rgb(247, 247, 247) }")

    def highlight_spec_par_curr(self):
        """Highlight check buttons for active spectral parameters."""
        pid = -1
        flag_single = self.spec_opts['view_mode'] == 'single'
        if flag_single:
            for i, cbo in enumerate(self.cboSpecPars):
                if self.pltSpecCurr.par.name == cbo.currentText():
                    pid = i
        for i, chk in enumerate(self.chkSpecPars):
            fnt = chk.font()
            fnt.setBold(i == pid)
            fnt.setUnderline(i == pid)
            chk.setFont(fnt)
            if i == pid or not flag_single:
                chk.setStyleSheet("QPushButton:checked { "
                                  "background-color: rgb(62, 68, 81); "                                  
                                  "color: rgb(247, 247, 247) }")
            else:
                chk.setStyleSheet("QPushButton:checked { "                                  
                                  "background-color: rgb(124, 136, 162); "
                                  "color: rgb(247, 247, 247) }")

    def update_plot_maps_widget(self):
        """Update map plots widget according to selected/deselected parameters"""
        self.update_map_pars_selected()
        self.link_maps_views(False)
        self.set_maps_view()
        self.set_maps_image_data()
        self.link_maps_views(True)

    def update_spec_pars_selected(self):
        """Update spectral parameters combos and active parameters list."""
        flag = self.pltSpecs[0].has_continuum()
        for i, cbo in enumerate(self.cboSpecPars):
            for pid, plt in enumerate(self.pltSpecs):
                if plt.par.normalized:                    
                    cbo.model().item(pid).setEnabled(flag)
                    if not flag and plt.par.name == cbo.currentText():
                        if i == 0:
                            cbo.setCurrentIndex(0)
                        else:
                            self.chkSpecPars[i].setChecked(False)
                            cbo.setCurrentIndex(-1)

        self.spec_pars_prev = self.spec_pars_selected
        self.spec_pars_selected = []

        cbo_pars = [cbo.currentText() for cbo in self.cboSpecPars if cbo.isEnabled()]
        plt_pars = [p.par.name for p in self.pltSpecs]

        for p in cbo_pars:
            if p in plt_pars:
                i = plt_pars.index(p)
                self.spec_pars_selected.append(i)

        self.num_spec_pars_selected = len(self.spec_pars_selected)
        if self.spec_opts['view_mode'] == 'single':
            if self.spec_par_curr > self.num_spec_pars_selected - 1:
                self.spec_par_curr = 0
            i = self.spec_pars_selected[self.spec_par_curr]
            self.pltSpecCurr = self.pltSpecs[i]

        for cbo in self.cboSpecPars:
            if not cbo.isEnabled() and cbo.currentText() in cbo_pars:
                cbo.setCurrentIndex(-1)
            for p in plt_pars:
                if p != cbo.currentText():
                    cbo.view().setRowHidden(cbo.findText(p), p in cbo_pars)

        if not self.spec_pars_prev:
            self.spec_pars_prev = self.spec_pars_selected

        act = self.ui.actSpecContFit
        has_qs = self.pltSpecs[0].has_data_cube('Quiet Sun')
        if not has_qs and act.isChecked():
            act.trigger()
        act.setEnabled(has_qs)
        self.ui.actSpecAtlas.setEnabled(flag)

    def update_map_pars_selected(self):
        """Update map parameters combos and active parameters list."""
        flag = self.pltSpecs[0].has_continuum()
        for i, cbo in enumerate(self.cboMapPars):
            for pid, plt in enumerate(self.pltMaps):
                if plt.par.normalized:
                    cbo.model().item(pid).setEnabled(flag)
                    if not flag and plt.par.name == cbo.currentText():
                        if i == 0:
                            cbo.setCurrentIndex(0)
                        else:
                            self.chkMapPars[i].setChecked(False)
                            cbo.setCurrentIndex(-1)

        self.map_pars_prev = self.map_pars_selected
        self.map_pars_selected = []

        cbo_pars = [cbo.currentText() for cbo in self.cboMapPars if cbo.isEnabled()]
        plt_pars = [p.par.name for p in self.pltMaps]

        for p in cbo_pars:
            if p in plt_pars:
                i = plt_pars.index(p)
                self.map_pars_selected.append(i)

        self.num_map_pars_selected = len(self.map_pars_selected)
        if self.maps_opts['view_mode'] == 'single':
            if self.map_par_curr > self.num_map_pars_selected - 1:
                self.map_par_curr = 0
            i = self.map_pars_selected[self.map_par_curr]
            self.pltMapCurr = self.pltMaps[i]

        for cbo in self.cboMapPars:
            if not cbo.isEnabled() and cbo.currentText() in cbo_pars:
                cbo.setCurrentIndex(-1)
            for p in plt_pars:
                if p != cbo.currentText():
                    cbo.view().setRowHidden(cbo.findText(p), p in cbo_pars)

        if not self.map_pars_prev:
            self.map_pars_prev = self.map_pars_selected

    def toggle_maps_axes_labels(self, flag):
        """Show/hide axes and labels on map plots."""
        self.maps_opts['axes_visible'] = flag
        self.ui.actMapsGrids.setEnabled(flag)
        self.set_maps_view()

    def toggle_maps_compass(self, flag):
        """Show/hide compass on map plots."""
        self.maps_opts['compass_visible'] = flag
        self.set_maps_view()

    def toggle_maps_grids(self, flag):
        """Show/hide grids on map plots."""
        self.maps_opts['grids_visible'] = flag
        for plt in self.pltMaps:
            plt.show_grid(flag)

    def update_map_unit_xy(self, plt):
        """Update unit of map plots."""
        unit = self.maps_opts['unit_xy']
        if unit == 'px':
            plt.set_pixel_scales(1.0, 1.0)
            plt.set_axes_labels(unit=u.pix)
        elif unit == 'arcsec':
            plt.set_pixel_scales(self.gris_obs.scale_x,
                                 self.gris_obs.scale_y)
            plt.set_axes_labels(unit=u.arcsec)

    def change_maps_unit_xy(self):
        """Change unit for map plots and connected panels."""
        if self.is_obs_open:
            txt = self.sender().text()
            if txt == 'pixel':
                self.maps_opts['unit_xy'] = 'px'
            else:
                self.maps_opts['unit_xy'] = 'arcsec'

            for plt in self.pltMaps:
                self.update_map_unit_xy(plt)
            self.on_map_plots_hover(-1, -1, -1)

            plt0 = self.pltMaps[0]
            for i in range(self.num_maps_poi):
                lbl = getattr(self.ui, 'infoPOI' + str(i + 1))
                if lbl.text():
                    self.update_maps_wgt_info(plt0.poi[i])
            for i in range(self.num_maps_roi):
                lbl = getattr(self.ui, 'infoROI' + str(i + 1))
                if lbl.text():
                    self.update_maps_wgt_info(plt0.roi[i])
            for i in range(self.num_maps_profiles):
                lbl = getattr(self.ui, 'infoProfile' + str(i + 1))
                if lbl.text():
                    self.update_maps_wgt_info(plt0.profiles[i].obj)

            self.set_maps_view()

    def change_maps_plot_wgt_color(self):
        """Set color of selected map widget using color dialog
        after pressing one of the color buttons."""
        btn = self.sender()
        
        # Assuming that all color buttons have names "clr[type]X", where X = 1..9
        wgt_type = btn.objectName()[3:-1].lower()
        wgt_idx = int(btn.objectName()[-1]) - 1
        c = btn.palette().button().color()
        color = QColorDialog.getColor(c)

        plt0 = self.pltMaps[0]
        if color.isValid():
            btn.setStyleSheet('background-color: ' + color.name() + ';')
            if wgt_type == 'poi':
                plt0.set_poi_color(wgt_idx, color)
            if wgt_type == 'roi':
                plt0.set_roi_color(wgt_idx, color)
            if wgt_type == 'measure':
                plt0.set_measure_color(wgt_idx, color)
            if wgt_type == 'profile':
                plt0.set_profile_color(wgt_idx, color)

    def update_maps_wgt_info(self, wgt):
        """
        Update selected map widget visibility, color and info.
        Args:
            wgt: one of map widgets instances (poi/roi/measure/profile)
        """
        wgt_type = ''
        if isinstance(wgt, GrisMapPOI):
            wgt_type = 'POI'
        if isinstance(wgt, GrisMapROI):
            wgt_type = 'ROI'
        if isinstance(wgt, GrisMapMeasure):
            wgt_type = 'Measure'
        if isinstance(wgt, GrisMapProfile):
            wgt_type = 'Profile'
        if wgt_type:
            chk = getattr(self.ui, 'chk' + wgt_type + str(wgt.idx + 1))
            btn = getattr(self.ui, 'clr' + wgt_type + str(wgt.idx + 1))
            lbl = getattr(self.ui, 'info' + wgt_type + str(wgt.idx + 1))
            if chk.isChecked() != wgt.visible:
                chk.blockSignals(True)
                chk.setChecked(wgt.visible)
                chk.blockSignals(False)
            btn.setStyleSheet('background-color: ' + wgt.color.name() + ';')
            if wgt.pos is not None:
                if wgt_type != 'Measure':
                    if self.maps_opts['unit_xy'] == 'px':
                        info = f'x: {wgt.pos.x():.0f}  y: {wgt.pos.y():.0f}'
                    else:
                        info = f"x: {wgt.pos.x() * self.gris_obs.scale_x:.1f}  " \
                               f"y: {wgt.pos.y() * self.gris_obs.scale_y:.1f}"
                else:
                    info = self.get_maps_measure_result(wgt)
                lbl.setText(info)
            else:
                lbl.setText('')

    def hide_maps_panels(self, flag):
        """Used to disable/hide and restore map working panels."""
        if flag:
            self.maps_opts['poi_visible'] = self.ui.chkShowPOI.isChecked()
            self.maps_opts['roi_visible'] = self.ui.chkShowROI.isChecked()
            self.maps_opts['measures_visible'] = self.ui.chkShowMeasures.isChecked()
            self.ui.actMapsPOI.setEnabled(False)
            self.ui.actMapsMeasures.setEnabled(False)
            self.ui.actMapsContours.setChecked(False)
            self.ui.actMapsContours.setEnabled(False)
            self.ui.actMapsProfiles.setChecked(False)
            self.ui.actMapsProfiles.setEnabled(False)
            self.ui.panelMapsPOI.setVisible(False)
            self.ui.panelMapsMeasures.setVisible(False)
            self.ui.chkShowPOI.setChecked(False)
            self.ui.chkShowROI.setChecked(False)
            self.ui.chkShowMeasures.setChecked(False)
        else:
            self.ui.actMapsPOI.setEnabled(True)
            self.ui.actMapsMeasures.setEnabled(True)
            self.ui.actMapsContours.setEnabled(True)
            self.ui.actMapsProfiles.setEnabled(True)
            self.ui.panelMapsPOI.setVisible(self.maps_opts['panel_poi_visible'])
            self.ui.panelMapsMeasures.setVisible(self.maps_opts['panel_measures_visible'])
            self.ui.chkShowPOI.setChecked(self.maps_opts['poi_visible'])
            self.ui.chkShowROI.setChecked(self.maps_opts['roi_visible'])
            self.ui.chkShowMeasures.setChecked(self.maps_opts['measures_visible'])

    def show_maps_quickinfo_panel(self, flag=True):
        """Show/hide maps quick info panel."""
        self.maps_opts['panel_quickinfo_visible'] = flag
        self.ui.panelMapsQuickInfo.setVisible(flag)

    def show_maps_poi_panel(self, flag=True):
        """Show/hide maps POI/ROI panel."""
        self.maps_opts['panel_poi_visible'] = flag
        self.ui.panelMapsPOI.setVisible(flag)

    def toggle_all_maps_poi(self, flag):
        """Show/hide all POIs and corresponding spectra."""
        for i in range(self.num_maps_poi):
            chk = getattr(self.ui, 'chkPOI' + str(i + 1))
            lbl = getattr(self.ui, 'infoPOI' + str(i + 1))
            chk.setEnabled(flag)
            self.pltMaps[0].set_poi_visible(i, flag and lbl.text() != '')

    def toggle_maps_poi(self, flag):
        """Show/hide selected POI."""
        if self.gris_obs is not None:
            idx = self.sender().property('idx')
            self.pltMaps[0].set_poi_visible(idx, flag)

    def reset_all_maps_poi(self):
        """Reset all maps POIs state."""
        self.pltMaps[0].reset_poi()
        self.ui.chkShowPOI.setChecked(True)

    def change_maps_poi_avg_box(self):
        """Set POIs box size for averaging spectra."""
        val = int(self.sender().currentText().split('x')[0])
        self.maps_opts['avg_box_size'] = val
        plt0 = self.pltMaps[0]
        for p in plt0.poi:
            self.update_poi_graph(p)

    def update_poi_roi_graphs(self):
        """Update spectra for all map POI and ROI."""
        plt0 = self.pltMaps[0]
        for p in plt0.poi:
            self.update_poi_graph(p)
        for r in plt0.roi:
            self.update_roi_graph(r)

    def update_poi_graph(self, poi):
        """Update/get spectra for a given map POI."""
        for pid in self.spec_pars_selected:
            self.pltSpecs[pid].set_graph_visible(poi.name, poi.visible)
            self.pltSpecs[pid].set_graph_style(poi.name, color=poi.color)

        data_sel = None
        if poi.pos is not None:
            map_curr = self.gris_obs.maps[self.map_id_curr]
            x = int(poi.pos.x())
            y = int(poi.pos.y())
            if 0 <= x < map_curr.num_x and \
                    0 <= y < map_curr.num_y:
                if self.maps_opts['avg_box_size'] > 1:
                    s = (self.maps_opts['avg_box_size'] - 1) // 2
                    x1 = max(x - s, 0)
                    x2 = min(x + s, map_curr.num_x)
                    y1 = max(y - s, 0)
                    y2 = min(y + s, map_curr.num_y)
                    data_sel = np.average(map_curr.data[:, :, y1:y2, x1:x2], axis=(2, 3))
                else:
                    data_sel = map_curr.data[:, :, y, x]

        for plt in self.pltSpecs:
            plt.set_data_cube(poi.name, data_sel)
        for pid in self.spec_pars_selected:
            self.pltSpecs[pid].update_graph(poi.name)

        self.refresh_spec_graphs_list()

        self.update_poi_graph_inv_profiles(poi)

    def update_roi_graph(self, roi):
        """Update/get spectra for a given map ROI."""
        for pid in self.spec_pars_selected:
            self.pltSpecs[pid].set_graph_visible(roi.name, roi.visible)
            self.pltSpecs[pid].set_graph_style(roi.name, color=roi.color)

        if self.gris_obs is not None:
            data_sel = None
            if roi.pos is not None:
                map_curr = self.gris_obs.maps[self.map_id_curr]
                roi_rect = roi.get_bounds(width=map_curr.num_x,
                                          height=map_curr.num_y)
                if roi_rect is not None:
                    x1, x2 = roi_rect.left(), roi_rect.right()
                    y1, y2 = roi_rect.top(), roi_rect.bottom()
                    data_sel = np.average(map_curr.data[:, :, y1:y2 + 1, x1:x2 + 1], axis=(2, 3))
                # data_sel = roi.obj.getArrayRegion(map_curr.data,
                #                                   roi.obj.parent,
                #                                   axes=(3, 2))
            for plt in self.pltSpecs:
                plt.set_data_cube(roi.name, data_sel)
            for pid in self.spec_pars_selected:
                self.pltSpecs[pid].update_graph(roi.name)

            self.refresh_spec_graphs_list()

    def toggle_all_maps_roi(self, flag):
        """Show/hide all ROIs and corresponding spectra."""
        for i in range(self.num_maps_roi):
            chk = getattr(self.ui, 'chkROI' + str(i + 1))
            lbl = getattr(self.ui, 'infoROI' + str(i + 1))
            chk.setEnabled(flag)
            self.pltMaps[0].set_roi_visible(i, flag and lbl.text() != '')

    def toggle_maps_roi(self, flag):
        """Show/hide selected ROI."""
        idx = self.sender().property('idx')
        self.pltMaps[0].set_roi_visible(idx, flag)

    def hide_maps_roi_handles(self, flag):
        """Hide handles used to rotate/resize ROIs."""
        for plt in self.pltMaps:
            plt.hide_roi_handles(flag)

    def reset_all_maps_roi(self):
        """Reset all maps ROIs."""
        self.pltMaps[0].reset_roi()
        self.ui.chkShowROI.setChecked(True)
        self.ui.chkHideROIHandles.setChecked(False)

    def show_maps_measures_panel(self, flag):
        """Show/hide measures panel."""
        self.maps_opts['panel_measures_visible'] = flag
        self.ui.panelMapsMeasures.setVisible(flag)
        for k in range(self.num_maps_measures):
            chk = getattr(self.ui, 'chkMeasure' + str(k + 1))
            for plt in self.pltMaps:
                plt.measures[k].set_visible(chk.isChecked() and flag)

    def toggle_all_maps_measures(self, flag):
        """Show/hide all map measure widgets."""
        for i in range(self.num_maps_measures):
            chk = getattr(self.ui, 'chkMeasure' + str(i + 1))
            lbl = getattr(self.ui, 'infoMeasure' + str(i + 1))
            chk.setEnabled(flag)
            self.pltMaps[0].set_measure_visible(i, flag and lbl.text() != '')

    def toggle_maps_measure(self, flag):
        """Show/hide selected map measure widget."""
        idx = self.sender().property('idx')
        self.pltMaps[0].set_measure_visible(idx, flag)

    def reset_all_maps_measures(self):
        """Reset all map measure widgets."""
        plt0 = self.pltMaps[0]
        plt0.reset_measures()
        self.ui.chkShowMeasures.setChecked(True)

    def change_maps_measures_unit(self):
        """Change unit for map measures."""
        self.maps_opts['unit_measures'] = self.sender().text()
        for i, m in enumerate(self.pltMaps[0].measures):
            lbl = getattr(self.ui, 'infoMeasure' + str(i + 1))
            if lbl.text():
                self.update_maps_wgt_info(m)

    def get_maps_measure_result(self, measure):
        """Calculate length for a given measure in selected units."""
        if not self.gris_obs:
            return ""

        unit = self.maps_opts['unit_measures']
        id_map = self.map_id_curr
        map_curr = self.gris_obs.maps[id_map]
        p1, p2 = measure.get_pos()
        if 0 <= p1.x() < map_curr.num_x and \
                0 <= p1.y() < map_curr.num_y and \
                0 <= p2.x() < map_curr.num_x and \
                0 <= p2.y() < map_curr.num_y:

            if unit == 'px':
                return f'{QLineF(p1, p2).length():.1f} px'

            if unit == 'arcsec':
                pp1 = QPointF(*self.gris_obs.coord_px_to_wcs(id_map, p1.x(), p1.y()))
                pp2 = QPointF(*self.gris_obs.coord_px_to_wcs(id_map, p2.x(), p2.y()))
                return f'{QLineF(pp1, pp2).length():.1f} arcsec'

            if unit == 'km' or unit == 'Mm':
                pp1 = QPointF(*self.gris_obs.coord_px_to_wcs(id_map, p1.x(), p1.y()))
                pp2 = QPointF(*self.gris_obs.coord_px_to_wcs(id_map, p2.x(), p2.y()))
                obs_t = self.gris_obs.get_datetime(id_map, int(p1.x()), int(p1.y()))
                dist = earth_distance(obs_t)
                c1 = SkyCoord(pp1.x() * u.arcsec, pp1.y() * u.arcsec,
                              distance=dist, frame=Helioprojective)
                c2 = SkyCoord(pp2.x() * u.arcsec, pp2.y() * u.arcsec,
                              distance=dist, frame=Helioprojective)
                if unit == 'km':
                    return f'{c1.separation_3d(c2).km:.0f} km'
                if unit == 'Mm':
                    return f'{c1.separation_3d(c2).Mm:.2f} Mm'

            if unit == 'min' or unit == 'sec':
                obs_t1 = self.gris_obs.get_datetime(id_map, int(p1.x()), int(p1.y()))
                obs_t2 = self.gris_obs.get_datetime(id_map, int(p2.x()), int(p2.y()))
                dt = abs((obs_t2 - obs_t1).total_seconds())
                if unit == 'min':
                    dt /= 60.
                return f'{dt:.1f} {unit}'
        else:
            return "--"

    def toggle_map_profiles_panel(self):
        """Toggle map profiles panel depending on the current maps view mode."""
        act = self.ui.actMapsProfiles
        flag = act.isChecked()
        if self.sender() == act:
            if act.isChecked() and \
               self.maps_opts['view_mode'] != 'single':
                self.ui.rbMapViewMode1.setChecked(True)
        else:
            if self.maps_opts['view_mode'] != 'single':
                act.setChecked(False)
        self.show_map_profiles_panel(act.isChecked())

    def show_map_profiles_panel(self, flag):
        """Show/hide map profiles panel."""
        self.maps_opts['panel_profiles_visible'] = flag
        self.ui.panelMapsProfiles.setVisible(flag)
        for k in range(self.num_maps_profiles):
            chk = getattr(self.ui, 'chkProfile' + str(k + 1))
            for plt in self.pltMaps:
                plt.profiles[k].set_visible(flag and chk.isChecked())
        if flag:
            self.update_map_profiles_plot()

    def reset_map_profiles(self):
        """Reset all map profiles."""
        for plt in self.pltMaps:
            plt.reset_profiles()
        for k in range(self.num_maps_profiles):
            chk = getattr(self.ui, 'chkProfile' + str(k + 1))
            chk.setChecked(False)
            btn_name = 'orientProfile' + str(k + 1)
            if hasattr(self.ui, btn_name):
                btn = getattr(self.ui, btn_name)
                btn.setChecked(False)
        self.ui.chkLinkProfiles.setChecked(False)
        self.ui.comboProfileAvgWidth.setCurrentIndex(0)

    def toggle_map_profile(self, flag):
        """Show/hide selected map profile curve."""
        idx = self.sender().property('Index')
        self.pltMapCurr.set_profile_visible(idx, flag)

    def update_map_profiles_plot(self):
        """Update map profiles plot and its curves."""
        if self.ui.panelMapsProfiles.isVisible():
            wgt = self.ui.wgtMapProfiles
            plt = self.pltMapCurr
            plt.profiles_plot.set_x_axis()
            try:
                prevProfiles = wgt.getItem(0, 0)
                if prevProfiles is not None:
                    wgt.removeItem(prevProfiles)
            except:
                pass
            wgt.addItem(plt.profiles_plot, 0, 0)
            for p in plt.profiles:
                self.update_map_profile_curve(p)

    def update_map_profile_curve(self, profile):
        """Update map profile curve."""
        plt = self.pltMapCurr
        idx = profile.idx
        if plt is not None:
            plt.profiles_plot.set_curve_color(idx, profile.color)
            plt.profiles_plot.set_curve_visible(idx, profile.visible)
            if self.gris_obs and \
               profile.visible and profile.pos is not None:
                data_sel = profile.obj.getArrayRegion(plt.plot.data,
                                                      profile.obj.parent, axes=(0, 1))
                data_curve = np.average(data_sel, axis=0)
                data_curve = np.delete(data_curve, np.where(data_curve == 0))
                plt.profiles_plot.set_curve_data(idx, data_curve)

    def change_map_profile_avg_width(self):
        """Change average width size for map profiles"""
        plt0 = self.pltMaps[0]
        val = int(self.ui.comboProfileAvgWidth.currentText())
        for i in range(self.num_maps_profiles):
            plt0.profiles[i].set_width(val)

    def change_map_profile_orient(self):
        """Change selected map profile orientation."""
        btn = self.sender()
        idx = int(btn.objectName()[-1]) - 1  # assuming num < 10
        plt0 = self.pltMapCurr
        if plt0.profiles[idx].pos is not None:
            if btn.isChecked():
                txt = btn.text()
                ang = 0
                if txt == 'H':
                    ang = 0
                elif txt == 'V':
                    ang = 90
                for plt in self.pltMaps:
                    plt.profiles[idx].set_orient(ang, fix=True)
            else:
                for plt in self.pltMaps:
                    plt.profiles[idx].set_orient(fix=False)

    def link_map_profiles(self, flag):
        """Link/unlink profile centers."""
        for plt in self.pltMaps:
            plt.link_profile_centers(flag)

    def toggle_map_contours_panel(self):
        """Toggle map contours panel depending on the current maps view mode."""
        act = self.ui.actMapsContours
        if self.sender() == act:
            if act.isChecked() and \
               self.maps_opts['view_mode'] != 'single':
                self.ui.rbMapViewMode1.setChecked(True)
        else:
            if self.maps_opts['view_mode'] != 'single':
                act.setChecked(False)
        self.show_map_contours_panel(act.isChecked())

    def show_map_contours_panel(self, flag):
        """Show/hide map contours panel."""
        self.maps_opts['panel_contours_visible'] = flag
        self.ui.panelMapsContours.setVisible(flag)
        if flag:
            self.update_map_contours_widget()

    def update_map_contours_widget(self):
        """Update map countours widget that include map image histogram
        and levels color bar."""
        wgt = self.ui.graphContoursControl
        try:
            prevCntrs = wgt.getItem(0, 0)
            if prevCntrs is not None:
                wgt.removeItem(prevCntrs)
        except:
            pass
        plt = self.pltMapCurr
        if plt is not None:
            self.update_map_contours_image()
            wgt.addItem(plt.contours, 0, 0)
            self.ui.checkShowContours.setChecked(plt.contours.is_enabled)
            self.ui.checkContoursHideMap.setChecked(not plt.contours.is_map_visible)
            self.ui.comboContoursWidth.setCurrentIndex(plt.contours.line_width - 1)

    def update_map_contours_image(self):
        """Update input map image used for contours generation."""
        if self.pltMapCurr is not None:
            self.pltMapCurr.update_contours_image()

    def update_map_contours_curves(self):
        """Update all contours of current map."""
        if self.pltMapCurr is not None:
            self.pltMapCurr.contours.update_map_image()

    def auto_update_map_contours(self):
        """Set contours update automatically when input map image changes."""
        if self.pltMapCurr is not None:
            self.pltMapCurr.contours.auto_update_curves(self.sender().isChecked())

    def toggle_map_contours(self, flag):
        """Show/hide contours for current map."""
        self.pltMapCurr.contours.hide_curves(not flag)

    def toggle_map_contours_image(self, flag):
        """Show/hide current map image."""
        self.pltMapCurr.contours.hide_map(flag)

    def generate_map_contours(self):
        """Generate contours for a given number of map intensity levels."""
        num = int(self.ui.labelContoursNumLevels.text())
        self.pltMapCurr.contours.generate_curves(num)

    def reset_map_contours(self):
        """Reset and delete all contours for all maps."""
        for plt in self.pltMaps:
            plt.contours.delete_curves()

    def change_map_contours_levels(self, val):
        """Set number of levels label to a given value."""
        self.ui.labelContoursNumLevels.setText(str(val))

    def change_map_contours_width(self):
        """Set current map contours line width."""
        val = float(self.ui.comboContoursWidth.currentText())
        self.pltMapCurr.contours.line_width = val
        self.pltMapCurr.contours.update_curves()

    def on_map_plots_hover(self, x, y, val):
        """Process hover event from map plots and show info on the
        current mouse cursor position within the plot.

        Args:
            x (int): x-axis pixel coordinate
            w (int): y-axis pixel coordinate
            val (float): z-axis plot value
        """
        # TODO: Add heliocentric angle field
        self.ui.lblMapUnits.setText(self.maps_opts['unit_xy'])
        if self.ui.panelMapsQuickInfo.isVisible():
            if x < 0 or y < 0:
                self.ui.lblMapX.setText('')
                self.ui.lblMapY.setText('')
                self.ui.lblMapWCSLon.setText('')
                self.ui.lblMapWCSLat.setText('')
                self.ui.lblMapValue.setText('')
                self.ui.lblMapTime.setText('')
            else:
                if self.gris_obs:
                    i = self.map_id_curr
                    try:                        
                        lon, lat = self.gris_obs.coord_px_to_wcs(i, x, y)
                        self.ui.lblMapWCSLon.setText(f'{lon:.2f}')
                        self.ui.lblMapWCSLat.setText(f'{lat:.2f}')
                    except:
                        pass
                    try:
                        t = self.gris_obs.get_timestamp(i, x, y)
                        self.ui.lblMapTime.setText(t)
                    except:
                        pass    
                if self.maps_opts['unit_xy'] == 'arcsec':
                    x, y = self.gris_obs.coord_px_to_arcsec(x, y)
                self.ui.lblMapX.setText(f'{x:.2f}')
                self.ui.lblMapY.setText(f'{y:.2f}')
                self.ui.lblMapValue.setText(f'{val:g}')

    def on_spec_plots_hover(self, w, val):        
        """Process hover event from spectrum plots and show info on the
        current mouse cursor position within the plot.

        Args:
            w (int): x-axis wavelength index value            
            val (float): y-axis plot value
        """
        if self.ui.panelSpecQuickInfo.isVisible():
            if w == np.inf:
                self.ui.lblSpecWaveln.setText('')
                self.ui.lblSpecValue.setText('')
            else:
                u_str = self.pltSpecs[0].str_wl_unit()
                if u_str == 'index':
                    self.ui.lblSpecWaveln.setText(str(int(w)))
                else:
                    self.ui.lblSpecWaveln.setText(f'{w:.3f} {u_str}')
                self.ui.lblSpecValue.setText(f'{val:g}')

    def on_map_wgt_resize(self, ev):
        """Override PyQtGraph.GraphicsView.resizeEvent and improve map widget resizing."""
        wgt = self.ui.wgtMaps
        r = QtCore.QRectF(0, 0, wgt.size().width(), wgt.size().height())
        wgt.setRange(r, padding=0, disableAutoPixel=False)
        wgt.updateMatrix()
        self.correct_map_wgt_col_width()
        ev.accept()

    def correct_map_wgt_col_width(self):
        """Correct a bug of slightly different map widget layout column widths."""
        if self.pltMaps and self.gris_obs and \
           self.maps_opts['view_mode'] != 'single':
            wgt = self.ui.wgtMaps
            num = self.num_map_pars_selected
            if num > 1:
                if wgt.ci.layout.itemAt(0, 0) is not None:
                    w_lbl = wgt.ci.layout.itemAt(0, 0).size().width()
                else:
                    w_lbl = 0
                if self.maps_opts['axes_visible']:
                    dw = 20
                else:
                    dw = 0
                if self.num_map_pars_selected < 4:
                    w_map = (wgt.size().width() - w_lbl - dw) // num
                    for i in range(num):
                        wgt.ci.layout.setColumnFixedWidth(i + 1, w_map)
                else:
                    w_map = (wgt.size().width() - w_lbl - dw) // 2
                    wgt.ci.layout.setColumnFixedWidth(1, w_map)
                    wgt.ci.layout.setColumnFixedWidth(2, w_map)

    def change_view_spec_unit(self):
        """Change wavelength unit on spectral plots."""
        u_btn = self.sender().text()
        if u_btn == 'index':
            wl_unit = u.pix
        elif u_btn == 'nm':
            wl_unit = u.nm
        else:
            wl_unit = u.angstrom

        plt0 = self.pltSpecs[0]
        if plt0.wl_scale is not None:
            for i, plt in enumerate(self.pltSpecs):
                plt.link_view_to(None)
                plt.set_wl_scale_unit(wl_unit)
                if i > 0:
                    plt.link_view_to(plt0)
                if plt.par.name == ParIntensityNorm:
                    self.update_graph_fts_atlas()

                if self.gris_inv_profiles:
                    for iprof in self.gris_inv_profiles:
                        iprof.set_wl_scale(unit=wl_unit)

            plt0.update_cursor_pos()
            if plt0.wl_markers:
                self.relist_spec_markers()

    def set_spec_wl_ref(self, flag):
        """Set/unset relative wavelength scale."""
        plt0 = self.pltSpecs[0]
        for i, plt in enumerate(self.pltSpecs):
            plt.link_view_to(None)
            plt.set_wl_scale_ref(flag)
            if i > 0:
                plt.link_view_to(plt0)
        plt0.update_cursor_pos()
        self.relist_spec_markers()
        self.update_graph_fts_atlas()
        self.update_poi_roi_graphs()

    def toggle_spec_grids(self, flag):
        """Show/hide grids on spectrum plots."""
        self.spec_opts['grids_visible'] = flag
        for plt in self.pltSpecs:
            plt.show_grid(flag)

    def export_specs_as_image(self):
        """Export currently visible spectrum plots to image file."""
        wgt = self.ui.wgtSpec
        dlg = ImageSaveDialog(self)
        dlg.setDirectory(self.obs_dir)
        dlg.set_image_size(wgt.width(), wgt.height())
        if dlg.exec():
            exporter = pg.exporters.ImageExporter(wgt.scene())
            exporter.parameters()['width'] = dlg.image_width()
            exporter.export(dlg.image_filename())

    def export_maps_as_image(self):
        """Export currently visible map plots to image file."""
        wgt = self.ui.wgtMaps
        dlg = ImageSaveDialog(self)
        dlg.setDirectory(self.obs_dir)
        dlg.set_image_size(wgt.width(), wgt.height())
        if dlg.exec():
            exporter = pg.exporters.ImageExporter(wgt.scene())
            exporter.parameters()['width'] = dlg.image_width()
            exporter.export(dlg.image_filename())

    def load_inversion_maps(self, file=''):
        """Load inversion maps from file."""
        if not file:
            file = QFileDialog.getOpenFileName(self,
                                               'Select an inversion file',
                                               self.gris_obs.dir_path,
                                               "FITS Files (*.fits)")[0]
        if file:
            self.gris_inv_maps.append(GrisInvMaps(file))
            imap = self.gris_inv_maps[-1]
            imap.read()
            try:
                if not imap.data:
                    show_info("Could not load selected file")
                    raise

                if imap.obs_name != self.gris_obs.name:
                    show_error('File belongs to another observation: ' + imap.obs_name)
                    raise

                if imap.num_x != self.gris_obs.num_x or \
                   imap.num_y != self.gris_obs.num_y:
                    show_error('File map dimensions do not match current observation')
                    raise

                plt_pars = [plt.par.name for plt in self.pltMaps]
                for i, new_par in enumerate(imap.pars):
                    if new_par.name not in plt_pars:
                        self.add_map_plot(new_par)
                        for cbo in self.cboMapPars:
                            cbo.addItem(new_par.name)
                        plt = self.pltMaps[-1]
                        plt.sync_widgets(self.pltMaps[0])
                self.update_inversion_maps()

                # self.update_poi_roi_graphs()
                self.set_map_compass_orient()

                for plt in self.pltMaps:
                    plt.plot.fit_image_to_view()

                show_info("File has been successfully loaded\n\n" +
                          "Imported maps for parameters:\n" +
                          '\n'.join(imap.par_names))
            except:
                self.gris_inv_maps = []

    def update_inversion_maps(self):
        """Update parameter maps with inversion results."""
        if self.gris_inv_maps:
            plt_pars = [plt.par.name for plt in self.pltMaps]
            time_step = self.gris_obs.orig_map_ids[0] - 1
            if self.gris_obs.is_time_series:
                curr = self.ui.comboMapID.currentIndex()
                time_step = self.gris_obs.orig_map_ids[curr] - 1
            for imap in self.gris_inv_maps:
                for i, par in enumerate(imap.par_names):
                    try:
                        pid = plt_pars.index(par)
                        self.pltMaps[pid].set_header(imap.headers[i])
                        self.pltMaps[pid].set_image_data(imap.data[i][time_step])
                    except:
                        pass

    def get_inversion_profile_files(self, file0):
        """Get list of all inversion profiles files from a given file."""
        if not self.gris_obs.is_time_series:
            return [file0]
        else:
            file_list = []
            file_pattern = re.compile('(.*)_(\d+)\.fits$')
            file_path = Path(file0).resolve()
            file_dir = file_path.parent
            match_res = file_pattern.match(file_path.name)
            if match_res:
                file_pref = match_res.groups()[0]
                file_list_iter = file_dir.glob(file_pref+'*.fits')
                for fn in sorted(file_list_iter):
                    res = file_pattern.match(fn.name)
                    if res:
                        map_id = int(res.groups()[1]) + 1
                        if map_id in self.gris_obs.orig_map_ids:
                            file_list.append(str(fn))
            return file_list

    def load_inversion_profiles(self, file=''):
        """Load spectral line inverted profiles using the open dialog or 
        a file argument."""
        if not file:
            file = QFileDialog.getOpenFileName(self,
                                               'Select an inversion profile file',
                                               self.gris_obs.dir_path,
                                               "FITS Files (*.fits)")
        if file[0]:
            inv_files = self.get_inversion_profile_files(file[0])
            if not inv_files:
                show_error('Incorrect file name/format')
                return

            for fn in inv_files:
                self.gris_inv_profiles.append(GrisInvProfile(fn))
                self.gris_inv_profiles[-1].read()
                imap = self.gris_inv_profiles[-1]
                try:
                    if not imap.data:
                        show_info("Could not load selected file")
                        raise

                    if imap.obs_name != self.gris_obs.name:
                        show_error('File belongs to another observation: ' +
                                   imap.obs_name)
                        raise

                    if imap.num_x != self.gris_obs.num_x or \
                       imap.num_y != self.gris_obs.num_y:
                        show_error('File map dimensions do not match current observation')
                        raise
                except:
                    self.gris_inv_profiles = []
                    return

            imap = self.gris_inv_profiles[0]
            for i in range(imap.num_pars):
                plt = self.pltSpecs[i]
                for j in range(self.num_maps_poi):
                    name = f'POI{j + 1}_FIT'
                    if name not in plt.graphs:
                        plt.add_graph(name, self.poi_colors[j])
                        plt.set_graph_style(name, width=1.25, linestyle=Qt.PenStyle.DashLine)

            self.update_poi_roi_graphs()
            # self.refresh_spec_graphs_list()

            show_info("Best-fit profiles have been successfully loaded\n\n"
                      "Profiles can be checked using POIs for IQUV plots")

    def update_poi_graph_inv_profiles(self, poi):
        """Update inverted spectral line profiles for a given POI."""
        if self.gris_inv_profiles:
            for pid in self.spec_pars_selected:
                gph_name = poi.name + '_FIT'
                self.pltSpecs[pid].set_graph_visible(gph_name, poi.visible)
                self.pltSpecs[pid].set_graph_style(gph_name, color=poi.color)

            if poi.visible:
                data_sel = []
                if poi.pos is not None:
                    imap = self.gris_inv_profiles[self.map_id_curr]
                    x = int(poi.pos.x())
                    y = int(poi.pos.y())
                    if 0 <= x < imap.num_x and 0 <= y < imap.num_y:
                        if self.maps_opts['avg_box_size'] > 1:
                            s = (self.maps_opts['avg_box_size'] - 1) // 2
                            x1 = max(x - s, 0)
                            x2 = min(x + s, imap.num_x)
                            y1 = max(y - s, 0)
                            y2 = min(y + s, imap.num_y)
                            for dat in imap.data:
                                data_sel.append(
                                    np.average(dat[:, y1:y2, x1:x2], axis=(1, 2)))
                        else:
                            for dat in imap.data:
                                data_sel.append(dat[:, y, x])
                    else:
                        for pid in self.spec_pars_selected:
                            gph_name = poi.name + '_FIT'
                            self.pltSpecs[pid].set_graph_visible(gph_name, False)

                plt0 = self.pltSpecs[0]
                wl_off = (plt0.wl_offset * plt0.wl_unit0).to(plt0.wl_unit).value
                wl_scale = imap.wl_scale + wl_off
                if plt0.use_wl_ref:
                    wl_ref = plt0.convert_wl(plt0.wl_ref, plt0.wl_unit0, plt0.wl_unit)
                    wl_scale -= wl_ref
                for i, dat in enumerate(data_sel):
                    self.pltSpecs[i].graphs[gph_name].setData(
                        wl_scale, dat)

    def spec_move_view_to_cursor(self):
        """Move current spectral view to wavelength cursor."""
        self.pltSpecs[0].move_view_to_cursor()

    def spec_move_cursor_to_view(self):
        """Move cursor to the middle of the current spectral view."""
        self.pltSpecs[0].move_cursor_to_view()

    def spec_link_view_to_cursor(self, flag):
        """Link/unlink view to the cursor (when scrolling spectra, cursor is
        shown in the middle."""
        for plt in self.pltSpecs:
            plt.is_fixed_view_to_cursor = flag
        self.pltSpecs[0].move_view_to_cursor()

    def spec_move_cursor_left(self):
        """Move cursor 1 wavelength point left on spectral plots."""
        wgt = self.ui.sliderWaveln
        val = max(wgt.value() - 1, wgt.minimum())
        wgt.setValue(val)

    def spec_move_cursor_right(self):
        """Move cursor 1 wavelength point right on spectral plots."""
        wgt = self.ui.sliderWaveln
        val = min(wgt.value() + 1, wgt.maximum())
        wgt.setValue(val)

    def spec_move_cursor_left_fast(self):
        """Move cursor 10 wavelength point left on spectral plots."""
        wgt = self.ui.sliderWaveln
        val = max(wgt.value() - 10, wgt.minimum())
        wgt.setValue(val)

    def spec_move_cursor_right_fast(self):
        """Move cursor 10 wavelength right left on spectral plots."""
        wgt = self.ui.sliderWaveln
        val = min(wgt.value() + 10, wgt.maximum())
        wgt.setValue(val)

    def spec_zoom_in_horiz(self):
        """Zoom in spectra horizontally."""
        plt0 = self.pltSpecs[0]
        plt0.zoom_in_horiz()

    def spec_zoom_out_horiz(self):
        """Zoom out spectra horizontally."""
        plt0 = self.pltSpecs[0]
        plt0.zoom_out_horiz()

    def spec_zoom_all_horiz(self):
        """Zoom spectra horizontally to the whole region."""
        plt0 = self.pltSpecs[0]
        plt0.zoom_all_horiz()

    def toggle_maps_roi_qs_panel(self, flag):
        """Show/hide panel for selecting quiet sun regions."""
        self.ui.panelMapsRoiQS.setVisible(flag)
        self.hide_maps_panels(flag)
        if self.gris_obs is not None:
            plt = self.pltMaps[0]
            for r in plt.roi_qs:
                if flag:
                    r.show()
                else:
                    r.hide()
        self.ui.actSpecQS.setEnabled(not flag)
        self.ui.actSpecQS.setChecked(True)
        self.toggle_spec_qs(True)
        self.fit_maps_to_view()

    def toggle_spec_avg(self, flag):
        """Show/hide spectrum averaged over all map pixels."""
        for plt in self.pltSpecs:
            plt.set_graph_visible('Average', flag)
        for pid in self.spec_pars_selected:
            if flag:
                self.pltSpecs[pid].update_graph('Average')
        self.refresh_spec_graphs_list()

    def toggle_spec_qs(self, flag):
        """Show/hide spectrum for selected quiet sun region(s)."""
        for plt in self.pltSpecs:
            plt.set_graph_visible('Quiet Sun', flag)
        for pid in self.spec_pars_selected:
            if flag:
                self.pltSpecs[pid].update_graph('Quiet Sun')
        self.refresh_spec_graphs_list()

    def add_maps_roi_qs(self):
        """Add new ROI for quiet sun area selection."""
        for plt in self.pltMaps:
            plt.add_roi_qs()
        num = len(self.pltMaps)
        for i in range(num - 1):
            for j in range(i + 1, num):
                self.pltMaps[i].link_roi_qs_to(self.pltMaps[j])
        self.pltMaps[0].roi_qs[-1].show()

        if len(self.pltMaps[0].roi_qs) == 1:
            self.reset_spec_cont_regs()
            self.pltSpecs[0].show_cont_fit_regs(
                self.ui.actSpecContFit.isChecked())

    def reset_all_maps_roi_qs(self):
        """Reset ROI that define quiet Sun regions."""
        for plt in self.pltMaps:
            plt.delete_roi_qs()

    def update_roi_qs_spec(self, roi):
        """Calculate and update quiet sun spectrum for selected regions."""
        if roi.pos is None:
            for plt in self.pltMaps:
                plt.delete_roi_qs(roi.idx)

        plt = self.pltMaps[0]
        map_curr = self.gris_obs.maps[self.map_id_curr]

        r_area, r_sum = 0.0, 0.0
        for r in plt.roi_qs:
            rb = r.get_bounds(width=map_curr.num_x,
                              height=map_curr.num_y)
            if rb is not None:
                x1, x2 = rb.left(), rb.right()
                y1, y2 = rb.top(), rb.bottom()
                r_area += rb.width() * rb.height()
                r_sum += np.sum(map_curr.data[:, :, y1:y2 + 1, x1:x2 + 1], axis=(2, 3))

        data_qs = None
        if r_area > 0:
            data_qs = r_sum / r_area

        for plt in self.pltSpecs:
            plt.set_data_cube('Quiet Sun', data_qs)
        for pid in self.spec_pars_selected:
            self.pltSpecs[pid].update_graph('Quiet Sun')

        self.ui.actSpecQS.setEnabled(data_qs is not None)

        plt0 = self.pltSpecs[0]
        plt0.fit_continuum()
        for plt in self.pltSpecs[1:]:
            plt.contin_data = plt0.contin_data
        self.refresh_spec_graphs_list()

    def save_spec_graphs_state(self):
        """Save all spectral graphs visibility state."""
        self.spec_view_graph_conf = {}
        plt0 = self.pltSpecs[0]
        for name, gph in plt0.graphs.items():
            self.spec_view_graph_conf[name] = gph.isVisible()
        self.spec_view_graph_conf['Average'] = self.ui.actSpecAvg.isChecked()
        self.spec_view_graph_conf['Quiet Sun'] = self.ui.actSpecQS.isChecked()
        self.spec_view_graph_conf['FTS Atlas'] = self.ui.actSpecAtlas.isChecked()

    def hide_spec_graphs(self, flag=True):
        """Hide all but essential spectral graphs for continuum fitting."""
        if flag:
            plt0 = self.pltSpecs[0]
            for name in plt0.graphs:
                plt0.set_graph_visible(name, False)
            plt0.set_graph_visible('Quiet Sun', True)
        else:
            if self.spec_view_graph_conf:
                plt0 = self.pltSpecs[0]
                for name in plt0.graphs:
                    plt0.set_graph_visible(name, self.spec_view_graph_conf[name])
                self.ui.actSpecAvg.setChecked(self.spec_view_graph_conf['Average'])
                self.ui.actSpecQS.setChecked(self.spec_view_graph_conf['Quiet Sun'])
                self.ui.actSpecAtlas.setChecked(self.spec_view_graph_conf['FTS Atlas'])

    def toggle_spec_cont_fit_panel(self, flag):
        """Show/hide spectral continuum fitting panel."""
        self.ui.panelSpecViewParControls.setVisible(not flag)
        if flag:
            self.ui.actSpecRelWaveln.setChecked(False)
        self.ui.panelSpecContFit.setVisible(flag)
        self.ui.chkShowPOI.setChecked(not flag)
        self.ui.chkShowROI.setChecked(not flag)
        self.ui.panelMapsPOI.setEnabled(not flag)
        if not self.is_obs_open:
            return
        if flag:
            self.save_spec_graphs_state()
            self.ui.actSpecRelWaveln.setChecked(False)
            self.ui.actSpecQS.setChecked(True)
            self.ui.actSpecAvg.setChecked(False)
            self.spec_pars_save = self.spec_pars_selected
            self.spec_pars_selected = [0]
            wgt = self.ui.wgtSpec
            wgt.clear()
            wgt.addItem(self.pltSpecs[0], row=0, col=0)
            # self.update_spec_par_plots()
            self.hide_spec_graphs(True)
            self.update_spec_cont_fit_intervals()
        else:
            self.spec_pars_selected = self.spec_pars_save
            plt0 = self.pltSpecs[0]
            for plt in self.pltSpecs[1:]:
                plt.contin_data = plt0.contin_data
            self.hide_spec_graphs(False)
            self.update_plot_specs_widget()
        self.ui.actSpecRelWaveln.setEnabled(not flag)
        self.ui.actSpecAvg.setEnabled(not flag)
        self.ui.actSpecQS.setEnabled(not flag)
        self.ui.actSpecAtlas.setEnabled(not flag)
        self.pltSpecs[0].show_cont_fit_regs(flag)
        self.update_spec_cont_fit_fts_atlas()

    def add_spec_cont_reg(self):
        """Add wavelength region used for fitting spectral continuum."""
        self.pltSpecs[0].add_cont_reg()

    def reset_spec_cont_regs(self):
        """Reset wavelength regions for spectral continuum to default."""
        self.pltSpecs[0].set_default_cont_regs()

    def del_spec_cont_reg(self):
        """Delete wavelength region used for fitting spectral continuum."""
        plt0 = self.pltSpecs[0]
        lst = self.ui.lstContFitIntervals
        reg_sel = [plt0.contin_regs[idx.row()] for idx in lst.selectedIndexes()]
        for reg in reg_sel:
            plt0.delete_cont_reg(reg)
        self.update_spec_cont_fit_intervals()

    def update_spec_cont_fit_degree(self, value):
        """Update degree of the polynomial used for spectral continuum fit."""
        self.pltSpecs[0].fit_continuum(degree=value)

    def update_spec_cont_fit_mult(self, val):
        """Update scaling multiplier of spectral continuum fit."""
        sli = self.ui.sliContFitMult
        dsb = self.ui.dsbContFitMult
        if isinstance(self.sender(), QSlider):
            val = dsb.minimum() + (dsb.maximum() - dsb.minimum()) * val / sli.maximum()
            dsb.blockSignals(True)
            dsb.setValue(val)
            dsb.blockSignals(False)
        else:
            val1 = (val - dsb.minimum()) * sli.maximum() /\
                   (dsb.maximum() - dsb.minimum())
            sli.blockSignals(True)
            sli.setValue(int(val1))
            sli.blockSignals(False)
        self.pltSpecs[0].fit_continuum(mult=val)

    def update_spec_cont_fit_intervals(self):
        """Update wavelength intervals for spectral continuum fit."""
        self.ui.lstContFitIntervals.clear()
        plt0 = self.pltSpecs[0]
        for i, reg in enumerate(plt0.contin_regs):
            intvl = reg.getRegion()
            wl1 = plt0.str_wl(intvl[0]).split(' ')[0]
            wl2 = plt0.str_wl(intvl[1]).split(' ')[0]
            self.ui.lstContFitIntervals.addItem(f'{i+1}. {wl1}–{wl2}')

    def update_spec_cont_fit_coeff(self):
        """List coefficients of polynomial from spectral continuum fit."""
        self.ui.lstContFitPars.clear()
        if self.pltSpecs[0].contin_func is not None:
            coef = self.pltSpecs[0].contin_func.coef
            for i, c in enumerate(coef):
                self.ui.lstContFitPars.addItem(f'c{i} = {c:.4e}')

    def update_spec_cont_fit_fts_atlas(self):
        """Show/hide FTS atlas spectrum for fitting spectral continiuum."""
        plt0 = self.pltSpecs[0]
        if self.ui.panelSpecContFit.isVisible():
            flag = self.ui.chkContFitShowFTS.isChecked()
            if flag:
                idx = [p.par.name for p in self.pltSpecs].index(ParIntensityNorm)
                x_pts, y_data = self.pltSpecs[idx].graphs['FTS Atlas'].getData()
                i_cont = np.array([plt0.get_continuum_at(wl) for wl in x_pts])
                y_pts = y_data * i_cont
                plt0.graphs['FTS Atlas'].setData(x_pts, y_pts)
            plt0.set_graph_visible('FTS Atlas', flag)
        else:
            plt0.set_graph_visible('FTS Atlas', False)

    def refresh_spec_graphs_list(self):
        """Update bottom option panel combo of current spectrum plot graphs."""
        if self.spec_pars_selected:
            plt = self.pltSpecs[self.spec_pars_selected[0]]
            cbox = self.ui.cboGraphCurr
            cbox.blockSignals(True)
            curr = cbox.currentText()
            cbox.clear()
            for name in plt.graphs:
                if plt.has_data_cube(name):
                    cbox.addItem(name)
            if cbox.findText(curr):
                cbox.setCurrentText(curr)
            else:
                cbox.setCurrentIndex(0)
            cbox.blockSignals(False)
            if cbox.currentText():
                cbox.currentTextChanged.emit(cbox.currentText())

    def select_spec_graph(self, curr):
        """Update selected spectrum plot graph style and options."""
        plt = self.pltSpecs[self.spec_pars_selected[0]]
        self.ui.chkShowGraph.blockSignals(True)
        self.ui.chkShowGraph.setChecked(plt.is_graph_visible(curr))
        self.ui.chkShowGraph.blockSignals(False)
        color, width, style = plt.get_graph_style(curr)
        self.ui.btnGraphColor.setStyleSheet(
            'background-color: ' + color + ';')
        self.ui.cboGraphWidth.blockSignals(True)
        self.ui.cboGraphWidth.setCurrentText(f'{width:g}')
        self.ui.cboGraphWidth.blockSignals(False)
        self.ui.cboGraphStyle.blockSignals(True)
        self.ui.cboGraphStyle.setCurrentIndex(style.value - 1)
        self.ui.cboGraphStyle.blockSignals(False)

    def toggle_spec_graph(self):
        """Show/hide selected spectrum from bottom option panel."""
        curr = self.ui.cboGraphCurr.currentText()
        if curr == 'Average':
            self.ui.actSpecAvg.trigger()
        elif curr == 'Quiet Sun':
            self.ui.actSpecQS.trigger()
        else:
            chk = getattr(self.ui, 'chk' + curr)
            chk.click()

    def change_spec_graph_color(self):
        """Set line color for selected spectrum graph."""
        curr = self.ui.cboGraphCurr.currentText()
        if curr == 'Average' or curr == 'Quiet Sun':
            btn = self.sender()
            c = btn.palette().button().color()
            color = QColorDialog.getColor(c)
            if color.isValid():
                btn.setStyleSheet('background-color: ' + color.name() + ';')
                for plt in self.pltSpecs:
                    plt.set_graph_style(curr, color=color)
        else:
            chk = getattr(self.ui, 'clr' + curr)
            chk.click()

    def change_spec_graph_width(self, txt):
        """Set line width for selected spectrum graph."""
        curr = self.ui.cboGraphCurr.currentText()
        width = float(txt)
        for plt in self.pltSpecs:
            plt.set_graph_style(curr, width=width)

    def change_spec_graph_style(self, idx):
        """Set line style for selected spectrum graph."""
        curr = self.ui.cboGraphCurr.currentText()
        for plt in self.pltSpecs:
            plt.set_graph_style(curr, linestyle=Qt.PenStyle(idx + 1))

    def show_spec_quickinfo_panel(self, flag):
        """Show/hide quick info panel for spectra."""
        self.spec_opts['panel_quickinfo_visible'] = flag
        self.ui.panelSpecQuickInfo.setVisible(flag)

    def show_spec_markers_panel(self, flag):
        """Show/hide spectral lines and markers panel."""
        self.spec_opts['panel_markers_visible'] = flag
        self.ui.panelSpecMarkers.setVisible(flag)

    def add_spec_marker(self):
        """Add wavelength marker on spectrum plots."""        
        flag_ok = False
        for plt in self.pltSpecs:            
            flag_ok = plt.add_wl_marker()
            if not flag_ok:
                break
        if flag_ok:
            self.ui.listSpecsMarkers.addItem('')
            self.relist_spec_markers()

    def delete_spec_marker(self):
        """Delete wavelength marker from spectrum plots."""
        lst = self.ui.listSpecsMarkers
        lst.blockSignals(True)
        sel = lst.selectedIndexes()
        for idx in sorted(sel, reverse=True):
            lst.takeItem(idx.row())
            for plt in self.pltSpecs:
                plt.delete_wl_marker(idx.row())
        lst.clearSelection()
        lst.blockSignals(False)
        self.relist_spec_markers()

    def relist_spec_markers(self):
        """Update list of markers according to the units and absolute/relative
        wavelength scale."""
        plt0 = self.pltSpecs[0]
        if plt0.wl_markers:
            for i, mrk in enumerate(plt0.wl_markers):
                item_text = plt0.str_wl(mrk.value())
                self.ui.listSpecsMarkers.item(i).setText(item_text)

    def clear_spec_markers(self):
        """Clear and reset all wavelength markers."""
        for plt in self.pltSpecs:
            plt.reset_wl_markers()
        self.ui.listSpecsMarkers.blockSignals(True)
        self.ui.listSpecsMarkers.clear()
        self.ui.listSpecsMarkers.blockSignals(False)

    def goto_spec_marker(self, idx):
        """Move cursor to the selected wavelength marker."""
        plt0 = self.pltSpecs[0]
        plt0.goto_marker(idx)
        plt0.update_cursor_pos()

    def goto_spec_next_marker(self):
        """Move cursor to the next wavelength marker."""
        plt0 = self.pltSpecs[0]
        idx = plt0.goto_next_marker()
        lst = self.ui.listSpecsMarkers
        if idx >= 0:
            lst.blockSignals(True)
            lst.clearSelection()
            lst.setCurrentRow(idx)
            lst.blockSignals(False)
        plt0.update_cursor_pos()

    def goto_spec_prev_marker(self):
        """Move cursor to the previous wavelength marker."""
        plt0 = self.pltSpecs[0]
        idx = plt0.goto_prev_marker()
        lst = self.ui.listSpecsMarkers
        if idx >= 0:
            lst.blockSignals(True)
            lst.clearSelection()
            lst.setCurrentRow(idx)
            lst.blockSignals(False)
        plt0.update_cursor_pos()

    def toggle_spec_markers(self, flag):
        """Show/hide wavelength vertical markers on spectra"""
        self.spec_opts['markers_visible'] = flag != 0
        for plt in self.pltSpecs:
            plt.hide_wl_markers(not flag)

    def list_spec_lines(self):
        """Fill spectral line list for current wavelength region."""
        plt0 = self.pltSpecs[0]
        self.ui.listSpecLines.clear()
        if plt0.line_markers is not None:
            txt_items = []
            for i, line in enumerate(plt0.line_markers):
                txt_items.append(plt0.str_wl(line.wl, u.angstrom) + '   ' + line.name)
            self.ui.listSpecLines.addItems(txt_items)

    def goto_spec_line(self, idx):
        """Move cursor position to the given spectral line."""
        plt0 = self.pltSpecs[0]
        plt0.goto_line(idx)
        plt0.update_cursor_pos()

    def goto_spec_next_line(self):
        """Move cursor position to the next spectral line in list."""
        plt0 = self.pltSpecs[0]
        idx = plt0.goto_next_line()
        lst = self.ui.listSpecLines
        if idx >= 0:
            lst.blockSignals(True)
            lst.clearSelection()
            lst.setCurrentRow(idx)
            lst.blockSignals(False)
        plt0.update_cursor_pos()

    def goto_spec_prev_line(self):
        """Move cursor position to the previous spectral line in list."""
        plt0 = self.pltSpecs[0]
        idx = plt0.goto_prev_line()
        lst = self.ui.listSpecLines
        if idx >= 0:
            lst.blockSignals(True)
            lst.clearSelection()
            lst.setCurrentRow(idx)
            lst.blockSignals(False)
        plt0.update_cursor_pos()

    def toggle_spec_lines(self, flag):
        """Show/hide spectral lines and labels on spectrum plots. """
        self.spec_opts['lines_visible'] = flag != 0
        for plt in self.pltSpecs:
            plt.show_lines(flag)

    # def change_spec_lines_color(self):        
    #     btn = self.sender()
    #     c = btn.palette().button().color()
    #     color = QColorDialog.getColor(c)
    #     if color.isValid():
    #         btn.setStyleSheet('background-color: ' + color.name() + ';')
    #         for plt in self.pltSpecs:
    #             plt.set_lines_color(color)

    def set_spec_wl_offset(self, val):
        """Shift wavelength scale by an offset (in Angstroms)."""
        sli = self.ui.sliSpecWavelnOffset
        dsb = self.ui.dsbSpecWavelnOffset
        if isinstance(self.sender(), QSlider):
            val *= dsb.maximum() / sli.maximum()
            dsb.blockSignals(True)
            dsb.setValue(val)
            dsb.blockSignals(False)
        else:
            val1 = val * sli.maximum() / dsb.maximum()
            sli.blockSignals(True)
            sli.setValue(int(val1))
            sli.blockSignals(False)

        self.spec_opts['waveln_offset'] = val
        for plt in self.pltSpecs:
            plt.set_wl_scale_offset(val)
        self.relist_spec_markers()
        plt0 = self.pltSpecs[0]
        plt0.update_cursor_pos()
        self.update_poi_roi_graphs()
        self.update_graph_fts_atlas()
        self.update_spec_cont_fit_fts_atlas()
        self.update_spec_cont_fit_intervals()

    def set_specs_wl_scale(self):
        """Initialize wavelength scale for all spectrum plots according to
        WCS from observation metadata."""
        for plt in self.pltSpecs:
            plt.set_wl_scale(self.gris_obs.num_waveln, u.angstrom, self.gris_obs.wcs_spec)
            plt.update_wl_scale()

    def update_graph_average(self):
        """Update spatially averaged spectrum for current map."""
        obs = self.gris_obs
        data_avg = np.average(obs.maps[self.map_id_curr].data, axis=(2, 3))
        for plt in self.pltSpecs:
            plt.set_data_cube('Average', data_avg)
        for pid in self.spec_pars_selected:
            self.pltSpecs[pid].update_graph('Average')

    def update_graph_qs(self):
        """Update quiet Sun spectrum based on selected ROI."""
        for r in self.pltMaps[0].roi_qs:
            self.update_roi_qs_spec(r)

    def update_graph_fts_atlas(self):
        """Update FTS atlas spectral curve to current units and set
        relative/absolute wavelength scale."""
        idx = [p.par.name for p in self.pltSpecs].index(ParIntensityNorm)
        plt = self.pltSpecs[idx]
        wl1 = plt.wl_scale[0]
        wl2 = plt.wl_scale[-1]
        if plt.use_wl_ref:
            wl1 += plt.convert_wl(plt.wl_ref, plt.wl_unit0, plt.wl_unit)
            wl2 += plt.convert_wl(plt.wl_ref, plt.wl_unit0, plt.wl_unit)
        wl1, wl2 = plt.convert_wl([wl1, wl2], plt.wl_unit, u.angstrom)
        sel = np.where((fts_atlas[:, 0] > wl1) & (fts_atlas[:, 0] < wl2))
        x, y = fts_atlas[sel, 0][0], fts_atlas[sel, 1][0]
        x = plt.convert_wl(x - plt.wl_ref, u.angstrom, plt.wl_unit)
        plt.graphs['FTS Atlas'].setData(x, y)
        plt.set_graph_visible('FTS Atlas', self.ui.actSpecAtlas.isChecked())

    def toggle_spec_fts(self, flag):
        """Show/hide FTS atlas spectrum (I/Ic parameter plot only)."""
        idx = [p.par.name for p in self.pltSpecs].index(ParIntensityNorm)
        if idx > 0:
            plt = self.pltSpecs[idx]
            plt.set_graph_visible('FTS Atlas', flag)

    def closeEvent(self, ev):
        """Exit program using top Close button."""
        if not self.flag_menu_exit:
            if show_question_yes('Exit program?'):
                if self.dlgHelp is not None:
                    if self.dlgHelp.isVisible():
                        self.dlgHelp.close()
            else:
                ev.ignore()

    def exit_app(self):
        """Exit program from menu."""
        if show_question_yes('Exit program?'):
            self.flag_menu_exit = True
            if self.dlgHelp is not None:
                if self.dlgHelp.isVisible():
                    self.dlgHelp.close()
            self.close()

    def toggle_shortcuts(self):
        """Show/hide program shortcuts dialog window."""
        if self.dlgShortcuts is None:
            self.dlgShortcuts = QtWidgets.QDialog()
            window = Ui_GrisViewShortcuts()
            window.setupUi(self.dlgShortcuts)
            self.dlgShortcuts.setWindowFlag(Qt.WindowType.FramelessWindowHint)
            self.dlgShortcuts.setWindowOpacity(0.9)
            self.dlgShortcuts.mousePressEvent = self.close_shortcuts
        self.dlgShortcuts.exec()

    def close_shortcuts(self, ev):
        """Close program shortcuts dialog window."""
        self.dlgShortcuts.accept()

    def show_help(self):
        """Show help window."""
        if self.dlgHelp is None:
            self.dlgHelp = GrisViewHelp()
        self.dlgHelp.show()

    def about_app(self):
        """Show information about the program."""
        show_info("Leibniz Institute for Solar Physics (KIS)\n"
                  "Science Data Center\n\n"
                  "GRISView\n"
                  "ver. " + __version__ + "\n\n"
                                          "Author: Taras Yakobchuk\n"
                                          "e-mail: yakobchuk@leibniz-kis.de")


def check_error_session_file(file_name):
    """Check saved session file for errors."""
    session = toml.load(file_name)
    if 'Preferences' not in session:
        return "Session file has unknown format"
    conf = session['Preferences']
    if 'ObsDir' not in conf or \
       'ObsName' not in conf:
        return "Invalid session file"
    if not Path(conf['ObsDir']).exists():
        return "Could not find observation by the path in the session file"
    return


def check_input_args(args):
    """Check CLI arguments for errors."""
    if not args.path:
        return True
    err_head = 'GRIS data visualization and analysis tool'
    err_msg = ''
    obs_path = Path(args.path)
    if not obs_path.exists():
        err_msg = "Input file/folder could not be found"
    if not err_msg:
        obs_dir = path.abspath(args.path)
        obs_file = ''
        if obs_path.is_file():
            ext = obs_path.suffix
            if ext == '.ses':
                err_msg = check_error_session_file(obs_dir)
                if not err_msg:
                    return True
            elif ext != '.fits':
                err_msg = "Unknown input file format"
            else:
                obs_file = obs_path.name
                obs_dir = obs_path.parent
        if not err_msg:
            obs_dir_obj = GrisObsDir(obs_dir)
            if not obs_dir_obj.files:
                err_msg = "Could not find observation(s) at the given path"
            else:
                if obs_file:
                    obs_names = [obs_dir_obj.get_file_obs_name(obs_file)]
                else:
                    obs_names = obs_dir_obj.get_obs_names()
                if len(obs_names) == 1:
                    num_maps = len(obs_dir_obj.get_obs_map_ids(obs_names[0]))
                    if int(args.map1) > 0:
                        if not 1 <= int(args.map1) <= num_maps:
                            err_msg = "Could not find map number in time series"
                    if int(args.map2) > 0 and not err_msg:
                        if not 1 <= int(args.map2) <= num_maps:
                            err_msg = "Could not find map number in time series"
    if err_msg:
        print(f"{err_head}\n\n{err_msg}")
        return False
    else:
        return True


def cli_open_obs(gui, args):
    """Read CLI arguments and open observation at startup."""
    obs_path = Path(args.path)
    obs_map1 = int(args.map1)
    obs_map2 = int(args.map2)
    if obs_path.is_file():
        ext = obs_path.suffix
        if ext == '.fits':
            obs_dir = str(obs_path.parent)
            obs_name = GrisObsDir.get_file_obs_name(obs_path.name)
            gui.open_obs(obs_dir, obs_name, obs_map1, obs_map2)
        elif ext == '.ses':
            gui.load_session(obs_path)
    if obs_path.is_dir():
        obs_dir = str(obs_path)
        obs_dir_obj = GrisObsDir(obs_path)
        obs_names = obs_dir_obj.get_obs_names()
        if len(obs_names) == 1:
            gui.open_obs(obs_dir, obs_names[0], obs_map1, obs_map2)
        elif len(obs_names) > 1:
            gui.open_obs(obs_dir)


def main():
    parser = argparse.ArgumentParser(description='GRIS data visualization and analysis tool')
    parser.add_argument("path", default='', nargs='?', help='File/folder of GRIS observation or a session file')
    parser.add_argument("map1", default='-1', nargs='?', help='Start time step number (for time series)')
    parser.add_argument("map2", default='-1', nargs='?', help='End time step number (for time series)')
    args = parser.parse_args()
    if check_input_args(args):
        app = QtWidgets.QApplication([])
        app.setStyle('Fusion')
        form = GrisViewGUI()
        if args.path:
            cli_open_obs(form, args)
        sys.exit(app.exec())


if __name__ == '__main__':
    main()
