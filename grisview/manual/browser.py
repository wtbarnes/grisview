from PyQt6.QtCore import QUrl
from PyQt6.QtWidgets import QDialog

from grisview.gui.help_form import Ui_GrisViewHelp

from os import path


class GrisViewHelp(QDialog):

    def __init__(self, **args):
        super().__init__()
        self.ui = Ui_GrisViewHelp()
        self.ui.setupUi(self)
        self.index_link = QUrl('index.md')
        curr_path = path.dirname(path.realpath(__file__))
        self.ui.textBrowser.setSearchPaths(
            [curr_path, path.join(curr_path, 'images')])
        self.ui.textBrowser.setSource(self.index_link)
        self.ui.textBrowser.setOpenExternalLinks(True)
        self.ui.btnHome.clicked.connect(self.ui.textBrowser.home)
        self.ui.btnBack.clicked.connect(self.ui.textBrowser.backward)
        self.ui.btnClose.clicked.connect(self.close)
