## Profiles Panel

<table cellpadding="10" width="100%">
    <tr>
        <td align="center"><img src="profiles_panel.jpg"/></td>        
    </tr>
</table>

Choose **Maps > Profiles** to show/hide this panel or press the **R** key.
The panel is available only in a single map view mode.

Enable one of the checkboxes on the left to add a profile. New profile appears 
in the middle of the visible map area.
Profiles can be dragged across the image using
the mouse. Position the pointer over the handle and drag it to adjust profile
length and orienation. 

**H** and **V** buttons lock the profile orientation to horizontal/vertical.  
**Slice Width** sets the profile width that it is averaged over.  
Use **Link Centers** to link and lock current profile centers. Linked profiles
are dragged together. This is useful if you want to quickly get radial profiles
in perpendicular directions.  
**Reset** button deletes all profiles and resets their colors to defaults.

Profile colors can be changed using color buttons on the panel or by double-clicking
the profile on the map. Right-click it to open the context menu with options.
