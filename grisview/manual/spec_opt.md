## Spectral plots advanced options

<table cellpadding="10" width="100%">
    <tr>
        <td align="center"><img src="spec_opt.jpg"/></td>        
    </tr>
</table>

Choose **Spectra > Options** to show/hide this panel or press the **O** key.
Select the spectrum from drop-down list to change visibility, colors and line
style. **Show cursor** option is useful when exporting visible
plots into image files. In this panel one can also change the wavelength scale
units used for spectral plots.
