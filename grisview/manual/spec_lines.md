## Spectral lines and markers panel

<table cellpadding="10" width="100%">
    <tr>
        <td align="center"><img src="spec_lines.jpg"/></td>        
    </tr>
</table>

Choose **Spectra > Lines and Markers** to show/hide this panel or press the **L** key. GRISView includes lists of main absorption lines for 1083 nm and 1565 nm spectral intervals. Click **On/Off** at the very top to show/hide
line markers and labels on the spectral plots. You can quickly navigate between the lines using **Prev/Next** buttons or by pressing
**Ctrl+Left/Right Arrows**.

Markers let you save wavelengths points and navigate 
between them. Set the cursor position and choose **Spectra > Markers > Add Marker** or press **Insert** key to create a new marker. To navigate between markers use **Prev/Next** buttons or press
**Ctrl+Shift+Left/Right Arrows**.

If you find that wavelength scale has some systematic shift with respect to the spectral lines, set the **Offset** at the very bottom to roughly compensate this.
