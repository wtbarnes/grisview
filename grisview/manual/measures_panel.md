## Measures panel

<table cellpadding="10" width="100%">
    <tr>
        <td align="center"><img src="measures_panel.jpg"/></td>        
    </tr>
</table>

Choose **Maps > Measures** to show/hide the panel or press the **M** key. This panel
lets you measure distances and time differences between points on the map.
Enable one of the checkboxes on the left to start measurement. By default,
a new measure appears in the middle of the visible map area. Measures can be
dragged by the mouse across the image.

Drag measure handles and see the results on the panel. Use colors
to easily distinguish between different measures.
Measure colors can be changed using the color buttons on the panel or by
double-clicking measure lines. Also, right-click measure on the map to
open the context menu with options.

**On/Off** checkboxe shows/hides all measures.  
**Reset** button deletes all measures and resets their colors to defaults.  
**Distance** buttons set measurement unit to pixels, arcseconds, kilo- or megameters  
**Time difference** buttons set measurement unit to seconds or minutes.
