## Viewing headers

You can select **File > View Headers** from main menu to view headers of
loaded observation FITS files. There are navigation buttons to quickly browse
through the file list, and files are automatically sorted by slit position and,
in case of time-series observation, one can switch between individual time points.
The name and folder of the currently displayed file are displayed on the top.
Headers are shown in categories, that can be collapsed and expanded. For
quick search there is **Filter** option. The search is done as you type the
keyword name. Click **Clear** button to remove filter and return to full
list view.
