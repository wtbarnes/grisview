## Loading and saving sessions

A session file is essentially a TOML file that contains necessary information to
recreate a GRISView session. You can set up different tools and panels, save a
session file, and then run a new GRISView session later and be able to pick up 
where you left off when you saved the session file.

You can select **File > Save Session** to open up a save file dialog. Once the
save file dialog is opened, select the location and filename that you want to
use to store the session file. By default, GRISView stores session files in 
the observation' data files folder.

Restoring a GRISView session file resets program GUI to get back to the state
described in the session file. To restore a session
file, click **File > Load Session** option and choose a session file to open
using the open file dialog. If the session file is related to the currently
open observation, GRISView will not load the data files again, but only restore the
program state.
