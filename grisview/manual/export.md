## Exporting plots

In GRISView you can export current map and spectral plots as images. Output
image size can be changed, however, preserving the aspect ratio. Before
exporting it is recommended to maximize application window and adjust the
horizontal size of the exported plot panel. To do that position the pointer over
the vertical dividing line with dots in the middle and then click and drag
left-right to adjust panels width. For spectra you may also want to change the
plot [options](spec_opt.md) first and hide the wavelength cursor.

Go to **File > Export > Map Plots as Image** (or **Spectral Plots as Image**)
in the main menu to open file saving dialog. Select the location and filename
that you want to use to store the image file. By default, GRISView saves
image files in the observation' data files folder. Then choose the output file
format in the drop-down list.

You can change the resolution of the output image' width and height, however,
aspect ratio cannot be changed. Image dimensions can be indicated in pixel
units and as a percentage of the current screen size of the panel. The size can
can be quickly set to origial by clicking **Reset** buttons.
