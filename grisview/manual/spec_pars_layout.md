## Spectral plot parameters selection panel

<table cellpadding="10" width="100%">
    <tr>
        <td align="center"><img src="spec_pars_layout.jpg"/></td>        
    </tr>
</table>

You can select here the parameter(s), for which to display spectra, and easily arrange multiple spectral plots in 
vertical layout. Use the checkboxes and the drop-downs to the right to add/remove parameter plots and customize their
position within the layout. 
Currently, GRISView includes the following parameters for visualization:

**I, Q, U, V** - Stokes parameters as read from input FITS files  
**Q/I, U/I, V/I** - Stokes parameters normalized to input intensity      
**DoLP** - degree of linear polarization      
**DoP** - degree of polarization  
**I/Ic, Q/Ic, U/Ic, V/Ic** - Stokes parameters normalized to Quiet Sun continuum intensity. To enable this set, you need to define Quiet Sun region(s) first and perform continuum fitting.

The bottom half of the bar has the slider to set the wavelength. Current wavelength is indicated by the red cursor in spectral plots.
Changes take effect as you drag the slider, updating parameter maps to the current wavelength.
