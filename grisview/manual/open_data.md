## Downloading and opening data

<table cellpadding="10" width="100%">
    <tr>
        <td align="center"><img src="open_dialog.jpg"/></td>        
    </tr>
</table>

GRISView works with GRIS@GREGOR calibrated data files, which are distributed through [Science Data Centre Arhive](https://archive.sdc.leibniz-kis.de/). Observations in both single map and time series scan modes are supported. Please note that data sets are downloaded from the archive in the TAR format and must be extracted first.

Choose **File > Open** in the main menu to get observation open dialog or press **Ctrl/Command+O**. Select folder, containing FITS files. The folder may contain multiple observations, while GRISView can only work with one observation at a time. Select an observation from the list on the top right of the window. Information about observation can be seen in the **Details** tab below. If selected folder contains PNG/GIF preview images, they can be displayed in the **Previews** tab. For new data sets downloaded/extracted during the GRISView work session, use **Refresh** button on the right to see the changes. Clicking **Open** will start loading selected observation. It can take some time, depending on the size of the data set and storage device read speed.

Before proceeding and opening observation, please check the total size of files and see if you have enough RAM, as GRISView loads all FITS files into the memory. For larger time series, you may want to load only time points in the *Start/End* range in order to use less memory.
