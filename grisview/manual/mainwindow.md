## GRISView main window

<table cellpadding="10" width="100%">
    <tr>
        <td align="center"><img src="images/main_window.jpg" style="padding: 20px;"/></td>        
    </tr>
    <tr>
        <td align="center">GRISView main window</td>
    </tr>
</table>

1. [Map plot parameters selection panel](map_pars_layout.md) 
2. [Map plots view area](maps_view_area.md)
3. [Quick info panel for maps](maps_quick_info.md)
4. [POI & ROI panel](poi_roi_panel.md)  
5. [Measures panel](measures_panel.md)
6. [Contours panel](contours_panel.md)
7. [Profiles panel](profile_panel.md)
8. [Spectral plot parameters selection panel](spec_pars_layout.md)
9. [Spectral plots view area](spec_view_area.md)
10. [Quick info panel for spectra](spec_quick_info.md)
11. [Spectral lines and markers panel](spec_lines.md)
12. [Spectral plots advanced options](spec_opt.md)
