## Quick info panel for maps

<table cellpadding="10" width="100%">
    <tr>
        <td align="center"><img src="maps_quick_info.jpg"/></td>        
    </tr>
</table>

Choose **Maps > Quick Info** to show/hide the panel or press the **Q** key. It shows pixel coordinates and value,
while hovering the mouse pointer over the map. It works for both single and multiple plots view modes. In addition
to relative point coordinates within a frame, WCS information from headers is used to calculate helioprojective
longitudes and  latitudes. Since standard GRIS maps are generated by stacking together multiple instrument slit positions, which are taken not simultaneously, the timestamp field is added for exact time display at each point. 
